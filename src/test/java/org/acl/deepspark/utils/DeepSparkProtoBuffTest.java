package org.acl.deepspark.utils;

import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam.Builder;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam.Inputs;

import com.google.protobuf.TextFormat;
import com.google.protobuf.TextFormat.ParseException;

public class DeepSparkProtoBuffTest {

	public static void main(String[] args) throws ParseException {
		Builder b = DeepSparkParam.newBuilder();
		b.setPeriod(10);
		b.setDecayLimit(10);
		for(int i = 0; i < 5; i++) {
			Inputs.Builder t = Inputs.newBuilder();
			TextFormat.getParser().merge("name: \"name1\"\ndim: [10, 100]",t);
			b.addInputs(t.build());
		}
		
		System.out.println(b.toString());
	}

}
