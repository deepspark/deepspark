package org.acl.deepspark.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import com.google.protobuf.CodedInputStream;

import caffe.Caffe.NetParameter;
import caffe.Caffe.V1LayerParameter;

public class NetParameterTester {
	public static void main(String[] args) throws FileNotFoundException, IOException {
		String name = "C:\\Users\\Hanjoo Kim\\Documents\\VGG_CNN_S.caffemodel";
		
		CodedInputStream input = CodedInputStream.newInstance(new FileInputStream(name));
		input.setSizeLimit(Integer.MAX_VALUE);
		NetParameter net = NetParameter.parseFrom(input);
		
		System.out.println(net.getLayersCount());
		List<V1LayerParameter> list = net.getLayersList();
		
		for(V1LayerParameter d :list) {
			System.out.println("layer {");
			System.out.println("name: " + d.getName());
			if(d.hasConvolutionParam()) {
				System.out.println("convolution_param {");
				System.out.println(d.getConvolutionParam().toString());
				System.out.println("}");
			}
			
			if(d.hasLrnParam()) {
				System.out.println("lrn_param {");
				System.out.println(d.getLrnParam().toString());
				System.out.println("}");
				
				System.out.println(d.toString());
			}
			
			if(d.hasPoolingParam()) {
				System.out.println("pooling_param {");
				System.out.println(d.getPoolingParam().toString());
				System.out.println("}");
			}
			
			if(d.hasInnerProductParam()) {
				System.out.println("inner_product_param {");
				System.out.println(d.getInnerProductParam().toString());
				System.out.println("}");
			}
			
			if(d.hasDropoutParam()) {
				System.out.println("dropout_param {");
				System.out.println(d.getDropoutParam().toString());
				System.out.println("}");
			}
			
			System.out.println("}");
		}
	}
}
