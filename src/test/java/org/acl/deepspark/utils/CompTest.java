package org.acl.deepspark.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.acl.deepspark.data.Weight;
import org.acl.deepspark.data.WeightStat;

public class CompTest {
	public static void main(String[] args )  {
		List<Weight> list = new ArrayList<Weight>(); 
		Weight w = new Weight();
		w.floatData = new float[80000000];
		
		Random r = new Random();
		
		long start =  System.currentTimeMillis();
		for(int i = 0; i < w.floatData.length; i++) {
			w.floatData[i] +=r.nextFloat();
			//System.out.println(w.floatData[i]);
		}
		System.out.println(System.currentTimeMillis() - start + " ms");
		
		list.add(w);
		
//		start =  System.currentTimeMillis();
//		Weight comp = WeightUtil.compressRandomly(w, 0.80f);
//		System.out.println(System.currentTimeMillis() - start + " ms");
		
		start =  System.currentTimeMillis();
		Weight comp2 = WeightUtil.compressRandomly2(w, 0.80f);
		System.out.println(System.currentTimeMillis() - start + " ms");

		for(int i = 0; i < comp2.sparseData.data.length; i++) {
			if(Float.isNaN(comp2.sparseData.data[i]))
				System.out.println(w.floatData[i]);
		}
		
		List<Weight> complist = new ArrayList<Weight>();
		complist.add(comp2);
		
		
		start =  System.currentTimeMillis();
		WeightStat.generateWeightStat(list);
		System.out.println(System.currentTimeMillis() - start + " ms");
		
		start =  System.currentTimeMillis();
		WeightStat s = WeightStat.generateWeightStat(list, complist);
		System.out.println(System.currentTimeMillis() - start + " ms");
		System.out.println(s.getMean());
		System.out.println(s.getStdev());
		
		start =  System.currentTimeMillis();
		WeightUtil.meanWeightList(list);
		System.out.println(System.currentTimeMillis() - start + " ms");
		
		//start =  System.currentTimeMillis();
		//Weight decomp = WeightUtil.decompress(comp2);
		//System.out.println(System.currentTimeMillis() - start + " ms");
		
		//for(int i =0 ; i < decomp.floatData.length;i++)
		//	System.out.println(decomp.floatData[i]);
	}
}
