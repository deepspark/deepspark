package org.acl.deepspark.utils;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.acl.deepspark.data.Weight;

public class WeightUtilTest {

	public static void main(String[] args) throws Exception {
		ObjectInputStream in = new ObjectInputStream(new FileInputStream("ext/seq_model.weights")); 
		List<Weight> list = (List<Weight>) in.readObject();
		
		//float th = WeightUtil.findThresholdFull(list, 0.9f);
		float th = WeightUtil.findThreshold(list, null, 0.9f);
		/*
		for(int i = 0; i < 100; i++) {
			
		}*/
		
		long start = System.currentTimeMillis();
		List<Weight> compressed = WeightUtil.compressWeightList(list, th,null);
		long end = System.currentTimeMillis();
		
		int org= 0;
		int comp = 0;
		for(int i = 0; i < compressed.size(); i++) {
			org += list.get(i).floatData.length;
			comp += compressed.get(i).sparseData.data.length;
		}
		System.out.println(String.format("deflated to %.2f%%", 1.25f * comp / org * 100));		
		
		System.out.println(String.format("compress %d msec", end-start));
		
		start = System.currentTimeMillis();
		Iterator<Weight> w = compressed.iterator();
		List<Weight> decom = new ArrayList<Weight>();
		while(w.hasNext())
			decom.add(WeightUtil.decompress(w.next()));
		end = System.currentTimeMillis();
		System.out.println(String.format("decompress %d msec", end-start));
	}

}
