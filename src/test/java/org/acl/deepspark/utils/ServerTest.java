package org.acl.deepspark.utils;

import java.io.IOException;

import org.acl.deepspark.nn.CaffeNet;
import org.acl.deepspark.nn.async.ParameterServer;

public class ServerTest {
	public static void main(String args[]) throws IOException {
		CaffeNet net = new CaffeNet(args[0]);
		
		ParameterServer a = new ParameterServer(net, 38470, 38471);
		a.startServer();
		
		
		
		a.stopServer();
	}
}
