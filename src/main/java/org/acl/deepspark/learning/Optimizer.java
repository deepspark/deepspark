package org.acl.deepspark.learning;

import java.util.List;

import org.acl.deepspark.data.Weight;

public interface Optimizer {
	public void setWeights(List<Weight> weights) throws Exception;
	//public void setGradients(List<Weight> gradients) throws Exception;
	public List<Weight> getWeights();
	//public List<Weight> getGradients();
	public void train();
	public float[] test();
}
