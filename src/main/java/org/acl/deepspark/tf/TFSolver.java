package org.acl.deepspark.tf;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.acl.deepspark.data.Minibatch;
import org.acl.deepspark.data.Weight;
import org.acl.deepspark.nn.async.easgd.ParameterEASGDClient;
import org.acl.deepspark.nn.async.easgd.ParameterEASGDServer;
import org.acl.deepspark.tf.TFNet;
import org.acl.deepspark.utils.DeepSparkConf;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.VoidFunction;

public class TFSolver implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1124201106949277521L;

	private transient TFNet net;

    private String host;
    private int port;
    
    private float movingRate;
	private int decayStep = 0;
	private float decayRate = 1;
	private int decayLimit = 0;

	private DeepSparkParam param;

	private String graphPath;

	public TFSolver(TFNet net, String host, int port, DeepSparkConf.DeepSparkParam param, String graphPath) {
		this.net = net;
        this.host = host;
        this.port = port;
        this.param = param;
        this.graphPath = graphPath;
        
        movingRate = param.getMovingRate();
        decayStep = param.getDecayStep() / param.getPeriod();
        decayRate = param.getDecayRate();
        decayLimit = param.getDecayLimit();
    }
	
	private long startTime;
	
	private void printLog(String mesg) {
		System.out.println(String.format("[%s %f]solver: %s", new Timestamp(System.currentTimeMillis()), (float) (System.currentTimeMillis() - startTime ) / 1000, mesg));
	}
	
	public void train(JavaRDD<Minibatch> data, String path, final int numWorker) throws IOException {
		ParameterEASGDServer server = new ParameterEASGDServer(net, port, numWorker, param);
                
		final List<Weight> init_weight = net.getWeights();
        final int iteration = param.getTfIteration();
        final int period = param.getPeriod();
        final String graph = new String(Files.readAllBytes(Paths.get(graphPath)));
        
        // define learner
        VoidFunction<Iterator<Minibatch>> learner = new VoidFunction<Iterator<Minibatch>>() {
        	/**
			 * 
			 */
			private static final long serialVersionUID = -8266771633994786231L;
			private transient TFNet localNet;
        	
			@Override
			public void call(Iterator<Minibatch> arg0) throws Exception {
				// TODO Auto-generated method stub
				// preparing data set
				
				List<Minibatch> data_list = new LinkedList<Minibatch>();
				while(arg0.hasNext()) {
					data_list.add(arg0.next());
				}
				int list_size = data_list.size();
				System.out.println(String.format("%d samples loaded", list_size));
				
				
				// iterative training
				for(int i = 0; i < iteration; i++) {
					// sample batch set
					Minibatch d = data_list.get(i % list_size);
					
					// launch training
					localNet.setFloatInputs(Arrays.asList(d.data, d.label, new float[]{0.5f}),0);
					localNet.train();
					
					// parameter exchange
					if(i % period == 0) {
						List<Weight> weights = localNet.getWeights();
						localNet.setWeights(ParameterEASGDClient.exchangeWeight(host, port, weights, param.getMovingRate(), param.getFixedMovingRate(),
								param.getCompressed(), localNet.getVariableProfile(), param.getCompressParam().getRatio(),
								param.getCompressParam().getApplyOption()));
					}
				}
				
				// finalize training
				System.out.println("Job's done");
			}
			
			private void readObject(ObjectInputStream in) throws Exception {
				in.defaultReadObject();
				localNet = new TFNet(param, graph);
				localNet.setWeights(init_weight);
			}
		};
		
		server.startServer();
        startTime = new Date().getTime();
        printLog("Start Learning...");
        data.foreachPartition(learner);
        printLog("Training done");
	}
	
	public TFNet getTFNet() {
		return net;
	}
}
