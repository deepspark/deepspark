package org.acl.deepspark.tf;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.StringArray;
import com.sun.jna.Structure;

public class RunOption extends Structure implements Structure.ByReference, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8198557788532349150L;
	
	public Pointer inputNames;
	public int numInputs;
	public Pointer targetNames;
	public int numTargets;
	public Pointer outputNames;
	public int numOutputs;
	
	@Override
	protected List getFieldOrder() {
		return Arrays.asList(new String[] {"inputNames","numInputs",
				"targetNames","numTargets","outputNames","numOutputs",});
	}
	
	public void setInputNames(String[] names) {
		inputNames = new StringArray(names);
		numInputs = names.length;
	}
	
	public void setTargetNames(String[] names) {
		targetNames = new StringArray(names);
		numTargets = names.length;
	}
	
	public void setOutputNames(String[] names) {
		outputNames = new StringArray(names);
		numOutputs = names.length;
	}
}
