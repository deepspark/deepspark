package org.acl.deepspark.tf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.acl.deepspark.data.Weight;
import org.acl.deepspark.learning.Optimizer;
import org.acl.deepspark.nn.async.easgd.CoordinateProfile;
import org.acl.deepspark.tf.TFWrapper.OutputVector;
import org.acl.deepspark.tf.TFWrapper.Session;
import org.acl.deepspark.tf.TFWrapper.Tensor;
import org.acl.deepspark.tf.TFWrapper.Tensors;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam.Inputs;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam.Variables;
import org.acl.deepspark.utils.WeightUtil;

public class TFNet implements Optimizer, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5740006499212700080L;
	public static final String SET_WEIGHT = "set_weight";
	public static final String GET_WEIGHT = "get_weight";
	public static final String SET_GRAD = "set_gradient";
	public static final String GET_GRAD = "get_gradient";
	public static final String TRAIN = "train";
	public static final String TEST = "test";
	public static final String INIT = "init";
	
	private List<ParameterShape> shapes;
	
	private Map<String, RunOption> runOptions;
	
	private Session session;
	private OutputVector outputs;
	private Tensors inputData;
	private Tensors variables;
	private List<Variables> variable_specs;
	private List<Inputs> inputs;
	
	public void setParamShape(List<ParameterShape> list) {
		shapes = list;
	}
	
	public void addCustomRunOption(String name, RunOption opts) {
		runOptions.put(name, opts);
	}

	public RunOption getRunOption(String name) {
		return runOptions.get(name);
	}
	
	public void setBatchSize(int batchSize) {
		if(inputData != null) {
			//release before
			for(int i =0; i < inputs.size() ; i++) {
				inputData.getTensor(i).destroy();
			}
			inputData.destroy();
		}
		
		inputData = new Tensors(inputs.size());
		for(int i =0; i < inputs.size() ; i++) {
			List<Integer> dim_list = inputs.get(i).getDimList();
			Iterator<Integer> iter = dim_list.iterator();
			Integer[] dim = new Integer[dim_list.size()];
			
			// batch size is the first element of input dimension
			int index= 0;
			dim[index++] = batchSize; if(iter.hasNext()) iter.next();
			
			while(iter.hasNext()) {
				dim[index++] = iter.next();
			}
			
			inputData.setTensor(new Tensor(dim, inputs.get(i).getType().getNumber()), i);
		}
	}
	
	private void init(DeepSparkParam params, String path) {
		session = new Session(path);
		outputs = new OutputVector();
		
		// init option setting
		RunOption initOpt = new RunOption();
        initOpt.setInputNames(new String[0]);
        initOpt.setOutputNames(new String[0]);
        initOpt.setTargetNames(new String[]{"init"});
        runOptions.put(INIT, initOpt);
		
		// Train/Test tensor settings
		RunOption trainOpt = new RunOption();
		RunOption testOpt = new RunOption();
		inputs = params.getInputsList();
		String[] input_names = new String[inputs.size()];
		inputData = new Tensors(inputs.size());
		for(int i =0; i < inputs.size(); i++) {
			input_names[i] = inputs.get(i).getName();
			
			List<Integer> dim_list = inputs.get(i).getDimList();
			inputData.setTensor(new Tensor(dim_list.toArray(new Integer[dim_list.size()]), inputs.get(i).getType().getNumber()), i);
		}
		
		trainOpt.setInputNames(input_names);
		trainOpt.setTargetNames(new String[]{"train"});
		trainOpt.setOutputNames(new String[0]);
		
		testOpt.setInputNames(input_names);
		testOpt.setTargetNames(new String[0]);
		testOpt.setOutputNames(new String[]{"output"});
		
		runOptions.put(TRAIN, trainOpt);
		runOptions.put(TEST, testOpt);
				
		// get/set weight tensor settings
		RunOption get_weightOpt = new RunOption();
		RunOption set_weightOpt = new RunOption();
		
		variable_specs = params.getVariablesList();
		
		get_weightOpt.setInputNames(new String[0]);
		get_weightOpt.setTargetNames(new String[0]);
				
		String[] w_inputs = new String[variable_specs.size()];
		String[] w_outputs = new String[variable_specs.size()];
		
		variables = new Tensors(variable_specs.size());
		List<ParameterShape> var_dim = new LinkedList<ParameterShape>();
		for(int i = 0; i < variable_specs.size(); i++) {
			w_inputs[i] = "assign_" + variable_specs.get(i).getName();
			w_outputs[i] = variable_specs.get(i).getName();
			
			List<Integer> dim_list = variable_specs.get(i).getDimList();
			
			int var_length = 1;
			
			for(Integer e: dim_list) {
				var_length *= e.intValue();
			}
			
			ParameterShape var_shape = new ParameterShape(var_length, i);
			var_dim.add(var_shape);
			
			variables.setTensor(new Tensor(dim_list.toArray(new Integer[dim_list.size()]), variable_specs.get(i).getType().getNumber()), i);
		}
		
		this.setParamShape(var_dim);
		
		get_weightOpt.setOutputNames(w_outputs);
		
		set_weightOpt.setInputNames(w_inputs);
		set_weightOpt.setTargetNames(new String[]{"modify"});
		set_weightOpt.setOutputNames(new String[0]);
		
		runOptions.put(SET_WEIGHT, set_weightOpt);
		runOptions.put(GET_WEIGHT, get_weightOpt);
		
		runAction(Tensors.EMPTY, INIT);
	}
	
	public TFNet(DeepSparkParam params, String path_to_graph, boolean isBin) {
		runOptions = new HashMap<String, RunOption>();
		init(params,path_to_graph);
	}
	
	public TFNet(DeepSparkParam params, String path_to_graph) {
		runOptions = new HashMap<String, RunOption>();
		init(params,path_to_graph);
	}
	
	public void runAction(Tensors inputTensors, String option) {
		session.run(inputTensors, outputs, runOptions.get(option));
	}
	
	public void runAction(String option) {
		session.run(inputData, outputs, runOptions.get(option));
	}
	
	@Override
	public void setWeights(List<Weight> weights) throws Exception {
		for(int i = 0; i< weights.size();i++) {
			if(weights.get(i).type == Tensor.FLOAT)
				variables.getTensor(i).setData(weights.get(i).floatData);
		}
		
		runAction(variables, SET_WEIGHT);
	}

	public void setWeights(List<Weight>[] weights) throws Exception {
		Iterator<Weight>[] iter = new Iterator[weights.length];
		for(int i = 0; i< weights.length; i++) {
			iter[i] = weights[i].iterator();
		}
		
		int index = 0;
		while(iter[0].hasNext()) {
			Tensor t = variables.getTensor(index);
			
			for(int i =0; i < iter.length; i++) {
				Weight w = iter[i].next();
				t.setData(w.floatData, w.offset);
			}
			
			index++;
		}
		
		runAction(variables, SET_WEIGHT);
	}
//	
//	@Override
//	public void setGradients(List<Weight> gradients) throws Exception {
//		for(int i = 0; i< gradients.size();i++) {
//			if(gradients.get(i).type == Tensor.FLOAT)
//				variables.getTensor(i).setData(gradients.get(i).floatData);
//		}
//		
//		runAction(variables, SET_GRAD);
//	}

	@Override
	public List<Weight> getWeights() {
		runAction(new Tensors(0), GET_WEIGHT);
		
		List<Weight> list = new ArrayList<Weight>();
		
		for(int i = 0; i < outputs.size(); i++) {
			Weight w = new Weight();
			w.layerIndex = i;
			w.offset = 0;
			w.type = variable_specs.get(i).getType().getNumber();
			if(w.type == Tensor.FLOAT)
				w.floatData = outputs.getOutputDataAsFloat(i, shapes.get(i).getLength());
			list.add(w);
		}
		
		return list;
	}
	
	public void validateWeight(List<CoordinateProfile[]> profiles, int numWorker) {
		List<Weight> entire = getWeights();
		List<Weight>[] splited = getWeights(profiles, numWorker);
		
		for(int i = 0; i < entire.size(); i++) {
			Weight e = entire.get(i);
			
			int index = 0;
			for(int j = 0; j < splited.length; j++) {
				Weight w = splited[j].get(i);
				for(int k = 0; k < w.floatData.length; k++) {
					if(e.floatData[index] != w.floatData[k]) {
						System.out.println(String.format("Warning layer %d, index %d: %f vs %f (offset: %d, subindex: %d)", i,index,e.floatData[index], w.floatData[k], w.offset, k));
					}
					index++;
				}
			}
		}
	}
	
	public List<Weight>[] getWeights(List<CoordinateProfile[]> profiles, int numWorker) {
		runAction(new Tensors(0), GET_WEIGHT);
		
		List<Weight>[] list = new List[numWorker];
		for (int i = 0; i < numWorker; i++)
			list[i] = new ArrayList<Weight>(); 
		
		Iterator<Variables> vIter = variable_specs.iterator();
		Iterator<CoordinateProfile[]> pIter = profiles.iterator();
		
		int index = 0;
		while(vIter.hasNext()) {
			Variables v = vIter.next();
			CoordinateProfile[] p = pIter.next();
			
			for(int i =0; i < p.length; i++) {
				Weight w = new Weight();
				w.layerIndex = index;
				w.offset = p[i].offset;
				w.type = v.getType().getNumber();
				if(w.type == Tensor.FLOAT)
					w.floatData = outputs.getOutputDataAsFloat(index, p[i].length, p[i].offset);
				list[i].add(w);
			}
			index++;
		}
		
		return list;
	}

//	@Override
//	public List<Weight> getGradients() {
//		runAction(new Tensors(0), GET_GRAD);
//		
//		List<Weight> list = new ArrayList<Weight>();
//		
//		for(int i = 0; i < outputs.size(); i++) {
//			Weight w = new Weight();
//			w.layerIndex = i;
//			w.offset = 0;
//			w.type = variable_specs.get(i).getType().getNumber();
//			if(w.type == Tensor.FLOAT)
//				w.floatData = outputs.getOutputDataAsFloat(i, shapes.get(i).getLength());
//			list.add(w);
//		}
//		
//		return list;
//	}

	public void setFloatInputs(List<float[]> data, int offset) throws Exception {
		for(int i = 0; i < data.size(); i++) { 
			setInput(data.get(i), i + offset);
		}
	}
	
	public void setIntInputs(List<int[]> data, int offset) throws Exception {
		for(int i = 0; i < data.size(); i++) { 
			setInput(data.get(i),i + offset);
		}
	}
	
	public void setInput(float[] data, int i ) throws Exception {
		inputData.getTensor(i).setData(data);
	}
	
	public void setInput(int[] data, int i ) throws Exception {
		inputData.getTensor(i).setData(data);
	}
	
	public float[] getResult(int idx,int length) {
		return outputs.getOutputDataAsFloat(idx, length);
	}
	
	@Override
	public void train() {
		runAction(inputData, TRAIN);
	}
	
	@Override
	public float[] test() {
		runAction(inputData, TEST);
		
		return null;
	}
	
	public List<Variables> getVariableProfile() {
		return variable_specs;
	}

	public void getWeightWithMasking2(List<Weight>[] oldWeight, List<float[]>[] diffMap, float th) {
		Iterator<Weight> currentIter = getWeights().iterator();
		Iterator<Weight>[] wIter = new Iterator[oldWeight.length];
		Iterator<float[]>[] diffIter = new Iterator[diffMap.length];
		for(int i = 0; i < diffIter.length; i++) {
			diffIter[i] = diffMap[i].iterator();
			wIter[i] = oldWeight[i].iterator();
		}
		
		while(wIter[0].hasNext()) {
			Weight c = currentIter.next();
			int offset = 0;
			for(int i = 0; i < diffIter.length;i++) {
				Weight w = wIter[i].next();
				float[] diff = diffIter[i].next();
				for(int k = 0; k < diff.length; k++) {
					if(diff[k] > th) {
						w.floatData[k] = c.floatData[offset+k];
					}
				}
				
				offset += diff.length;
			}
		}
	}

	public void addWeights(List<Weight>[] weights, List<CoordinateProfile[]> profiles, int numHost) {
		List<Weight>[] current = getWeights(profiles, numHost);
		
		for(int i = 0; i < weights.length; i++) {
			Iterator<Weight> compIter = current[i].iterator();
			Iterator<Weight> commIter = weights[i].iterator();
			
			while(compIter.hasNext()) {
				Weight comp = compIter.next();
				Weight comm = commIter.next();
				
				for(int k = 0; k < comp.floatData.length; k++) {
					comp.floatData[k] += comm.floatData[k];
				}
			}
		}
		
		try {
			setWeights(current);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
