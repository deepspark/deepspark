package org.acl.deepspark.tf;

import java.io.Serializable;

public class ParameterShape implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6130833913800442078L;
	private int length;
	private int layerIdx;
	
	public ParameterShape(int length, int layerIdx) {
		this.length = length;
		this.layerIdx = layerIdx;
	}

	public int getLength() {
		return length;
	}

	public int getLayerIdx() {
		return layerIdx;
	}	
}
