package org.acl.deepspark.tf;

import java.io.Serializable;

import org.apache.commons.lang.ArrayUtils;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

public class TFWrapper implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7844102156460453045L;

	private interface TFInterface extends Library {
		public TFInterface INSTANCE = (TFInterface) Native.loadLibrary("libtfwrapper.so", TFInterface.class);
		
		public Pointer createSession(String path, int isBin);
		public void closeSession(Pointer ptr);
		public Pointer makeTensor(int[] tensorDim, int numDim, int type);
		public void destroyTensor(Pointer ptr);
		public void setFloatData(Pointer ptr, float[] data, int num, int offset);
		public void setIntData(Pointer ptr, int[] data, int num, int offset);
		public Pointer getFloatData(Pointer ptr);
		public Pointer getIntData(Pointer ptr);
		public void runAction(Pointer session, Pointer inputTensor, Pointer outputVector, RunOption options);

		// tensor vector 
		public Pointer makeVector();
		public void clearVector(Pointer ptr);
		public int vectorSize(Pointer ptr);
		public Pointer getOutputData(Pointer ptr, int idx, int type, int offset);
		public void destroyVector(Pointer ptr);
		
		
		// tensor array 
		public Pointer makeTensors(int size);
		public void destroyTensors(Pointer ptr);
		public void setTensor(Pointer arr, Pointer t, int idx);
		public Pointer getTensor(Pointer arr, int idx);
		
		// misc.		
		public void checkRunOption(RunOption a);
	}
	
	public static void checkRunOption(RunOption a) {
		TFInterface.INSTANCE.checkRunOption(a);
	}
	
	public static class Session {
		private Pointer ptr;
		
		public Session(String proto, boolean isBin) {
			int bin = 0;
			if(isBin) {
				bin = 1;
			}
			ptr = TFWrapper.TFInterface.INSTANCE.createSession(proto, bin);
		}
		
		public Session(String proto) {
			this(proto, false);
		}
		
		public void run(Tensors input, OutputVector output, RunOption options) {
			output.clear();
			TFWrapper.TFInterface.INSTANCE.runAction(ptr, input.ptr, output.ptr, options);
		}
	}
	
	public static class Tensors {
		private Pointer ptr;
		private Tensor[] tensors;
		public static Tensors EMPTY = new Tensors(0); 
		
		public Tensors(int size) {
			if(size != 0) {
				tensors = new Tensor[size];
				ptr = TFInterface.INSTANCE.makeTensors(size);
			} else {
				tensors = new Tensor[0];
				ptr = Pointer.NULL;
			}
		}
		
		public void setTensor(Tensor t, int idx) {
			tensors[idx] = t;
			TFInterface.INSTANCE.setTensor(ptr, t.ptr, idx);
		}
		
		public Tensor getTensor(int idx) {
			return tensors[idx];
		}
		
		public void destroy() {
			TFInterface.INSTANCE.destroyTensors(ptr);
			ptr = null;
		}
		
		@Override
		public void finalize() {
			if(ptr != null)
				destroy();
		}
	}
	
	public static  class OutputVector {
		private Pointer ptr;
		
		public OutputVector() {
			ptr = TFInterface.INSTANCE.makeVector();
		}
		
		public void clear() {
			TFInterface.INSTANCE.clearVector(ptr);
		}
		
		public int size() {
			return TFInterface.INSTANCE.vectorSize(ptr);
		}
		
		public float[] getOutputDataAsFloat(int idx, int length) {
			return TFInterface.INSTANCE.getOutputData(ptr, idx, Tensor.FLOAT,0).getFloatArray(0, length);
		}
		
		public float[] getOutputDataAsFloat(int idx, int length, int offset) {
			return TFInterface.INSTANCE.getOutputData(ptr, idx, Tensor.FLOAT,offset).getFloatArray(0, length);
		}
		
		public int[] getOutputDataAsInteger(int idx, int length) {
			return TFInterface.INSTANCE.getOutputData(ptr, idx, Tensor.INT32,0).getIntArray(0, length);
		}
		
		@Override
		public void finalize() {
			if(ptr != null)
				destroy();
		}

		public void destroy() {
			TFInterface.INSTANCE.destroyVector(ptr);
			ptr = null;
		}
	}
	
	public static class Tensor {
		private Pointer ptr;
		private int length;
		private int type;
		
		public static final int FLOAT = 0;
		public static final int INT32 = 1;
		
		public Tensor(int[] dim, int type) {
			length = 1;
			
			for(int i : dim)
				length *= i;
			
			ptr = TFInterface.INSTANCE.makeTensor(dim, dim.length, type);
			this.type = type;
		}
		
		public Tensor(Integer[] dim, int type) {
			length = 1;
			
			for(int i : dim)
				length *= i;
			
			ptr = TFInterface.INSTANCE.makeTensor(ArrayUtils.toPrimitive(dim), dim.length, type);
			this.type = type;
		}
		
		public void setData(float[] data) throws Exception {
			setData(data, 0);
		}
		
		public void setData(float[] data, long offset) throws Exception {
			Pointer p = TFInterface.INSTANCE.getFloatData(ptr);
			p.write(offset * 4, data, 0, data.length);
		}
		
		public void setData(int[] data) throws Exception {
			Pointer p = TFInterface.INSTANCE.getIntData(ptr);
			p.write(0, data, 0, data.length);
			
			/*
			if(length == data.length) {
				TFInterface.INSTANCE.setIntData(ptr, data, length,0);
			} else {
				throw new Exception(String.format("size mismatch (%d vs %d)", length, data.length));
			}*/
		}
		
		public int getType() {
			return type;
		}
		
		public float[] getFloatData() {
			Pointer data_ptr = TFInterface.INSTANCE.getFloatData(ptr);
			return data_ptr.getFloatArray(0, length);
		}
		
		public int[] getIntData() {
			Pointer data_ptr = TFInterface.INSTANCE.getIntData(ptr);
			return data_ptr.getIntArray(0, length);
		}
		
		public void destroy() {
			TFInterface.INSTANCE.destroyTensor(ptr);
			ptr = null;
		}
		
		public int getLength() {
			return length;
		}
		
		@Override
		public void finalize() {
			if(ptr != null)
				destroy();
		}
	}
}
