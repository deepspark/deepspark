package org.acl.deepspark.tf;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.acl.deepspark.data.LMDBReader2;
import org.acl.deepspark.data.SeqBatch;
import org.acl.deepspark.data.SeqRecord;

public class SeqRecordLoader implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6060324411941489291L;
	private BlockingQueue<SeqBatch> queue;
	private boolean stopFlag; 
	private Thread loader;
	private LMDBReader2[] reader;
	private int batchSize;
	private int[] srcSize;
	private int[] dstSize;
	
	static final int PAD = 0;
	static final int GO = 1;
	static final int EOS = 2;
	static final int UNK = 3;
	
	public SeqRecordLoader(int capacity, int batchSize,
			int[] src_size, int[] dst_size, 
			LMDBReader2[] sources) {
		queue = new ArrayBlockingQueue<SeqBatch>(capacity);
		
		
		this.batchSize = batchSize;
		this.srcSize = src_size;
		this.dstSize = dst_size;
		this.reader = sources;
		
		stopFlag = false;
		loader = new LoaderThread();
	}
	
	public void setBatchSize(int batchsize) {
		this.batchSize = batchsize;
	}
	
	public void startLoader() {
		stopFlag = false;
		loader.start();
	}
	
	public void stopLoader() throws InterruptedException {
		stopFlag = true;
		loader.interrupt();
		loader.join();
	}
	
	public SeqBatch getNextBatch() throws InterruptedException {
		return queue.take();
	}
	
	private class LoaderThread extends Thread {
		@Override
		public void run() {
			while(!stopFlag) {
				try {
					queue.offer(nextBatch());
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
		
		private SeqBatch nextBatch() throws ClassNotFoundException, IOException {
			Random r = new Random();
			//select bucket
			int bucket_id = r.nextInt(4);
			int[][] encode_batch = new int[srcSize[bucket_id]][batchSize];
			int[][] decode_batch = new int[dstSize[bucket_id]+1][batchSize];
			float[][] target_weight = new float[dstSize[bucket_id]][batchSize];
			
			// read from bucket
			for(int m = 0; m < batchSize; m++) {
				SeqRecord sample = reader[bucket_id].getSample();
				
				// encoder data
				while(sample.encode_data.size() < srcSize[bucket_id]) {
					sample.encode_data.add(PAD);
				}
				
				Collections.reverse(sample.encode_data);
				
				int k = 0;
				Iterator<Integer> iter = sample.encode_data.iterator();
				while(iter.hasNext()) {
					encode_batch[k++][m]= iter.next();
				}
				
				// decoder data
				sample.decode_data.add(0,GO);
				while(sample.decode_data.size() < dstSize[bucket_id]) {
					sample.decode_data.add(PAD);
				}
				k = 0;
				iter = sample.decode_data.iterator();
				while(iter.hasNext()) {
					decode_batch[k++][m] = iter.next();
				}
				decode_batch[k][m] = 0;
				
				// target_weight
				for(k = 0; k < dstSize[bucket_id]; k++) {
					int target = decode_batch[k+1][m];
					if(k == dstSize[bucket_id] -1 || target == PAD)
						target_weight[k][m] = 0.0f;
					else
						target_weight[k][m] = 1.0f;
				}
			}
			
			SeqBatch batch = new SeqBatch();
			batch.bucketId = bucket_id;
			batch.batchSize = batchSize;
			batch.encodeBatch = encode_batch;
			batch.decodeBatch = decode_batch;
			batch.targetWeight = target_weight;
			
			return batch;
		}
	}
}
