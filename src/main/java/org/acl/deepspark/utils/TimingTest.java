package org.acl.deepspark.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.acl.deepspark.data.Weight;
import org.acl.deepspark.nn.CaffeNet;
import org.acl.deepspark.nn.async.easgd.CoordinateProfile;

public class TimingTest {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		CaffeNet net = new CaffeNet("solver_test.txt");
		net.getVariableProfile();
		List<Weight> list = net.getWeights();
		
		List<CoordinateProfile[]> profiles = new ArrayList<CoordinateProfile[]>();
		
		Iterator<Weight> wIter = list.iterator();
		
		int numWorker = 8;
		
		while(wIter.hasNext()) {
			CoordinateProfile[] profile = new CoordinateProfile[numWorker];
			Weight w = wIter.next();
			int partSize = w.floatData.length / numWorker;
			
			for(int i = 0; i < numWorker;i++) {
				profile[i] = new CoordinateProfile();
				profile[i].offset = i * partSize;
				
				if(i == (numWorker-1)) // last part
					profile[i].length = w.floatData.length - profile[i].offset;
				else
					profile[i].length = partSize;
			}
			
			profiles.add(profile);
		}
		
		List<Weight>[] splited = net.getWeights(profiles, numWorker);
		
		List<float[]>[] diffMap = new List[numWorker];
		for(int i = 0; i < diffMap.length;i++) {
			diffMap[i] = new ArrayList<float[]>();
			
			Iterator<Weight> w = splited[i].iterator();
			while(w.hasNext()) {
				diffMap[i].add(w.next().floatData);
			}
		}
		
		splited = net.getWeights(profiles, numWorker);	
		
		float th = WeightUtil.findThresholdFromDiff(diffMap, net.getVariableProfile(), 0.7f);
		
		
		// test
		net.getWeightWithMasking(splited, diffMap, th);
		net.getWeightWithMasking(splited, diffMap, th);
		net.getWeightWithMasking(splited, diffMap, th);
	}

}
