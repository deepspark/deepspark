package org.acl.deepspark.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.acl.deepspark.data.SparseEntries;
import org.acl.deepspark.data.Weight;
import org.acl.deepspark.data.WeightStat;
import org.acl.deepspark.tf.TFWrapper.Tensor;
import org.acl.deepspark.utils.DeepSparkConf.CompressionParam.Regularizer;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam.Variables;

public class WeightUtil { 
	public static Weight compress(Weight original, float threshold) {
		Weight compressed = new Weight();
		
		// currently, only for float type
		compressed.layerIndex = original.layerIndex;
		compressed.offset = original.offset;
		compressed.type = original.type;
		compressed.sparseData = new SparseEntries();
		
		byte[] iBuffer = new byte[original.floatData.length];
		float[] vBuffer = new float[original.floatData.length];
		
		// pruning, only for float type
		compressed.originalLength = original.floatData.length;
		byte arr = 0;
		int index = 0;
		for(int i = 0; i < compressed.originalLength ; i++) {
			float value = original.floatData[i];
			if(Math.abs(value) > threshold) { // meaningful
				vBuffer[index] = value;
				iBuffer[index++] = arr;
				arr = 0;

				continue;
			} else {
				if( (byte) 0xff == arr ) {
					vBuffer[index] = 0f;
					iBuffer[index++] = arr;
					arr = 0;						
				} else {
					arr++;
					continue;
				}
			}
		}
		
		compressed.sparseData.index = Arrays.copyOfRange(iBuffer,0 , index);
		compressed.sparseData.data = Arrays.copyOfRange(vBuffer,0 , index);
		
		return compressed;
	}
	
	public static Weight compressForServer(Weight local, Weight remote) {
		Weight compressed = new Weight();
		
		// currently, only for float type
		compressed.layerIndex = local.layerIndex;
		compressed.offset = local.offset;
		compressed.type = local.type;
		compressed.sparseData = new SparseEntries();
		
		byte[] iBuffer = new byte[remote.sparseData.data.length];
		float[] vBuffer = new float[remote.sparseData.data.length];
		
		// pruning, only for float type
		compressed.originalLength = local.floatData.length;
		
		int index = 0;
		for(int i =0; i < iBuffer.length; i++) {
			iBuffer[i] = remote.sparseData.index[i];
			index += 0xff & iBuffer[i]; 
			if(Float.isNaN(remote.sparseData.data[i]))
				vBuffer[i] = Float.NaN;
			else
				vBuffer[i] = local.floatData[index];
			index++;
		}
		
		compressed.sparseData.index = iBuffer;
		compressed.sparseData.data = vBuffer;
		
		return compressed;
	}
	
	public static Weight compressWithDiff(Weight original, float threshold, float[] diff) {
		Weight compressed = new Weight();
		
		// currently, only for float type
		compressed.layerIndex = original.layerIndex;
		compressed.offset = original.offset;
		compressed.type = original.type;
		compressed.sparseData = new SparseEntries();
		
		byte[] iBuffer = new byte[original.floatData.length];
		float[] vBuffer = new float[original.floatData.length];
		
		// pruning, only for float type
		compressed.originalLength = original.floatData.length;
		byte arr = 0;
		int index = 0;
		for(int i = 0; i < compressed.originalLength ; i++) {
			float value = diff[i];
			if(value > threshold) { // meaningful
				vBuffer[index] = original.floatData[i];
				iBuffer[index++] = arr;
				arr = 0;

				continue;
			} else {
				if( (byte) 0xff == arr ) {
					vBuffer[index] = Float.NaN;
					iBuffer[index++] = arr;
					arr = 0;						
				} else {
					arr++;
					continue;
				}
			}
		}
		
		compressed.sparseData.index = Arrays.copyOfRange(iBuffer,0 , index);
		compressed.sparseData.data = Arrays.copyOfRange(vBuffer,0 , index);
		
		return compressed;
	}
	
	public static void applyDiffWithMomentum(List<Weight> local, List<Weight> remote, List<float[]> res, float alpha) {
		// make new diff list
		// diff values is always positive
		if(res.isEmpty()) {
			Iterator<Weight> iter = local.iterator();
			while(iter.hasNext()) {
				Weight w = iter.next();
				float[] array = new float[w.floatData.length];
				res.add(array);
			}
			alpha = 0.0f;
		}
		
		Iterator<Weight> iter1 = local.iterator();
		Iterator<Weight> iter2 = remote.iterator();
		Iterator<float[]> iterR = res.iterator();
		while(iter1.hasNext()) {
			Weight weight1 = iter1.next();
			Weight weight2 = iter2.next();
			float[] arr = iterR.next();
			
			if(weight2.sparseData == null) { 
				// for dense matrix
				for(int i = 0; i < weight1.floatData.length; i++) {
					arr[i] *= alpha;
					arr[i] += (1 - alpha) * Math.abs( (weight1.floatData[i] - weight2.floatData[i]) );
				}
			} else { 
				// for sparse matrix
				int index = 0;
				SparseEntries e = weight2.sparseData;
				for(int i = 0; i < e.data.length; i++) {
					if(!Float.isNaN(e.data[i])) {
						index += 0xff & e.index[i];
						arr[index] *= alpha;
						arr[i] += (1- alpha) * Math.abs( (weight1.floatData[index] - e.data[i]) );
					} else {
						index += 0xff & e.index[i];
					}
					index++;
				}
			}
		}
	}
	
	public static float findThreshold(List<Weight> w_list, List<Variables> weightProfile, float cut_ratio) throws Exception {
		final int MAX_SAMPLE = 10000;
//		final float RATIO = 0.01f;
		Random rand = new Random();
		
		if(w_list.size() != weightProfile.size())
			throw new Exception("size mismatch");
		
		List<Float> weights = new ArrayList<Float>();
		int total = 0;
		Iterator<Weight> w_iter = w_list.iterator();
		Iterator<Variables> pIter = weightProfile.iterator();
		
		while(w_iter.hasNext()) {
			Weight w = w_iter.next();
			Variables profile = pIter.next();
			if(!profile.getSkipCompress())
				total +=  w.floatData.length;
		}
		
		
		float th = 0;
		Date startTime = new Date();
		
		w_iter = w_list.iterator();
		pIter = weightProfile.iterator();
		while(w_iter.hasNext()) {
			Weight w = w_iter.next();
			Variables profile = pIter.next();
			
			if(!profile.getSkipCompress()) {
				int maxIter = (int) (MAX_SAMPLE * ((float) w.floatData.length / total));
				if(maxIter < w.floatData.length) {
					Set<Integer> selected = new HashSet<Integer>();
					for(int i = 0 ; i < maxIter;) {
						int index = rand.nextInt(w.floatData.length);
						if(!selected.contains(index)) {
							selected.add(index);
							weights.add(Math.abs(w.floatData[index]));
							i++;
						}
					}
				} else {
					for(int i = 0 ; i < w.floatData.length;i++) {
						weights.add(Math.abs(w.floatData[i]));
					}
				}
			}	
		}
		Collections.sort(weights);
		
		th = weights.get((int) (weights.size() * cut_ratio) - 1);
		
		Date endTime = new Date();
		System.out.println(String.format("find %f for %d ms", th, 
				(endTime.getTime() - startTime.getTime())));
		return th;
	}
	
	
	
	public static float findThreshold(List<Weight>[] w_list, List<Variables> weightProfile, float cut_ratio) throws Exception {
		final int MAX_SAMPLE = 10000;
		Random rand = new Random();
		
//		if(w_list.size() != variable_profiles.size())
//			throw new Exception("size mismatch");
		
		List<Float> weights = new ArrayList<Float>();
		int total = 0;
		for(int i = 0; i < w_list.length;i++) {
			Iterator<Weight> w_iter = w_list[i].iterator();
			while(w_iter.hasNext()) {
				total +=  w_iter.next().floatData.length;
			}
		}
		
		float th = 0;
		Date startTime = new Date();
		
		for(int i = 0; i < w_list.length; i++) {
			Iterator<Weight> w_iter = w_list[i].iterator();
			Iterator<Variables> pIter = weightProfile.iterator();
			while(w_iter.hasNext()) {
				Weight w = w_iter.next();
				Variables profile = pIter.next();
				
				if(!profile.getSkipCompress()) {
					int maxIter = (int) (MAX_SAMPLE * ((float) w.floatData.length / total));
					if(maxIter < w.floatData.length) {
						Set<Integer> selected = new HashSet<Integer>();
						
						int k = 0;
						while(k < maxIter) {
							int index = rand.nextInt(w.floatData.length);
							if(!selected.contains(index)) {
								selected.add(index);
								weights.add(Math.abs(w.floatData[index]));
								k++;
							}
						}
					} else {
						for(int k = 0 ; k < w.floatData.length;k++) {
							weights.add(Math.abs(w.floatData[k]));
						}
					}
				}
			}
		}
		
		Collections.sort(weights);
		
		th = weights.get((int) (weights.size() * cut_ratio) - 1);
		
		Date endTime = new Date();
		System.out.println(String.format("find %f for %d ms", th, 
				(endTime.getTime() - startTime.getTime())));
		return th;
	}
	
	public static float findThresholdFromDiff(List<float[]>[] w_list, List<Variables> weightProfile, float cut_ratio) throws Exception {
		final int MAX_SAMPLE = 10000;
		Random rand = new Random();
		
//		if(w_list.size() != variable_profiles.size())
//			throw new Exception("size mismatch");
		
		List<Float> weights = new ArrayList<Float>();
		int total = 0;
		for(int i = 0; i < w_list.length;i++) {
			Iterator<float[]> w_iter = w_list[i].iterator();
			while(w_iter.hasNext()) {
				total +=  w_iter.next().length;
			}
		}
		
		float th = 0;
		
		if(total == 0)
			return th;
		
		Date startTime = new Date();
		
		for(int i = 0; i < w_list.length; i++) {
			Iterator<float[]> w_iter = w_list[i].iterator();
			Iterator<Variables> pIter = weightProfile.iterator();
			while(w_iter.hasNext()) {
				float[] w = w_iter.next();
				Variables profile = pIter.next();
				
				if(!profile.getSkipCompress()) {
					int maxIter = (int) (MAX_SAMPLE * ((float) w.length / total));
					if(maxIter < w.length) {
						Set<Integer> selected = new HashSet<Integer>();
						
						int k = 0;
						while(k < maxIter) {
							int index = rand.nextInt(w.length);
							if(!selected.contains(index)) {
								selected.add(index);
								weights.add(w[index]);
								k++;
							}
						}
					} else {
						for(int k = 0 ; k < w.length;k++) {
							weights.add(w[k]);
						}
					}
				}
			}
		}
		
		Collections.sort(weights);
		
		th = weights.get(Math.max((int) (weights.size() * cut_ratio) - 1, 0));
		
		Date endTime = new Date();
		System.out.println(String.format("find %f for %d ms", th, 
				(endTime.getTime() - startTime.getTime())));
		return th;
	}
	
	public static float findThresholdFullFromDiff(List<float[]>[] w_list, List<Variables> weightProfile, float cut_ratio) throws Exception {
		int total = 0;
		for(int i = 0; i < w_list.length;i++) {
			Iterator<float[]> w_iter = w_list[i].iterator();
			Iterator<Variables> pIter = weightProfile.iterator();
			while(w_iter.hasNext()) {
				Variables profile = pIter.next();
				if(!profile.getSkipCompress())
					total +=  w_iter.next().length;
			}
		}
		float[] arr = new float[total];
		
		float th = 0;
		
		if(total == 0)
			return th;
		
		Date startTime = new Date();
		int index = 0;
		for(int i = 0; i < w_list.length; i++) {
			Iterator<float[]> w_iter = w_list[i].iterator();
			Iterator<Variables> pIter = weightProfile.iterator();
			while(w_iter.hasNext()) {
				float[] w = w_iter.next();
				Variables profile = pIter.next();
				
				if(!profile.getSkipCompress()) {
					System.arraycopy(w, 0, arr, index, w.length);
					index += w.length;
				}
			}
		}
		
		Arrays.sort(arr);
		
		th = arr[(int) (arr.length * cut_ratio) - 1];
		
		Date endTime = new Date();
		System.out.println(String.format("For %d of target parameters, find %f for %d ms", total, th, 
				(endTime.getTime() - startTime.getTime())));
		return th;
	}
	
	public static float findThresholdFull(List<Weight> w_list, float cut_ratio) throws Exception {
//		Iterator<Variables> v_iter = variable_profiles.iterator();
		float th = 0;
		Date startTime = new Date();

		Iterator<Weight> w_iter = w_list.iterator();
		int wholeSize = 0;
		while(w_iter.hasNext()) {
			wholeSize += w_iter.next().floatData.length;
		}
		float[] arr = new float[wholeSize];
		
		int index = 0;
		w_iter = w_list.iterator();
		while(w_iter.hasNext()) {
			Weight w = w_iter.next();
			for(int i = 0; i < w.floatData.length; i++) 
				arr[index++] = Math.abs(w.floatData[i]); 
		}
		
		Arrays.sort(arr);
		
		th = arr[(int) (arr.length * cut_ratio)];
		
		Date endTime = new Date();
		System.out.println(String.format("find %f for %d ms", th, 
				(endTime.getTime() - startTime.getTime())));
		return th;
	}
	
	public static float findThreshold(Map<Integer,Weight> w_list, List<Variables> variable_profiles, float cut_ratio) throws Exception {
		final int MAX_SAMPLE = 100;
		final float RATIO = 0.1f;
		Random rand = new Random();
		
		if(w_list.size() != variable_profiles.size())
			throw new Exception("size mismatch");
		
		List<Float> weights = new ArrayList<Float>();
		Iterator<Variables> v_iter = variable_profiles.iterator();
		float th = 0;
		Date startTime = new Date();
				
		int key = 0;
		while(v_iter.hasNext()) {
			Weight w = w_list.get(key++);
			Variables profile = v_iter.next();
			Set<Integer> selected = new HashSet<Integer>();
			
			if(!profile.getSkipCompress()) {
				int maxIter =(int) (w.floatData.length * RATIO); 
				if(maxIter > MAX_SAMPLE) {
					maxIter = MAX_SAMPLE;
				} 
				
				for(int i = 0 ; i < maxIter; i++) {
					int index = rand.nextInt(w.floatData.length);
					if(!selected.contains(index)) {
						selected.add(index);
						weights.add(Math.abs(w.floatData[index]));
					}
				}
			}	
		}
		Collections.sort(weights);
		
		th = weights.get((int) (weights.size() * cut_ratio));
		
		Date endTime = new Date();
		System.out.println(String.format("find %f for %d ms", th, 
				(endTime.getTime() - startTime.getTime())));
		return th;
	}
	
	@SuppressWarnings("unchecked")
	public static Weight decompress(Weight compressed) {
		if(compressed.sparseData == null)
			return compressed;
		
		// for float
		Weight res = new Weight();
		res.floatData = new float[compressed.originalLength];
		res.layerIndex = compressed.layerIndex;
		res.offset = compressed.offset;
		res.type = compressed.type;
 		
		int resIndex = 0;
		for(int i = 0; i < compressed.sparseData.data.length; i++) {
			resIndex += 0xff & compressed.sparseData.index[i];
			res.floatData[resIndex] = compressed.sparseData.data[i];
			resIndex++;
		}
		
		return res;
	}
	
	public static void addFromTo(List<Weight> from, List<Weight> to) {
		if(from.size() != to.size()) {
			throw new RuntimeException(String.format("Size mismatch! (%d vs %d)",from.size(),to.size()));
		}
		
		Iterator<Weight> f = from.iterator();
		Iterator<Weight> t = to.iterator();
		
		while(f.hasNext()) {
			addFromTo(f.next(), t.next());
		}
	}
	
	public static void addFromTo(Weight f, Weight t) {
		if(t.type == Tensor.FLOAT) {
			for(int j = f.offset; j < f.floatData.length; j++) {
				t.floatData[j] += f.floatData[j-f.offset];
			}				
		}
	}
	
	public static void sub(List<Weight> sub, List<Weight> inPlace) {
		if(sub.size() != inPlace.size()) {
			throw new RuntimeException(String.format("Size mismatch! (%d vs %d)",sub.size(),inPlace.size()));
		}
		
		for(int i = 0; i < sub.size();i++) {
			Weight f = sub.get(i);
			Weight t = inPlace.get(i);
			
			if(t.type == Tensor.FLOAT) {
				for(int j = f.offset; j < f.floatData.length; j++) {
					t.floatData[j] -= f.floatData[j-f.offset];
				}				
			}
		}	
	}
	
	public static void scalarMult(Weight t, float ratio) {
	if(t.type == Tensor.FLOAT) {
			for(int j = 0; j < t.floatData.length; j++) {
				t.floatData[j] *= ratio;
			}
		}
	}	
	
	public static void scalarMult(List<Weight> to, float ratio) {
		Iterator<Weight> iter = to.iterator();
		
		while(iter.hasNext()) {
			scalarMult(iter.next(), ratio);
		}
			
	}	
	
	public static void clear(List<Weight> l) {
		for(int i =0; i < l.size(); i++) {
			if(l.get(i).type == Tensor.FLOAT)
				Arrays.fill(l.get(i).floatData, 0);
		}
	}
	
	public static void EASGDUpdate(Weight remote_weight, Weight local_weight, float coeff) {
		for(int i = 0; i < local_weight.floatData.length; i++) {
			local_weight.floatData[i] = EASGD_L2(remote_weight.floatData[i], local_weight.floatData[i], coeff);
		}
	}

	public static void EASGDUpdate(Weight remote_weight, Weight local_weight, float coeff1, float coeff2, float th, int option, Regularizer l1_norm) {
		if(remote_weight.floatData == null) {
			// compressed EASGD
			switch(option) {
			case 0: // option 1: just decompress (fill zero)
				Weight decomp = WeightUtil.decompress(remote_weight);
				for(int i = 0; i < local_weight.floatData.length; i++) {
					local_weight.floatData[i] = EASGD_L2(decomp.floatData[i], local_weight.floatData[i], coeff1);
				}
				break;
				
			case 1: // option 2: just apply only non-zero values
				int index = 0;
				SparseEntries e = remote_weight.sparseData;
				for(int i = 0; i < e.data.length; i++) {
					if(e.data[i] > 0) {
						index += 0xff & e.index[i];
						local_weight.floatData[index] = EASGD_L2(e.data[i], local_weight.floatData[index], coeff1);
					} else {
						index += 0xff & e.index[i];
					}
				}
				break;
			case 2: //  option 3: apply on non-zero values and over the threshold
				SparseEntries e1 = remote_weight.sparseData;
				int sparseIdx = 0;
				int rIndex = 0;
				for(int i = 0; i < local_weight.floatData.length; i++) {
					float localVal = local_weight.floatData[i];
					if(sparseIdx < e1.index.length &&
							rIndex == (0xff & e1.index[sparseIdx])) {
						if( e1.data[sparseIdx] > 0f || localVal > th) {
							local_weight.floatData[i] = EASGD_L2(e1.data[sparseIdx], localVal, coeff1);
						}
						sparseIdx++;
						rIndex = 0;
					} else {
						if( localVal > th) {
							local_weight.floatData[i] = EASGD_L2(0, localVal, coeff1);
						}
						rIndex++;
					} 
				}
			}
			
		} else {
			// normal EASGD
			switch(l1_norm) {
			case L1:
				for(int i = 0; i < local_weight.floatData.length; i++) {
					local_weight.floatData[i] = EASGD_L1(remote_weight.floatData[i], local_weight.floatData[i], coeff2);
				}
				break;
			case L2:
				for(int i = 0; i < local_weight.floatData.length; i++) {
					local_weight.floatData[i] = EASGD_L2(remote_weight.floatData[i], local_weight.floatData[i], coeff1);
				}
				break;
			case MIXED:
				for(int i = 0; i < local_weight.floatData.length; i++) {
					local_weight.floatData[i] = EASGD_Elastic(remote_weight.floatData[i], local_weight.floatData[i], coeff2, coeff1);
				}
				break;
			default:
				break;
			}
		}
	}
	
	public static void EASGDUpdateWithDiff(Weight remote_weight, Weight local_weight, DeepSparkParam param) {
		if(remote_weight.floatData == null) {
			// compressed EASGD
			switch(param.getCompressParam().getApplyOption()) {
			case 0: // option 1: just decompress (fill zero)
				Weight decomp = WeightUtil.decompress(remote_weight);
				switch(param.getCompressParam().getPosttrainMethod()) {
				case L1:
					for(int i = 0; i < local_weight.floatData.length; i++) {
						local_weight.floatData[i] = EASGD_L1(decomp.floatData[i], local_weight.floatData[i], param.getFixedMovingRate());
					}
					break;
				case L2:
					for(int i = 0; i < local_weight.floatData.length; i++) {
						local_weight.floatData[i] = EASGD_L2(decomp.floatData[i], local_weight.floatData[i], param.getMovingRate());
					}
					break;
				case MIXED:
					for(int i = 0; i < local_weight.floatData.length; i++) {
						local_weight.floatData[i] = EASGD_Elastic(decomp.floatData[i], local_weight.floatData[i], param.getFixedMovingRate(), param.getMovingRate());
					}
					break;
				default:
					break;
				}
				break;
			case 1: // option 2: just apply only non-zero values
				int index = 0;
				SparseEntries e = remote_weight.sparseData;
				for(int i = 0; i < e.data.length; i++) {
					if(!Float.isNaN(e.data[i])) {
						index += 0xff & e.index[i];
						switch(param.getCompressParam().getPosttrainMethod()) {
						case L1:
							local_weight.floatData[index] = EASGD_L1(e.data[i], local_weight.floatData[index], param.getFixedMovingRate());
							break;
						case L2:
							local_weight.floatData[index] = EASGD_L2(e.data[i], local_weight.floatData[index], param.getMovingRate());
							break;
						case MIXED:
							local_weight.floatData[index] = EASGD_Elastic(e.data[i], local_weight.floatData[index], param.getFixedMovingRate(), param.getMovingRate());
							break;
						default:
							break;
						}
					} else {
						index += 0xff & e.index[i];
					}
					index++;
				}
				break;
			case 2:
				
				break;
			}
			
		} else {
			// normal EASGD
			switch(param.getCompressParam().getPretrainMethod()) {
			case L1:
				for(int i = 0; i < local_weight.floatData.length; i++) {
					local_weight.floatData[i] = EASGD_L1(remote_weight.floatData[i], local_weight.floatData[i], param.getFixedMovingRate());
				}
				break;
			case L2:
				for(int i = 0; i < local_weight.floatData.length; i++) {
					local_weight.floatData[i] = EASGD_L2(remote_weight.floatData[i], local_weight.floatData[i], param.getMovingRate());
				}
				break;
			case MIXED:
				for(int i = 0; i < local_weight.floatData.length; i++) {
					local_weight.floatData[i] = EASGD_Elastic(remote_weight.floatData[i], local_weight.floatData[i], param.getFixedMovingRate(), param.getMovingRate());
				}
				break;
			default:
				break;
			}
		}
	}
	
	public static void EASGDUpdateWithDiff(Weight remote_weight, Weight local_weight, WeightStat remoteStat, WeightStat localStat, float factor, DeepSparkParam param) {
		float movingRate = factor * param.getMovingRate();
		float fixedMovingRate = factor * param.getFixedMovingRate();
		
		float a = 1; float b = 0;
		if(remoteStat != null && localStat != null) {
			a = remoteStat.getStdev() / localStat.getStdev();
			// b = remoteStat.getMean() - a * localStat.getMean();
		}
		
		if(remote_weight.floatData == null) {
			// compressed EASGD
			switch(param.getCompressParam().getApplyOption()) {
			case 0: // option 1: just decompress (zero fill)
				Weight decomp = WeightUtil.decompress(remote_weight);
				switch(param.getCompressParam().getPosttrainMethod()) {
				case L1:
					for(int i = 0; i < local_weight.floatData.length; i++) {
						local_weight.floatData[i] = EASGD_L1(decomp.floatData[i], local_weight.floatData[i], fixedMovingRate);
					}
					break;
				case L2:
					for(int i = 0; i < local_weight.floatData.length; i++) {
						local_weight.floatData[i] = EASGD_L2(decomp.floatData[i], local_weight.floatData[i], movingRate);
					}
					break;
				case MIXED:
					for(int i = 0; i < local_weight.floatData.length; i++) {
						local_weight.floatData[i] = EASGD_Elastic(decomp.floatData[i], local_weight.floatData[i], fixedMovingRate, movingRate);
					}
					break;
				default:
					break;
				}
				break;
			case 1: // option 2: just apply only non-zero values
				int index = 0;
				SparseEntries e = remote_weight.sparseData;
				for(int i = 0; i < e.data.length; i++) {
					if(!Float.isNaN(e.data[i])) {
						index += 0xff & e.index[i];
						switch(param.getCompressParam().getPosttrainMethod()) {
						case L1:
							local_weight.floatData[index] = EASGD_L1(e.data[i], local_weight.floatData[index], fixedMovingRate);
							break;
						case L2:
							local_weight.floatData[index] = EASGD_L2(e.data[i], local_weight.floatData[index], movingRate);
							break;
						case MIXED:
							local_weight.floatData[index] = EASGD_Elastic(e.data[i], local_weight.floatData[index], fixedMovingRate, movingRate);
							break;
						default:
							break;
						}
					} else {
						index += 0xff & e.index[i];
					}
					index++;
				}
				break;
			case 2:
				SparseEntries e1 = remote_weight.sparseData;
				int sparseIdx = 0;
				int base = 0;
				
				for(int i = 0; i < local_weight.floatData.length; i++) {
					float localVal = local_weight.floatData[i];
					if(sparseIdx < e1.index.length) {
						if(base + (e1.index[sparseIdx] & 0xff) == i) {
							if(Float.isNaN(e1.data[sparseIdx])) {
								switch(param.getCompressParam().getPosttrainMethod()) {
								case L1:
									local_weight.floatData[i] = EASGD_L1(localVal * a + b , localVal, fixedMovingRate);
									break;
								case L2:
									local_weight.floatData[i] = EASGD_L2(localVal * a + b , localVal, movingRate);
									break;
								case MIXED:
									local_weight.floatData[i] = EASGD_Elastic(localVal * a + b, localVal, fixedMovingRate, movingRate);
									break;
								default:
									break;
								}
							} else {
								switch(param.getCompressParam().getPosttrainMethod()) {
								case L1:
									local_weight.floatData[i] = EASGD_L1(e1.data[sparseIdx] , localVal, fixedMovingRate);
									break;
								case L2:
									local_weight.floatData[i] = EASGD_L2(e1.data[sparseIdx], localVal, movingRate);
									break;
								case MIXED:
									local_weight.floatData[i] = EASGD_Elastic(e1.data[sparseIdx], localVal, fixedMovingRate, movingRate);
									break;
								default:
									break;
								}
							}
							sparseIdx++;
							base = i + 1; 
						} else {
							switch(param.getCompressParam().getPosttrainMethod()) {
							case L1:
								local_weight.floatData[i] = EASGD_L1(localVal * a + b, localVal, fixedMovingRate);
								break;
							case L2:
								local_weight.floatData[i] = EASGD_L2(localVal * a + b , localVal, movingRate);
								break;
							case MIXED:
								local_weight.floatData[i] = EASGD_Elastic(localVal * a + b , localVal, fixedMovingRate, movingRate);
								break;
							default:
								break;
							}
						}
					} else {
						switch(param.getCompressParam().getPosttrainMethod()) {
						case L1:
							local_weight.floatData[i] = EASGD_L1(localVal * a + b, localVal, fixedMovingRate);
							break;
						case L2:
							local_weight.floatData[i] = EASGD_L2(localVal *a + b, localVal, movingRate);
							break;
						case MIXED:
							local_weight.floatData[i] = EASGD_Elastic(localVal *a + b, localVal, fixedMovingRate, movingRate);
							break;
						default:
							break;
						}
					}
				}
				break;
			}
			
		} else {
			// normal EASGD
			switch(param.getCompressParam().getPretrainMethod()) {
			case L1:
				for(int i = 0; i < local_weight.floatData.length; i++) {
					local_weight.floatData[i] = EASGD_L1(remote_weight.floatData[i], local_weight.floatData[i], fixedMovingRate);
				}
				break;
			case L2:
				for(int i = 0; i < local_weight.floatData.length; i++) {
					local_weight.floatData[i] = EASGD_L2(remote_weight.floatData[i], local_weight.floatData[i], movingRate);
				}
				break;
			case MIXED:
				for(int i = 0; i < local_weight.floatData.length; i++) {
					local_weight.floatData[i] = EASGD_Elastic(remote_weight.floatData[i], local_weight.floatData[i], fixedMovingRate, movingRate);
				}
				break;
			default:
				break;
			}
		}
	}

	private static float EASGD(float remote, float local, float coeff) {
		//L1-norm
		if(local > remote) {
			return local - coeff;
		} else if(local < remote) {
			return local + coeff;
		} else {
			return local;
		}
		// return local * (1.0f - coeff) + remote * coeff;  // L2-norm
	}
	
	private static float EASGD_L1(float remote, float local, float coeff) {
		float result = local;
		
		if(local > remote) {
			result -= coeff;
		} else if(local < remote) {
			result += coeff;
		}
		
		return result;
	}
	
	private static float EASGD_L1_Diff(float remote, float local, float coeff) {
		float result = 0f;
		
		if(local > remote) {
			result = coeff;
		} else if(local < remote) {
			result = coeff;
		}
		
		return result;
	}
	
	private static float EASGD_Elastic_Diff(float remote, float local, float coeff_L1,float coeff_L2) {
		float result = EASGD_L2_Diff(remote,local, coeff_L2);
		result += EASGD_L1_Diff(remote, local, coeff_L1);		
		return result;
	}
	
	private static float EASGD_Elastic(float remote, float local, float coeff_L1,float coeff_L2) {
		if(local > remote) {
			return local * (1.0f - coeff_L2) + remote * coeff_L2 - coeff_L1;
		} else if(local < remote) {
			return local * (1.0f - coeff_L2) + remote * coeff_L2 + coeff_L1;
		} else {
			return local;
		}
	}
	
	private static float EASGD_L2(float remote, float local, float coeff) {
		return local * (1.0f - coeff) + remote * coeff;  // L2-norm
	}
	
	private static float EASGD_L2_Diff(float remote, float local, float coeff) {
		float result = - coeff * (local - remote);
		return result;
	}
	
	public static List<Weight> compressWeightList(List<Weight> list, float th, List<Variables> weightProfile) {
		List<Weight> output = new ArrayList<Weight>();
		
		Iterator<Weight> iter = list.iterator();
		Iterator<Variables> pIter = weightProfile.iterator();
		
		while(iter.hasNext()) {
			Weight w = iter.next();
			if(pIter.next().getSkipCompress())
				output.add(w);
			else {
			Weight comp = WeightUtil.compress(w, th);
			output.add(comp);
			}
		}
		return output;
	}
	
	public static List<Weight> compressWeightListForServer(List<Weight> list, List<Weight> compressed, List<Variables> weightProfile) {
		List<Weight> output = new ArrayList<Weight>();
		
		Iterator<Weight> org = list.iterator();
		Iterator<Weight> client = compressed.iterator();
		
		while(org.hasNext()) {
			Weight local = org.next();
			Weight remote = client.next();
			
			if(remote.floatData != null)
				output.add(local);
			else  {
				Weight comp = WeightUtil.compressForServer(local, remote);
				output.add(comp);
			}
		}
		
		return output;
	}
	
	public static List<Weight> compressWeightListWithDiffMap(List<Weight> list, float th, List<Variables> weightProfile, List<float[]> diffMap ) {
		List<Weight> output = new ArrayList<Weight>();
		
		Iterator<Weight> iter = list.iterator();
		Iterator<Variables> pIter = weightProfile.iterator();
		Iterator<float[]> diff = diffMap.iterator();
		
		while(iter.hasNext()) {
			Weight w = iter.next();
			float[] d = diff.next();
			if(pIter.next().getSkipCompress())
				output.add(w);
			else {
			Weight comp = WeightUtil.compressWithDiff(w, th, d);
			output.add(comp);
			}
		}
		return output;
	}

	public static void initDiffMap(List<float[]>[] diffMap, List<Weight>[] weights) {
		for(int i =0; i < weights.length;i++) {
			Iterator<Weight> wIter = weights[i].iterator();
			while(wIter.hasNext()) {
				Weight w = wIter.next();
				float[] f = new float[w.floatData.length];
				Arrays.fill(f, 0.0f);
				diffMap[i].add(f);
			}
		}
	}

	public static List<Weight> compressWeightListRandomly(List<Weight> list, List<Variables> weightProfile,
			float th) {
		List<Weight> output = new ArrayList<Weight>();
		
		Iterator<Weight> iter = list.iterator();
		Iterator<Variables> pIter = weightProfile.iterator();
		
		while(iter.hasNext()) {
			Weight w = iter.next();
			if(pIter.next().getSkipCompress())
				output.add(w);
			else {
				Weight comp = WeightUtil.compressRandomly(w, th);
				output.add(comp);
			}
		}
		return output;
	}

	public static Weight compressRandomly(Weight original, float th) {
		if(original.floatData.length < 2)
			return original;
		
		float r = 1-th;
		int sampleLength = (int) Math.ceil(r * original.floatData.length); // minimum: 1
		int batchSize =  (int) (1 / r);
		Random rand = new Random();
		
		Weight compressed = new Weight();
		
		// currently, only for float type
		compressed.layerIndex = original.layerIndex;
		compressed.offset = original.offset;
		compressed.type = original.type;
		compressed.sparseData = new SparseEntries();
		
		int[] indices = new int[sampleLength];
		
		int sample_1 = sampleLength-1;
		int i = 0;
		int newLength = 0, prevIdx = 0;
		int orgLength = original.floatData.length;
		for(; i < sample_1; i++) {
			indices[i] = batchSize * i + rand.nextInt(batchSize);
			newLength += (indices[i] - prevIdx) / 256 + 1;
			prevIdx = indices[i]; 
			orgLength -= batchSize;
		}
		indices[i] = batchSize * i + rand.nextInt(orgLength);
		newLength += (indices[i] - prevIdx) / 256 + 1;
		
		byte[] iBuffer = new byte[newLength];
		float[] vBuffer = new float[newLength];
		
		// pruning, only for float type
		compressed.originalLength = original.floatData.length;
		prevIdx = -1;
		int index = 0;
		for(i = 0; i < sampleLength; i++) {
			int tmpPrevIdx = prevIdx;
			
			iBuffer[index] = (byte) ( (indices[i] - tmpPrevIdx - 1) & 0xff);
			vBuffer[index++] = original.floatData[indices[i]];
			prevIdx = indices[i];
		}
		
		compressed.sparseData.index = iBuffer;
		compressed.sparseData.data = vBuffer;
		
		return compressed;
	}
	
	public static Weight compressRandomly2(Weight original, float th) {
		//if(original.floatData.length < 2)
		//	return original;
		Random rand = new Random();
		
		Weight compressed = new Weight();
		// currently, only for float type
		compressed.layerIndex = original.layerIndex;
		compressed.offset = original.offset;
		compressed.type = original.type;
		compressed.sparseData = new SparseEntries();
		compressed.originalLength = original.floatData.length;
		
		byte[] iBuffer = new byte[original.floatData.length];
		float[] vBuffer = new float[original.floatData.length];
		
		int count = 0;
		int prevIdx = -1;
		for(int i = 0; i < original.floatData.length;i++) {
			float thd = th;
			//float thd = (float) (th * Math.exp(-Math.abs(original.floatData[i])));
			if(rand.nextFloat() > thd) { // chance to send
				while(i - prevIdx > 256) {
					iBuffer[count] = (byte) 0xff;
					vBuffer[count] = Float.NaN;
					prevIdx += 256;
					count++;
				}
				iBuffer[count] =  (byte) ((i - prevIdx -1) & 0xff);
				vBuffer[count] = original.floatData[i];
				count++;
				prevIdx = i;
			}
		}
		
		compressed.sparseData.index = Arrays.copyOfRange(iBuffer, 0, count);
		compressed.sparseData.data = Arrays.copyOfRange(vBuffer, 0, count);
		
		return compressed;
	}

	public static List<Weight> compressWeightListRandomly2(List<Weight> list, List<Variables> weightProfile,
			float th) {
		List<Weight> output = new ArrayList<Weight>();
		
		Iterator<Weight> iter = list.iterator();
		Iterator<Variables> pIter = weightProfile.iterator();
		
		while(iter.hasNext()) {
			Weight w = iter.next();
			if(pIter.next().getSkipCompress())
				output.add(w);
			else {
				Weight comp = WeightUtil.compressRandomly2(w, th);
				output.add(comp);
			}
		}
		return output;
	}
	
	public static float meanWeightList(List<Weight>[] list) {
		float mean = 0f;
		int count = 0;
		
		for(int i = 0; i < list.length; i++) {
			Iterator<Weight> iter = list[i].iterator();
			while(iter.hasNext()) {
				Weight w = iter.next();
				if(w.floatData == null) {
					float local_mean = 0f;
					for(int k = 0; k < w.sparseData.data.length; k++) {
						local_mean += w.sparseData.data[k];
					}
					mean += local_mean * ((float) w.originalLength / w.sparseData.data.length); 
					count += w.originalLength;
				} else {
					for(int k = 0; k <w.floatData.length; k++) {
						mean += w.floatData[k];
					}
					count += w.floatData.length;
				}
			}
		}
		
		return mean / count;
	}
	
	public static float meanWeightList(List<Weight> list) {
		float mean = 0f;
		int count = 0;
		
		Iterator<Weight> iter = list.iterator();
		while(iter.hasNext()) {
			Weight w = iter.next();
			if(w.floatData == null) {
				float local_mean = 0f;
				for(int k = 0; k < w.sparseData.data.length; k++) {
					local_mean += w.sparseData.data[k];
				}
				mean += local_mean * ((float) w.originalLength / w.sparseData.data.length); 
				count += w.originalLength;
			} else {
				for(int k = 0; k <w.floatData.length; k++) {
					mean += w.floatData[k];
				}
				count += w.floatData.length;
			}
		}
		
		return mean / count;
	}

	public static void EASGDUpdateClient(Weight remote_weight, Weight local_weight, WeightStat remoteStat, WeightStat localStat, float factor, DeepSparkParam param) {
		float movingRate = factor * param.getMovingRate();
		float fixedMovingRate = factor * param.getFixedMovingRate();
		float a = 1; float b = 0;
		if(remoteStat != null && localStat != null) {
			a = remoteStat.getStdev() / localStat.getStdev();
			// b = remoteStat.getMean() - a * localStat.getMean();
			//System.out.println("a: " + a);
			//System.out.println("b: " + b); 
		}
		
		
		if(remote_weight.floatData == null) {
			// compressed EASGD
			switch(param.getCompressParam().getApplyOption()) {
			case 0: // option 1: just decompress (zero fill)
				Weight decomp = WeightUtil.decompress(remote_weight);
				switch(param.getCompressParam().getPosttrainMethod()) {
				case L1:
					for(int i = 0; i < local_weight.floatData.length; i++) {
						local_weight.floatData[i] = EASGD_L1_Diff(decomp.floatData[i], local_weight.floatData[i], fixedMovingRate);
					}
					break;
				case L2:
					for(int i = 0; i < local_weight.floatData.length; i++) {
						local_weight.floatData[i] = EASGD_L2_Diff(decomp.floatData[i], local_weight.floatData[i], movingRate);
					}
					break;
				case MIXED:
					for(int i = 0; i < local_weight.floatData.length; i++) {
						local_weight.floatData[i] = EASGD_Elastic_Diff(decomp.floatData[i], local_weight.floatData[i], fixedMovingRate, movingRate);
					}
					break;
				default:
					break;
				}
				break;
			case 1: // option 2: just apply only non-zero values
				SparseEntries e = remote_weight.sparseData;
				int sparseIdx = 0;
				int base = 0;
				
				for(int i = 0; i < local_weight.floatData.length; i++) {
					float localVal = local_weight.floatData[i];
					if(sparseIdx < e.index.length && 
							base + (e.index[sparseIdx] & 0xff) == i) {
						if(Float.isNaN(e.data[sparseIdx])) {
							switch(param.getCompressParam().getPosttrainMethod()) {
							case L1:
								local_weight.floatData[i] = 0;
								break;
							case L2:
								local_weight.floatData[i] = 0;
								break;
							case MIXED:
								local_weight.floatData[i] = 0;
								break;
							default:
								break;
							}
						} else {
							switch(param.getCompressParam().getPosttrainMethod()) {
							case L1:
								local_weight.floatData[i] = EASGD_L1_Diff(e.data[sparseIdx] , localVal, fixedMovingRate);
								break;
							case L2:
								local_weight.floatData[i] = EASGD_L2_Diff(e.data[sparseIdx], localVal, movingRate);
								break;
							case MIXED:
								local_weight.floatData[i] = EASGD_Elastic_Diff(e.data[sparseIdx], localVal, fixedMovingRate, movingRate);
								break;
							default:
								break;
							}
						}
						sparseIdx++;
						base = i + 1; 
					} else {
						switch(param.getCompressParam().getPosttrainMethod()) {
						case L1:
							local_weight.floatData[i] = 0;
							break;
						case L2:
							local_weight.floatData[i] = 0;
							break;
						case MIXED:
							local_weight.floatData[i] = 0;
							break;
						default:
							break;
						}
					}
				}
				break;
			case 2:
				SparseEntries e1 = remote_weight.sparseData;
				sparseIdx = 0;
				base = 0;
				
				for(int i = 0; i < local_weight.floatData.length; i++) {
					float localVal = local_weight.floatData[i];
					if(sparseIdx < e1.index.length) {
						if(base + (e1.index[sparseIdx] & 0xff) == i) {
							if(Float.isNaN(e1.data[sparseIdx])) {
								switch(param.getCompressParam().getPosttrainMethod()) {
								case L1:
									local_weight.floatData[i] = EASGD_L1_Diff(localVal * a + b , localVal, fixedMovingRate);
									break;
								case L2:
									local_weight.floatData[i] = EASGD_L2_Diff(localVal * a + b , localVal, movingRate);
									break;
								case MIXED:
									local_weight.floatData[i] = EASGD_Elastic_Diff(localVal * a + b, localVal, fixedMovingRate, movingRate);
									break;
								default:
									break;
								}
							} else {
								switch(param.getCompressParam().getPosttrainMethod()) {
								case L1:
									local_weight.floatData[i] = EASGD_L1_Diff(e1.data[sparseIdx] , localVal, fixedMovingRate);
									break;
								case L2:
									local_weight.floatData[i] = EASGD_L2_Diff(e1.data[sparseIdx], localVal, movingRate);
									break;
								case MIXED:
									local_weight.floatData[i] = EASGD_Elastic_Diff(e1.data[sparseIdx], localVal, fixedMovingRate, movingRate);
									break;
								default:
									break;
								}
							}
							sparseIdx++;
							base = i + 1; 
						} else {
							switch(param.getCompressParam().getPosttrainMethod()) {
							case L1:
								local_weight.floatData[i] = EASGD_L1_Diff(localVal * a + b , localVal, fixedMovingRate);
								break;
							case L2:
								local_weight.floatData[i] = EASGD_L2_Diff(localVal * a + b , localVal, movingRate);
								break;
							case MIXED:
								local_weight.floatData[i] = EASGD_Elastic_Diff(localVal * a + b , localVal, fixedMovingRate, movingRate);
								break;
							default:
								break;
							}
						}
					} else {
						switch(param.getCompressParam().getPosttrainMethod()) {
						case L1:
							local_weight.floatData[i] = EASGD_L1_Diff(localVal * a + b, localVal, fixedMovingRate);
							break;
						case L2:
							local_weight.floatData[i] = EASGD_L2_Diff(localVal * a + b, localVal, movingRate);
							break;
						case MIXED:
							local_weight.floatData[i] = EASGD_Elastic_Diff(localVal * a + b, localVal, fixedMovingRate, movingRate);
							break;
						default:
							break;
						}
					}
				}
				break;
			}
			
		} else {
			// normal EASGD
			switch(param.getCompressParam().getPretrainMethod()) {
			case L1:
				for(int i = 0; i < local_weight.floatData.length; i++) {
					local_weight.floatData[i] = EASGD_L1_Diff(remote_weight.floatData[i], local_weight.floatData[i], fixedMovingRate);
				}
				break;
			case L2:
				for(int i = 0; i < local_weight.floatData.length; i++) {
					local_weight.floatData[i] = EASGD_L2_Diff(remote_weight.floatData[i], local_weight.floatData[i], movingRate);
				}
				break;
			case MIXED:
				for(int i = 0; i < local_weight.floatData.length; i++) {
					local_weight.floatData[i] = EASGD_Elastic_Diff(remote_weight.floatData[i], local_weight.floatData[i], fixedMovingRate, movingRate);
				}
				break;
			default:
				break;
			}
		}
	}
}
