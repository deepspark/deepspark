package org.acl.deepspark.nn.driver;

import java.io.ObjectInputStream;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.acl.deepspark.data.ByteRecord;
import org.acl.deepspark.data.Weight;
import org.acl.deepspark.nn.CaffeNet;
import org.acl.deepspark.nn.async.easgd.DistAsyncEASGDSolverWithPruning;
import org.acl.deepspark.nn.async.easgd.DistAsyncEASGDSolverWithPruningRandom;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam;
import org.acl.deepspark.utils.DeepSparkParamLoader;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.ArrayPrimitiveWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

import scala.Tuple2;

/**
 * Created by Hanjoo on 2017-01-31.
 */
public class AsyncEASGDImageNetTestPruning {
    public static void main(String[] args) throws Exception {
    	SparkConf conf = new SparkConf().setAppName("AsyncImageNetTest")
                .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        
        JavaSparkContext sc = new JavaSparkContext(conf);
        
        CaffeNet net = new CaffeNet(args[0], new int[]{3, 256, 256});
        DeepSparkParam param = DeepSparkParamLoader.readConf(args[1]);
        
        String serverHost = InetAddress.getLocalHost().getHostAddress();
        final int port = 10020;
        System.out.println("ParameterServer host: " + serverHost);
        DistAsyncEASGDSolverWithPruningRandom driver = new DistAsyncEASGDSolverWithPruningRandom(net, serverHost, port, param);
        int numExecutors = conf.getInt("spark.executor.instances", -1);
        System.out.println("number of executors = " + numExecutors);
        
        System.out.println("Data Loading...");
        JavaPairRDD<FloatWritable, ArrayPrimitiveWritable> train_seq = 
        		sc.sequenceFile("/imagenet_train.hsf", FloatWritable.class, ArrayPrimitiveWritable.class);

        Integer[] tempkey = new Integer[numExecutors];
        for(int i = 0; i < tempkey.length; i++)
        	tempkey[i] = i;
        
        JavaRDD<Integer> rddkey = sc.parallelize(Arrays.asList(tempkey), numExecutors);
        
        JavaRDD<ByteRecord> train_samples = train_seq.map(new Function<Tuple2<FloatWritable,ArrayPrimitiveWritable>, ByteRecord>() {
        	@Override
			public ByteRecord call(Tuple2<FloatWritable, ArrayPrimitiveWritable> arg0) throws Exception {
        		ByteRecord d = new ByteRecord();
				d.label = arg0._1.get();
				d.data = (byte[]) arg0._2.get();
				d.dim = new int[]{3,256,256};
				return d;
			}
		});
        
        // spill to local
        driver.prepareLocalByte(train_samples, "tmp_data", numExecutors);
        driver.prepareLocalDummy(rddkey, "tmp_data_val",new int[]{3, 256, 256});
        
        
        train_samples.unpersist();
        System.out.println("Start Learning...");
        Date startTime = new Date();
        
        //train local
        driver.trainWithLMDB(rddkey, null, numExecutors);
        
        Date endTime = new Date();
        	
        long time = endTime.getTime() - startTime.getTime();
        System.out.println(String.format("Training time: %f secs", (double) time / 1000));
        
        //save all.
        Configuration hdfs_conf = new Configuration();
        FileSystem fs = FileSystem.get(hdfs_conf);
        List<Weight>[] weights = new List[numExecutors];
        
        for(int j = param.getSnapshotPeriod() * param.getPeriod();
        		j <= net.getSolverSetting().getMaxIter(); j += param.getSnapshotPeriod() * param.getPeriod()) {
	        for(int i = 0; i < numExecutors; i++) {
	        	FSDataInputStream fin = fs.open(new Path(String.format("%s_%d_%d.weight", param.getSnapshotPrefix(), i, j)));
		        ObjectInputStream in = new ObjectInputStream(fin);
		        
		        weights[i] = (List<Weight>) in.readObject();
		        in.close();
		        fin.close();
	        }

	        driver.getNet().setWeights(weights);
	        System.out.println(String.format("%s_%d.caffemodel saved...", param.getSnapshotPrefix(),j));
	        driver.saveCaffeModel(weights, String.format("%s_%d.caffemodel", param.getSnapshotPrefix(),j), fs);
        }
        
        sc.close();
    }
}
