package org.acl.deepspark.nn.driver;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.acl.deepspark.data.LMDBReader2;
import org.acl.deepspark.data.LMDBWriter2;
import org.acl.deepspark.data.SeqRecord;
import org.acl.deepspark.data.Weight;
import org.acl.deepspark.nn.async.easgd.ParameterEASGDClient;
import org.acl.deepspark.nn.async.easgd.ParameterEASGDServer;
import org.acl.deepspark.nn.async.easgd.PartialExchangerManager;
import org.acl.deepspark.tf.RunOption;
import org.acl.deepspark.tf.TFNet;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam;
import org.acl.deepspark.utils.DeepSparkParamLoader;
import org.acl.deepspark.utils.WeightUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;

import scala.Tuple2;

/**
 * Created by Hanjoo on 2016-01-18.
 */
public class TFSeq2SeqTest {
	static final int PAD = 0;
	static final int GO = 1;
	static final int EOS = 2;
	static final int UNK = 3;
	
    public static void main(String[] args) throws Exception {
    	SparkConf conf = new SparkConf().setAppName("AsyncMnistTest")
                .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        
        JavaSparkContext sc = new JavaSparkContext(conf);
        int numExecutors = conf.getInt("spark.executor.instances", -1);
        System.out.println("number of executors = " + numExecutors);
        
        final DeepSparkParam param = DeepSparkParamLoader.readConf(args[1]);
        System.out.println("Data Loading...");
        
        JavaRDD<String> en_ids = sc.textFile("sampled.en");
        JavaRDD<String> fr_ids = sc.textFile("sampled.fr");
        
        JavaRDD<String> en_test_ids = sc.textFile("test.en");
        JavaRDD<String> fr_test_ids = sc.textFile("test.fr");
        
        // english
        JavaPairRDD<Integer, List<Integer>> seq_en = en_ids.mapToPair(new PairFunction<String, Integer, List<Integer>>() {
			@Override
			public Tuple2<Integer, List<Integer>> call(String t) throws Exception {
				Scanner s = new Scanner(t);
				
				Integer key = s.nextInt();
				List<Integer> val = new ArrayList<Integer>();
				
				while(s.hasNextInt()) {
					val.add(s.nextInt());
				}
				s.close();
				return new Tuple2<Integer, List<Integer>>(key, val);
			}
		});
        
        JavaPairRDD<Integer, List<Integer>> seq_test_en = en_test_ids.mapToPair(new PairFunction<String, Integer, List<Integer>>() {
			@Override
			public Tuple2<Integer, List<Integer>> call(String t) throws Exception {
				Scanner s = new Scanner(t);
				
				Integer key = s.nextInt();
				List<Integer> val = new ArrayList<Integer>();
				
				while(s.hasNextInt()) {
					val.add(s.nextInt());
				}
				s.close();
				return new Tuple2<Integer, List<Integer>>(key, val);
			}
		});
        
        // french
        JavaPairRDD<Integer, List<Integer>> seq_fr = fr_ids.mapToPair(new PairFunction<String, Integer, List<Integer>>() {
			@Override
			public Tuple2<Integer, List<Integer>> call(String t) throws Exception {
				Scanner s = new Scanner(t);
				
				Integer key = s.nextInt();
				List<Integer> val = new ArrayList<Integer>();
				
				while(s.hasNextInt()) {
					val.add(s.nextInt());
				}
				s.close();
				val.add(EOS);
				return new Tuple2<Integer, List<Integer>>(key, val);
			}
		});
        
        JavaPairRDD<Integer, List<Integer>> seq_test_fr = fr_test_ids.mapToPair(new PairFunction<String, Integer, List<Integer>>() {
			@Override
			public Tuple2<Integer, List<Integer>> call(String t) throws Exception {
				Scanner s = new Scanner(t);
				
				Integer key = s.nextInt();
				List<Integer> val = new ArrayList<Integer>();
				
				while(s.hasNextInt()) {
					val.add(s.nextInt());
				}
				s.close();
				val.add(EOS);
				return new Tuple2<Integer, List<Integer>>(key, val);
			}
		});
        
        // join one
        JavaPairRDD<Integer, Tuple2<List<Integer>, List<Integer>>> joined_seq = seq_en.join(seq_fr).filter(new Function<Tuple2<Integer,Tuple2<List<Integer>,List<Integer>>>, Boolean>() {
			
			@Override
			public Boolean call(Tuple2<Integer, Tuple2<List<Integer>, List<Integer>>> v1) throws Exception {
				if(v1._1 < 100000)
					return true;
				else
					return false;
			}
		}).repartition(numExecutors);
        JavaPairRDD<Integer, Tuple2<List<Integer>, List<Integer>>> joined_test_seq = seq_test_en.join(seq_test_fr);
        List<SeqRecord> test_data = joined_test_seq.mapValues(new Function<Tuple2<List<Integer>,List<Integer>>, SeqRecord>() {
			@Override
			public SeqRecord call(Tuple2<List<Integer>, List<Integer>> v1) throws Exception {
				SeqRecord data = new SeqRecord();
				data.encode_data = v1._1;
				data.decode_data = v1._2;
				return data;
			}
		}).values().collect();
        
        // make bucket db
        final String db_name_prefix = "bucket";
        final int[] src_size = new int[]{5,10,20,40};
        final int[] dst_size = new int[]{10,15,25,50};
        final int batchSize = 64;
        
        // test set
        Iterator<SeqRecord> iter = test_data.iterator();
        List<SeqRecord>[] test_buckets = new List[4];
        for(int i = 0; i < 4 ; i++) {
			test_buckets[i] = new ArrayList<SeqRecord>();
		}
        
        while(iter.hasNext()) {
        	SeqRecord data = iter.next();
        	for(int i = 0; i < src_size.length; i++) {
				int source_len = src_size[i];
				int destination_len = dst_size[i];
				
				if(data.encode_data.size() < source_len &&
						data.decode_data.size() < destination_len) {
					test_buckets[i].add(data);
					break;
				}
			}
        }
        
        // initialize learning
        final String serverHost = InetAddress.getLocalHost().getHostAddress();
        System.out.println("ParameterServer host: " + serverHost);
        
        final String graph = new String(Files.readAllBytes(Paths.get(args[0])));
        TFNet net = new TFNet(param, graph);
        final List<Weight> init_weight = net.getWeights();
        final int iteration = param.getTfIteration();
        final int period = param.getPeriod();
        
        // test options
        for(int i = 0; i < 4; i++) {
			RunOption test = new RunOption();
			
			test.inputNames = net.getRunOption(TFNet.TRAIN).inputNames;
			test.numInputs = net.getRunOption(TFNet.TRAIN).numInputs;
			
			test.setTargetNames(new String[]{});
			
			if(i == 0 ) {
				test.setOutputNames(new String[] {"model_with_buckets/sequence_loss/truediv"});
			} else {
				test.setOutputNames(new String[] {String.format("model_with_buckets/sequence_loss_%d/truediv",i)});
			}
			
			net.addCustomRunOption(String.format("test_bucket%d", i), test);
		}
        
        testNet(net, test_buckets, src_size, dst_size, batchSize);
        
        // training set
        joined_seq.foreach(new VoidFunction<Tuple2<Integer,Tuple2<List<Integer>,List<Integer>>>>() {
        	transient LMDBWriter2[] dbWriter;
			
			private void readObject(ObjectInputStream in) throws ClassNotFoundException, IOException {
				System.gc();
				in.defaultReadObject();
				this.dbWriter = new LMDBWriter2[4];
				
				for(int i = 0; i < 4 ; i++) {
					File dir = new File(db_name_prefix+i);
					if(dir.mkdirs())
						System.out.println("mkdir successfully done...");
					dbWriter[i] = new LMDBWriter2(db_name_prefix+i, 100);
				}
			}
			
			@Override
			public void call(Tuple2<Integer, Tuple2<List<Integer>, List<Integer>>> t) throws Exception {
				// TODO Auto-generated method stub
				Tuple2<List<Integer>, List<Integer>> rec = t._2();
				
				SeqRecord data = new SeqRecord();
				data.encode_data = rec._1;
				data.decode_data = rec._2;
				
				for(int i = 0; i < src_size.length; i++) {
					int source_len = src_size[i];
					int destination_len = dst_size[i];
					
					if(data.encode_data.size() < source_len &&
							data.decode_data.size() < destination_len) {
						dbWriter[i].putSample(data);;
						break;
					}
				}
			}
		});
        
        // remove saved training data
        joined_seq.unpersist();
        
        // initial parameter exchanger
        final int listenPort = 12345;
        ParameterEASGDServer server = new ParameterEASGDServer(net, listenPort, numExecutors, param);
        server.startServer();
        System.out.println("Start Learning...");
        Date startTime = new Date();
        
        //make dummy RDD
        Integer[] tempkey = new Integer[numExecutors];
        for(int i = 0; i < tempkey.length; i++)
        	tempkey[i] = i;
        
        JavaRDD<Integer> rddkey = sc.parallelize(Arrays.asList(tempkey), numExecutors).cache();
        final List<Tuple2<Integer, String>> nodeHosts = rddkey.mapToPair(new PairFunction<Integer, Integer, String>() {
			@Override
			public Tuple2<Integer, String> call(Integer t) throws Exception {
				return new Tuple2<Integer, String>(t, InetAddress.getLocalHost().getHostAddress());
			}
		}).collect();
        
        // learning
        rddkey.foreachPartition(new VoidFunction<Iterator<Integer>>() {
        	private transient TFNet localNet;
			@Override
			public void call(Iterator<Integer> t) throws Exception {
				LMDBReader2 reader[] = new LMDBReader2[4];
				
				for(int i = 0; i < 4; i++)
					reader[i]= new LMDBReader2(db_name_prefix+i);
				
				// TODO Auto-generated method stub
				for(int i = 0; i < 4; i++) {
					RunOption train = new RunOption();
					RunOption test = new RunOption();
					
					train.inputNames = localNet.getRunOption(TFNet.TRAIN).inputNames;
					test.inputNames = localNet.getRunOption(TFNet.TRAIN).inputNames;
					train.numInputs = localNet.getRunOption(TFNet.TRAIN).numInputs;
					test.numInputs = localNet.getRunOption(TFNet.TRAIN).numInputs;
					
					train.setTargetNames(new String[]{});
					test.setTargetNames(new String[]{});
					
					if(i == 0 ) {
						train.setOutputNames(new String[] {String.format("GradientDescent"),
								String.format("global_norm/global_norm"),
								String.format("model_with_buckets/sequence_loss/truediv")});
						
						test.setOutputNames(new String[] {"model_with_buckets/sequence_loss/truediv"});
					} else {
						train.setOutputNames(new String[] {String.format("GradientDescent_%d",i),
								String.format("global_norm_%d/global_norm",i),
								String.format("model_with_buckets/sequence_loss_%d/truediv",i)});
						
						test.setOutputNames(new String[] {String.format("model_with_buckets/sequence_loss_%d/truediv",i)});
					}
					
					localNet.addCustomRunOption(String.format("bucket%d", i), train);
					localNet.addCustomRunOption(String.format("test_bucket%d", i), test);
				}
				Random r = new Random();
				
				
				System.out.println("Ready to learn!");
				
				// make contacts
				int myNum = t.next();
					
				// batch data
				int[][] encode_batch = new int[src_size[3]][batchSize];
				int[][] decode_batch = new int[dst_size[3]+1][batchSize];
				float[][] target_weight = new float[dst_size[3]][batchSize];
				
				// iterative training
				for(int i = 0; i < iteration; i++) {
					//select bucket
					int bucket_id = r.nextInt(4);
					
					// read from bucket
					for(int m = 0; m < batchSize; m++) {
						SeqRecord sample = reader[bucket_id].getSample();
						
						// encoder data
						while(sample.encode_data.size() < src_size[bucket_id]) {
							sample.encode_data.add(PAD);
						}
						Collections.reverse(sample.encode_data);
						int k = 0;
						Iterator<Integer> iter = sample.encode_data.iterator();
						while(iter.hasNext()) {
							encode_batch[k++][m]= iter.next();
						}
						
						// decoder data
						sample.decode_data.add(0,GO);
						while(sample.decode_data.size() < dst_size[bucket_id]) {
							sample.decode_data.add(PAD);
						}
						k = 0;
						iter = sample.decode_data.iterator();
						while(iter.hasNext()) {
							decode_batch[k++][m] = iter.next();
						}
						decode_batch[k][m] = 0;
						
						// target_weight
						for(k = 0; k < dst_size[bucket_id]; k++) {
							int target = decode_batch[k+1][m];
							if(k == dst_size[bucket_id] -1 || target == PAD)
								target_weight[k][m] = 0.0f;
							else
								target_weight[k][m] = 1.0f;
						}
					}
					
					// launch training
					localNet.setIntInputs(Arrays.asList(encode_batch), 0);
					localNet.setIntInputs(Arrays.asList(decode_batch),src_size[3]);
					localNet.setFloatInputs(Arrays.asList(target_weight),src_size[3] + dst_size[3]+1);
					
					localNet.runAction(String.format("bucket%d", bucket_id));
					float loss = localNet.getResult(2, 1)[0];
					System.out.println(String.format("Training bucket %d, perplexity: %f", bucket_id,Math.exp(loss)));
					
					// parameter exchange
					if((i+1) % period == 0) {
						List<Weight> weights = localNet.getWeights();
						localNet.setWeights(ParameterEASGDClient.exchangeWeight(serverHost, listenPort, weights, param.getMovingRate(),
								param.getFixedMovingRate(), param.getCompressed(),localNet.getVariableProfile(), 
								param.getCompressParam().getRatio(), param.getCompressParam().getApplyOption()));
					}
				}
				
				// finalize training
				System.out.println("Job's done");
			}
			
			private void readObject(ObjectInputStream in) throws Exception {
				in.defaultReadObject();
				localNet = new TFNet(param, graph);
				localNet.setWeights(init_weight);
			}
		});
        
        Date endTime = new Date();
		long time = endTime .getTime() - startTime.getTime();
        System.out.println(String.format("Training time: %f secs", (double) time / 1000));
        
        
        List<Weight> model = server.getServer_params();
        
        Path path = new Path("seq_model.weights");
        FileSystem fs = FileSystem.get(new Configuration());
		FSDataOutputStream fin = fs.create(path);
		ObjectOutputStream out = new ObjectOutputStream(fin);
		out.writeObject(model);
		out.close();
        
        // test
        net.setWeights(server.getServer_params());
        testNet(net, test_buckets, src_size, dst_size, batchSize);
        sc.close();
    }
    
    public static void testNet(TFNet net, List<SeqRecord>[] test_buckets, int[] src_size, int[] dst_size, int batchSize) throws Exception {
    	for(int b = 0; b < 4; b++) {
    		if(test_buckets[b].size() < batchSize * 2) {
    			System.out.println(String.format("bucket %d skipped", b));
    			continue;
    		}	
    		Iterator<SeqRecord> test_it = test_buckets[b].iterator();
        	
        	float loss = 0;
        	int i;
        	for(i = 0; i < 2; i++) {
				// make batch
				int[][] encode_batch = new int[src_size[3]][batchSize];
				int[][] decode_batch = new int[dst_size[3]+1][batchSize];
				float[][] target_weight = new float[dst_size[3]][batchSize];
				
				for(int m = 0; m < batchSize; m++) {
					SeqRecord sample = test_it.next();
					
					// encoder data
					int k = 1;
					Iterator<Integer> it = sample.encode_data.iterator();
					while(it.hasNext()) {
						encode_batch[src_size[b]-k][m]= it.next();
						k++;
					}
					for(;k <= src_size[b];k++)
						encode_batch[src_size[b] - k][m] = PAD;
					
					// decoder data
					k = 0;
					decode_batch[k++][m] = GO;
					it = sample.decode_data.iterator();
					while(it.hasNext()) {
						decode_batch[k++][m] = it.next();
					}
					for(;k < dst_size[b];k++)
						decode_batch[k][m] = PAD;
					decode_batch[k][m] = 0;
					
					// target_weight
					for(k = 0; k < dst_size[b]; k++) {
						int target = decode_batch[k+1][m];
						if(k == dst_size[b] -1 || target == PAD)
							target_weight[k][m] = 0.0f;
						else
							target_weight[k][m] = 1.0f;
					}
				}
				
				// launch testing
				
				net.setIntInputs(Arrays.asList(encode_batch), 0);
				net.setIntInputs(Arrays.asList(decode_batch),src_size[3]);
				net.setFloatInputs(Arrays.asList(target_weight),src_size[3] + dst_size[3]+1);
				net.runAction(String.format("test_bucket%d", b));
				loss += net.getResult(0, 1)[0];
			}
        	loss /= i;
        	System.out.println(String.format("for bucket %d perplexity: %f", b, Math.exp(loss)));
        }
    }
}
