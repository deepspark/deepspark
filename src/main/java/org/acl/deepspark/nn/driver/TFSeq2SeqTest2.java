package org.acl.deepspark.nn.driver;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import org.acl.deepspark.data.LMDBReader2;
import org.acl.deepspark.data.LMDBWriter2;
import org.acl.deepspark.data.SeqBatch;
import org.acl.deepspark.data.SeqRecord;
import org.acl.deepspark.data.Weight;
import org.acl.deepspark.nn.async.easgd.CoordinateProfile;
import org.acl.deepspark.nn.async.easgd.ParameterEASGDClient;
import org.acl.deepspark.nn.async.easgd.PartialExchangerManager;
import org.acl.deepspark.nn.async.easgd.PartialParameterExchanger;
import org.acl.deepspark.tf.RunOption;
import org.acl.deepspark.tf.SeqRecordLoader;
import org.acl.deepspark.tf.TFNet;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam;
import org.acl.deepspark.utils.DeepSparkParamLoader;
import org.acl.deepspark.utils.WeightUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;

import com.jmatio.io.MatFileWriter;
import com.jmatio.types.MLArray;
import com.jmatio.types.MLDouble;

import scala.Tuple2;

/**
 * Created by Hanjoo on 2016-01-18.
 */
public class TFSeq2SeqTest2 {
	static final int PAD = 0;
	static final int GO = 1;
	static final int EOS = 2;
	static final int UNK = 3;
	
	public static void main(String[] args) throws Exception {
    	SparkConf conf = new SparkConf().setAppName("Seq2SeqNMTExample")
                .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        
        JavaSparkContext sc = new JavaSparkContext(conf);
        final int numExecutors = conf.getInt("spark.executor.instances", -1);
        System.out.println("number of executors = " + numExecutors);
        
        final DeepSparkParam param = DeepSparkParamLoader.readConf(args[1]);
        System.out.println("Data Loading...");
        
        JavaRDD<String> en_ids = sc.textFile("ids40000_with_number.en");
        JavaRDD<String> fr_ids = sc.textFile("ids40000_with_number.fr");
        
        JavaRDD<String> en_test_ids = sc.textFile("test.en");
        JavaRDD<String> fr_test_ids = sc.textFile("test.fr");
        
        // english
        JavaPairRDD<Integer, List<Integer>> seq_en = en_ids.mapToPair(new PairFunction<String, Integer, List<Integer>>() {
			@Override
			public Tuple2<Integer, List<Integer>> call(String t) throws Exception {
				Scanner s = new Scanner(t);
				
				Integer key = s.nextInt();
				List<Integer> val = new ArrayList<Integer>();
				
				while(s.hasNextInt()) {
					val.add(s.nextInt());
				}
				s.close();
				return new Tuple2<Integer, List<Integer>>(key, val);
			}
		});
        
        JavaPairRDD<Integer, List<Integer>> seq_test_en = en_test_ids.mapToPair(new PairFunction<String, Integer, List<Integer>>() {
			@Override
			public Tuple2<Integer, List<Integer>> call(String t) throws Exception {
				Scanner s = new Scanner(t);
				
				Integer key = s.nextInt();
				List<Integer> val = new ArrayList<Integer>();
				
				while(s.hasNextInt()) {
					val.add(s.nextInt());
				}
				s.close();
				return new Tuple2<Integer, List<Integer>>(key, val);
			}
		});
        
        // french
        JavaPairRDD<Integer, List<Integer>> seq_fr = fr_ids.mapToPair(new PairFunction<String, Integer, List<Integer>>() {
			@Override
			public Tuple2<Integer, List<Integer>> call(String t) throws Exception {
				Scanner s = new Scanner(t);
				
				Integer key = s.nextInt();
				List<Integer> val = new ArrayList<Integer>();
				
				while(s.hasNextInt()) {
					val.add(s.nextInt());
				}
				s.close();
				val.add(EOS);
				return new Tuple2<Integer, List<Integer>>(key, val);
			}
		});
        
        JavaPairRDD<Integer, List<Integer>> seq_test_fr = fr_test_ids.mapToPair(new PairFunction<String, Integer, List<Integer>>() {
			@Override
			public Tuple2<Integer, List<Integer>> call(String t) throws Exception {
				Scanner s = new Scanner(t);
				
				Integer key = s.nextInt();
				List<Integer> val = new ArrayList<Integer>();
				
				while(s.hasNextInt()) {
					val.add(s.nextInt());
				}
				s.close();
				val.add(EOS);
				return new Tuple2<Integer, List<Integer>>(key, val);
			}
		});
        
        // join one
//        JavaPairRDD<Integer, Tuple2<List<Integer>, List<Integer>>> joined_seq = seq_en.join(seq_fr).repartition(numExecutors);
        
        // sampling..
        JavaPairRDD<Integer, Tuple2<List<Integer>, List<Integer>>> joined_seq = seq_en.join(seq_fr).repartition(numExecutors);
        JavaPairRDD<Integer, Tuple2<List<Integer>, List<Integer>>> joined_test_seq = seq_test_en.join(seq_test_fr);
        List<SeqRecord> test_data = joined_test_seq.mapValues(new Function<Tuple2<List<Integer>,List<Integer>>, SeqRecord>() {
			@Override
			public SeqRecord call(Tuple2<List<Integer>, List<Integer>> v1) throws Exception {
				SeqRecord data = new SeqRecord();
				data.encode_data = v1._1;
				data.decode_data = v1._2;
				return data;
			}
		}).values().collect();
        
        // make bucket db
        final String db_name_prefix = "bucket";
        final int[] src_size = new int[]{5,10,20,40};
        final int[] dst_size = new int[]{10,15,25,50};
        final int batchSize = 64;
        
        // test set
        Iterator<SeqRecord> iter = test_data.iterator();
        List<SeqRecord>[] test_buckets = new List[4];
        for(int i = 0; i < 4 ; i++) {
			test_buckets[i] = new ArrayList<SeqRecord>();
		}
        
        while(iter.hasNext()) {
        	SeqRecord data = iter.next();
        	for(int i = 0; i < src_size.length; i++) {
				int source_len = src_size[i];
				int destination_len = dst_size[i];
				
				if(data.encode_data.size() < source_len &&
						data.decode_data.size() < destination_len) {
					test_buckets[i].add(data);
					break;
				}
			}
        }
        
        // initialize learning
        final String serverHost = InetAddress.getLocalHost().getHostAddress();
        System.out.println("ParameterServer host: " + serverHost);
        
        final String graph = new String(Files.readAllBytes(Paths.get(args[0])));
        TFNet net = new TFNet(param, graph);
        final List<Weight> initWeight = net.getWeights();
        final int iteration = param.getTfIteration();
        final int period = param.getPeriod();
        
        // test options
        for(int i = 0; i < 4; i++) {
			RunOption test = new RunOption();
			
			test.inputNames = net.getRunOption(TFNet.TRAIN).inputNames;
			test.numInputs = net.getRunOption(TFNet.TRAIN).numInputs;
			
			test.setTargetNames(new String[]{});
			
			if(i == 0 ) {
				test.setOutputNames(new String[] {"model_with_buckets/sequence_loss/truediv"});
			} else {
				test.setOutputNames(new String[] {String.format("model_with_buckets/sequence_loss_%d/truediv",i)});
			}
			
			net.addCustomRunOption(String.format("test_bucket%d", i), test);
		}
        
        testNet(net, test_buckets, src_size, dst_size, batchSize);
        
        // training set
        joined_seq.foreach(new VoidFunction<Tuple2<Integer,Tuple2<List<Integer>,List<Integer>>>>() {
        	transient LMDBWriter2[] dbWriter;
			
			private void readObject(ObjectInputStream in) throws ClassNotFoundException, IOException {
				System.gc();
				in.defaultReadObject();
				this.dbWriter = new LMDBWriter2[4];
				
				for(int i = 0; i < 4 ; i++) {
					File dir = new File(db_name_prefix+i);
					if(dir.mkdirs())
						System.out.println("mkdir successfully done...");
					dbWriter[i] = new LMDBWriter2(db_name_prefix+i, 100);
				}
			}
			
			@Override
			public void call(Tuple2<Integer, Tuple2<List<Integer>, List<Integer>>> t) throws Exception {
				Tuple2<List<Integer>, List<Integer>> rec = t._2();
				
				SeqRecord data = new SeqRecord();
				data.encode_data = rec._1;
				data.decode_data = rec._2;
				
				for(int i = 0; i < src_size.length; i++) {
					int source_len = src_size[i];
					int destination_len = dst_size[i];
					
					if(data.encode_data.size() < source_len &&
							data.decode_data.size() < destination_len) {
						dbWriter[i].putSample(data);;
						break;
					}
				}
			}
		});
        
        // remove saved training data
        joined_seq.unpersist();
        
        // initial parameter exchanger
        final int listenPort = 12345;
        final float th = WeightUtil.findThreshold(initWeight, net.getVariableProfile(), param.getCompressParam().getRatio());
        
        System.out.println("Start Learning...");
        Date startTime = new Date();
        
        //make dummy RDD
        Integer[] tempkey = new Integer[numExecutors];
        for(int i = 0; i < tempkey.length; i++)
        	tempkey[i] = i;
        
        JavaRDD<Integer> rddkey = sc.parallelize(Arrays.asList(tempkey), numExecutors).cache();
        List<Tuple2<Integer, String>> nodeHosts = rddkey.mapToPair(new PairFunction<Integer, Integer, String>() {
			@Override
			public Tuple2<Integer, String> call(Integer t) throws Exception {
				return new Tuple2<Integer, String>(t, InetAddress.getLocalHost().getHostAddress());
			}
		}).collect();
        
        final Map<Integer, String> hostBook = buildHostMap(nodeHosts); 
        
        PartialExchangerManager manager = new PartialExchangerManager(initWeight, listenPort, numExecutors);
        manager.distributeProfiles();
        
        // learning
        rddkey.foreachPartition(new VoidFunction<Iterator<Integer>>() {
        	private transient TFNet localNet;
        	private long startTime;
        	
        	private void printLogTime(String mesg) {
        		System.out.println(String.format("[%f solver]: %s",  (float) (System.currentTimeMillis() - startTime ) / 1000, mesg));
        	}
        	
        	@Override
			public void call(Iterator<Integer> t) throws Exception {
        		System.gc();
        		int myID = t.next();
				LMDBReader2 reader[] = new LMDBReader2[4];
				
				for(int i = 0; i < 4; i++)
					reader[i]= new LMDBReader2(db_name_prefix+i);
				
				// difference map
				List<float[]>[] diffMap = new List[hostBook.size()];
				for(int i = 0; i < diffMap.length; i++)
					diffMap[i] = new ArrayList<float[]>();
				
				for(int i = 0; i < 4; i++) {
					RunOption train = new RunOption();
					RunOption test = new RunOption();
					
					train.inputNames = localNet.getRunOption(TFNet.TRAIN).inputNames;
					test.inputNames = localNet.getRunOption(TFNet.TRAIN).inputNames;
					train.numInputs = localNet.getRunOption(TFNet.TRAIN).numInputs;
					test.numInputs = localNet.getRunOption(TFNet.TRAIN).numInputs;
					
					train.setTargetNames(new String[]{});
					test.setTargetNames(new String[]{});
					
					if(i == 0 ) {
						train.setOutputNames(new String[] {String.format("Adam"),
								String.format("global_norm/global_norm"),
								String.format("model_with_buckets/sequence_loss/truediv")});
						
						test.setOutputNames(new String[] {"model_with_buckets/sequence_loss/truediv"});
					} else {
						train.setOutputNames(new String[] {String.format("Adam_%d",i),
								String.format("global_norm_%d/global_norm",i),
								String.format("model_with_buckets/sequence_loss_%d/truediv",i)});
						
						test.setOutputNames(new String[] {String.format("model_with_buckets/sequence_loss_%d/truediv",i)});
					}
					
					localNet.addCustomRunOption(String.format("bucket%d", i), train);
					localNet.addCustomRunOption(String.format("test_bucket%d", i), test);
				}
				
				List<CoordinateProfile[]> profiles = ParameterEASGDClient.getProfiles(serverHost, listenPort, myID);
				
				localNet.validateWeight(profiles, numExecutors);
				
				PartialParameterExchanger exchanger = new PartialParameterExchanger(initWeight, localNet.getVariableProfile(),
						listenPort, 2, myID, th, profiles, param);
				exchanger.startServer();
				
				// data loader
				SeqRecordLoader loader = new SeqRecordLoader(20, batchSize, src_size, dst_size, reader);
				loader.startLoader();
				
				ParameterEASGDClient.waitOtherProcess(serverHost, listenPort, myID);
				startTime = System.currentTimeMillis();
				
				int ex_counter = 0;
				long train_time_per_batch = 0;
				long exchange_time = 0;
				long train_time = 0;
				long exchange_normal = 0;
				long exchange_comp = 0;
				
				int display = 500;
				float checkLoss = 0.0f;
				
				List<Weight>[] weights = localNet.getWeights(profiles, hostBook.size());
//				WeightUtil.initDiffMap(diffMap, weights);
				
				exchanger.setStartTime(startTime);
				printLogTime("Ready to learn!");
				
				// iterative training
				for(int i = 0; i < iteration; i++) {
					// get next batch
					SeqBatch batch = loader.getNextBatch();
					
					// parameter exchange
					if((i+1) % period == 0) {
						// exchange weights
                		printLogTime(String.format("%d batches processed, and its avg. process time: %d ms", param.getPeriod(), train_time / param.getPeriod()));
                		train_time = 0L;
                		ex_counter++;
                		                		
						// get weight
                    	weights = localNet.getWeights(profiles, hostBook.size());
                    	
                    	// launch training
                    	long start_train = System.currentTimeMillis();	
						localNet.setIntInputs(Arrays.asList(batch.encodeBatch), 0);
						localNet.setIntInputs(Arrays.asList(batch.decodeBatch),src_size[3]);
						localNet.setFloatInputs(Arrays.asList(batch.targetWeight),src_size[3] + dst_size[3]+1);
						localNet.runAction(String.format("bucket%d", batch.bucketId));
						float loss = localNet.getResult(2, 1)[0];
						long end_tra = System.currentTimeMillis();
						train_time += end_tra -start_train;
	                	train_time_per_batch += end_tra -start_train;
						checkLoss += loss / display;
						
						//exchanging 
                    	long start = System.currentTimeMillis();
                    	ParameterEASGDClient.exchangePartialWeightRandomly(hostBook, myID, listenPort, weights, i+1, param);
						localNet.addWeights(weights, profiles, hostBook.size());
						long end = System.currentTimeMillis();
						
						printLogTime(String.format("client: %d-th weight exchanged (%d ms)", ex_counter, end-start));
						exchange_time += (end-start);
						
						if(param.getCompressed() & i+1 > param.getCompressParam().getPretrainIteration())
							exchange_comp += (end-start);
						else
							exchange_normal += (end-start);
					} else {
						long start_train = System.currentTimeMillis();	
						// launch training
						localNet.setIntInputs(Arrays.asList(batch.encodeBatch), 0);
						localNet.setIntInputs(Arrays.asList(batch.decodeBatch),src_size[3]);
						localNet.setFloatInputs(Arrays.asList(batch.targetWeight),src_size[3] + dst_size[3]+1);
						localNet.runAction(String.format("bucket%d", batch.bucketId));
						
						float loss = localNet.getResult(2, 1)[0];
						long end_tra = System.currentTimeMillis();
						train_time += end_tra -start_train;
	                	train_time_per_batch += end_tra -start_train;
						checkLoss += loss / display;
					}
					
                	// display
                	if((i+1) % display == 0) {
                		printLogTime(String.format("Iteration %d, training perplexity: %f", i+1, Math.exp(checkLoss)));
                		checkLoss = 0f;
                	}
                	
					
				}
				
				printLogTime(String.format("avg. batch computing time: %d ms", train_time_per_batch / iteration));
                printLogTime(String.format("avg. communication time: %d ms", exchange_time / ex_counter));
                
                if(param.getCompressed()) {
                	if(param.getCompressParam().getPretrainIteration() != 0)
                		printLogTime(String.format("avg. normal communication time: %d ms", exchange_normal * param.getPeriod() /
                				param.getCompressParam().getPretrainIteration() ));
                	
                	if(iteration - param.getCompressParam().getPretrainIteration() != 0)
                		printLogTime(String.format("avg. compressed communication time: %d ms", exchange_comp * param.getPeriod() 
                				/ (iteration - param.getCompressParam().getPretrainIteration()) ));
                }
                
				loader.stopLoader();
				// finalize training
				System.out.println("Training is done");
				
				ParameterEASGDClient.gather(serverHost, listenPort, exchanger);				
			}
			
			private void readObject(ObjectInputStream in) throws Exception {
				in.defaultReadObject();
				localNet = new TFNet(param, graph);
				localNet.setWeights(initWeight);
			}
		});
        
        Date endTime = new Date();
		long time = endTime .getTime() - startTime.getTime();
        System.out.println(String.format("Training time: %f secs", (double) time / 1000));
        
        List<Weight>[] final_weight = manager.getParameters();
        net.setWeights(final_weight);
        testNet(net, test_buckets, src_size, dst_size, batchSize);
        
        //save all.
        Configuration hdfs_conf = new Configuration();
        FileSystem fs = FileSystem.get(hdfs_conf);
        List<Weight>[] weights = new List[numExecutors];
        
        for(int j = param.getSnapshotPeriod() * param.getPeriod();
        		j <= param.getTfIteration(); j += param.getSnapshotPeriod() * param.getPeriod()) {
	        for(int i = 0; i < numExecutors; i++) {
	        	FSDataInputStream fin = fs.open(new Path(String.format("%s_%d_%d.weight", param.getSnapshotPrefix(), i, j)));
		        ObjectInputStream in = new ObjectInputStream(fin);
		        
		        weights[i] = (List<Weight>) in.readObject();
		        in.close();
		        fin.close();
	        }

	        net.setWeights(weights);
	        testNet(net, test_buckets, src_size, dst_size, batchSize);
	        
	        System.out.println(String.format("%s_%d.tfweights saved...", param.getSnapshotPrefix(),j));
	        Path path = new Path(String.format("%s_%d.tfweights", param.getSnapshotPrefix(),j));
	        
	        FSDataOutputStream fin = fs.create(path);
			ObjectOutputStream out = new ObjectOutputStream(fin);
			out.writeObject(net.getWeights());
			out.close();
			fin.close();
        }
        
        sc.close();
    }
    
	private static Map<Integer, String> buildHostMap(List<Tuple2<Integer, String>> nodeHosts) {
		Iterator<Tuple2<Integer, String>> iter = nodeHosts.iterator();
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		while(iter.hasNext()) {
			Tuple2<Integer, String> t = iter.next();
			map.put(t._1, t._2);
		}
		return map;
	}

	public static void testNet(TFNet net, List<SeqRecord>[] test_buckets, int[] src_size, int[] dst_size, int batchSize) throws Exception {
    	float total_loss = 0.0f;
    	int total_size = 0;
		for(int bucket_id = 0; bucket_id < 4; bucket_id++) {
    		total_size += test_buckets[bucket_id].size();
			float bucket_loss = 0.0f;
    		int numBatch = test_buckets[bucket_id].size() / batchSize;
    		Iterator<SeqRecord> test_it = test_buckets[bucket_id].iterator();
    		
    		for(int i = 0; i < numBatch; i++) {
    			// make batches to fit batchSize
				int[][] encode_batch = new int[src_size[3]][batchSize];
				int[][] decode_batch = new int[dst_size[3]+1][batchSize];
				float[][] target_weight = new float[dst_size[3]][batchSize];
				
				for(int m = 0; m < batchSize; m++) {
					SeqRecord sample = test_it.next();
					
					// encoder data
					int k = 1;
					Iterator<Integer> it = sample.encode_data.iterator();
					while(it.hasNext()) {
						encode_batch[src_size[bucket_id]-k][m]= it.next();
						k++;
					}
					for(;k <= src_size[bucket_id];k++)
						encode_batch[src_size[bucket_id] - k][m] = PAD;
					
					// decoder data
					k = 0;
					decode_batch[k++][m] = GO;
					it = sample.decode_data.iterator();
					while(it.hasNext()) {
						decode_batch[k++][m] = it.next();
					}
					for(;k < dst_size[bucket_id];k++)
						decode_batch[k][m] = PAD;
					decode_batch[k][m] = 0;
					
					// target_weight
					for(k = 0; k < dst_size[bucket_id]; k++) {
						int target = decode_batch[k+1][m];
						if(k == dst_size[bucket_id] -1 || target == PAD)
							target_weight[k][m] = 0.0f;
						else
							target_weight[k][m] = 1.0f;
					}
				}
				
				net.setIntInputs(Arrays.asList(encode_batch), 0);
				net.setIntInputs(Arrays.asList(decode_batch),src_size[3]);
				net.setFloatInputs(Arrays.asList(target_weight),src_size[3] + dst_size[3]+1);
				net.runAction(String.format("test_bucket%d", bucket_id));
				bucket_loss += net.getResult(0, 1)[0] * batchSize;
    		}
    		
    		// remains
    		int rBatchSize = test_buckets[bucket_id].size() % batchSize;
    		if( rBatchSize != 0) {
    			// make batches to fit batchSize
    			net.setBatchSize(rBatchSize);
				int[][] encode_batch = new int[src_size[3]][rBatchSize];
				int[][] decode_batch = new int[dst_size[3]+1][rBatchSize];
				float[][] target_weight = new float[dst_size[3]][rBatchSize];
				
				for(int m = 0; m < rBatchSize; m++) {
					SeqRecord sample = test_it.next();
					
					// encoder data
					int k = 1;
					Iterator<Integer> it = sample.encode_data.iterator();
					while(it.hasNext()) {
						encode_batch[src_size[bucket_id]-k][m]= it.next();
						k++;
					}
					for(;k <= src_size[bucket_id];k++)
						encode_batch[src_size[bucket_id] - k][m] = PAD;
					
					// decoder data
					k = 0;
					decode_batch[k++][m] = GO;
					it = sample.decode_data.iterator();
					while(it.hasNext()) {
						decode_batch[k++][m] = it.next();
					}
					for(;k < dst_size[bucket_id];k++)
						decode_batch[k][m] = PAD;
					decode_batch[k][m] = 0;
					
					// target_weight
					for(k = 0; k < dst_size[bucket_id]; k++) {
						int target = decode_batch[k+1][m];
						if(k == dst_size[bucket_id] -1 || target == PAD)
							target_weight[k][m] = 0.0f;
						else
							target_weight[k][m] = 1.0f;
					}
				}
				
				net.setIntInputs(Arrays.asList(encode_batch), 0);
				net.setIntInputs(Arrays.asList(decode_batch),src_size[3]);
				net.setFloatInputs(Arrays.asList(target_weight),src_size[3] + dst_size[3]+1);
				net.runAction(String.format("test_bucket%d", bucket_id));
				
				bucket_loss += net.getResult(0, 1)[0] * rBatchSize;			
    		}
    		net.setBatchSize(batchSize);
    		
    		total_loss += bucket_loss;
    		
    		bucket_loss /= test_buckets[bucket_id].size();
    		System.out.println(String.format("for bucket %d perplexity: %f", bucket_id, Math.exp(bucket_loss)));
    	}
        
		total_loss /= total_size;
		System.out.println(String.format("total perplexity: %f", Math.exp(total_loss)));
    }
	
	protected static void saveMatFile(List<Weight>[] weights, String format) throws IOException {
    	FileSystem fs = FileSystem.get(new Configuration()); 
		
		int size = 0;
		for(int i = 0; i < weights.length; i++) {
			Iterator<Weight> iter = weights[i].iterator();
			
			while(iter.hasNext()) {
				size += iter.next().floatData.length;
			}
		}
		
		double[] j_weight = new double[size];
		
		
		int index= 0;
		for(int i = 0; i < weights.length; i++) {
			Iterator<Weight> iter_w = weights[i].iterator();
			
			while(iter_w.hasNext()) {
				float[] w = iter_w.next().floatData;
				
				for(int k =0; k < w.length; k++) {
					j_weight[index+k] = w[k];
				}
				
				index += w.length;
			}
		}
		
		MLDouble weight = new MLDouble("weight", j_weight, 1);
		
		ArrayList<MLArray> a = new ArrayList<MLArray>();
		a.add(weight);
		
		new MatFileWriter(format, a);
		
		fs.copyFromLocalFile(new Path(format), new Path(format));
	}
}
