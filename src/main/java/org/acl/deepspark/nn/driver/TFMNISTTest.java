package org.acl.deepspark.nn.driver;

import java.io.ObjectInputStream;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.acl.deepspark.data.Minibatch;
import org.acl.deepspark.data.Weight;
import org.acl.deepspark.nn.async.easgd.CoordinateProfile;
import org.acl.deepspark.nn.async.easgd.ParameterEASGDClient;
import org.acl.deepspark.nn.async.easgd.ParameterEASGDServer;
import org.acl.deepspark.nn.async.easgd.PartialExchangerManager;
import org.acl.deepspark.nn.async.easgd.PartialParameterExchanger;
import org.acl.deepspark.tf.TFNet;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam;
import org.acl.deepspark.utils.DeepSparkParamLoader;
import org.acl.deepspark.utils.MnistLoader;
import org.acl.deepspark.utils.WeightUtil;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;

import scala.Tuple2;

/**
 * Created by Hanjoo on 2016-01-18.
 */
public class TFMNISTTest {
    public static void main(String[] args) throws Exception {
    	SparkConf conf = new SparkConf().setAppName("TFMNISTTest")
                .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        
        JavaSparkContext sc = new JavaSparkContext(conf);

        final DeepSparkParam param = DeepSparkParamLoader.readConf(args[1]);
        System.out.println("Data Loading...");
        Minibatch[] train_sample = MnistLoader.loadMinibatchFromHDFS("/data/MNIST/mnist_train.txt", 100,true);
        Minibatch[] test_sample = MnistLoader.loadMinibatchFromHDFS("/data/MNIST/mnist_test.txt", 100,true);

        int numExecutors = conf.getInt("spark.executor.instances", -1);
        System.out.println("number of executors = " + numExecutors);
        JavaRDD<Minibatch> train_data;
        if(numExecutors != -1)        	
        	train_data = sc.parallelize(Arrays.asList(train_sample), numExecutors).cache();
        else
        	train_data = sc.parallelize(Arrays.asList(train_sample)).cache();
        
        final String serverHost = InetAddress.getLocalHost().getHostAddress();
        System.out.println("ParameterServer host: " + serverHost);
        
        final String graph = new String(Files.readAllBytes(Paths.get(args[0])));
        TFNet net = new TFNet(param, graph);
        final List<Weight> initWeight = net.getWeights();
        final int iteration = param.getTfIteration();
        final int period = param.getPeriod();
        
        float acc = 0;
        for(int i = 0; i < 100; i++) {
        	net.setFloatInputs(Arrays.asList(test_sample[i].data, test_sample[i].label, new float[]{1.0f}),0);
        	net.test();
        	float[] a =net.getResult(0, 1);
        	acc += a[0];
        }
        acc /= 100;
        System.out.println(acc);
        
        // make internal list
        JavaRDD<List<Minibatch>> train_list = train_data.mapPartitions(new FlatMapFunction<Iterator<Minibatch>, List<Minibatch>>() {
			@Override
			public Iterable<List<Minibatch>> call(Iterator<Minibatch> t) throws Exception {
				ArrayList<List<Minibatch>> ret = new ArrayList<List<Minibatch>>();
				
				List<Minibatch> a = new LinkedList<Minibatch>();
				while(t.hasNext()) {
					a.add(t.next());
				}
				
				ret.add(a);
				return ret;
			}
		},true);
        
        //make dummy RDD
        Integer[] tempkey = new Integer[numExecutors];
        for(int i = 0; i < tempkey.length; i++)
        	tempkey[i] = i;
        
        JavaRDD<Integer> rddkey = sc.parallelize(Arrays.asList(tempkey), numExecutors).repartition(numExecutors);
        List<String> nodeHosts = train_list.map(new Function<List<Minibatch>, String>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1568682441468673814L;

			@Override
			public String call(List<Minibatch> v1) throws Exception {
				return InetAddress.getLocalHost().getHostAddress();
			}
        	
		}).collect();
        
        final Map<Integer, String> hostBook = buildHostMap(nodeHosts); 
        
        final int listenPort = 12345;


        /*// for single server 
        
        ParameterEASGDServer server = new ParameterEASGDServer(net, listenPort, numExecutors, param);
        server.startServer();
        */
        
        final float th = WeightUtil.findThreshold(initWeight, net.getVariableProfile(), param.getCompressParam().getRatio());        
        PartialExchangerManager manager = new PartialExchangerManager(initWeight, listenPort, numExecutors);
        manager.distributeProfiles();
        
		System.out.println("Start Learning...");
        Date startTime = new Date();
        train_list.foreachPartition(new VoidFunction<Iterator<List<Minibatch>>>() {
        	private transient TFNet localNet;
        	
        	private void readObject(ObjectInputStream in) throws Exception {
				in.defaultReadObject();
				localNet = new TFNet(param, graph);
				localNet.setWeights(initWeight);
			}
			@Override
			public void call(Iterator<List<Minibatch>> t) throws Exception {
				
				List<Minibatch> data_list = t.next();
				
				// find myID
				int myID = -1;
				for(int i = 0; i < hostBook.size();i++) {
					if(hostBook.get(i).equals(InetAddress.getLocalHost().getHostAddress())) {
						myID = i;
						break;
					}
				}
				
				// error!
				if(myID == -1) {
					System.out.println("WARNING: host " + InetAddress.getLocalHost().getHostAddress());

					System.out.println("HOST BOOK>");
					for(int i = 0; i < hostBook.size();i++) {
						System.out.println(hostBook.get(i));
					}
					
					throw new Exception();
				}
				
				// difference map
				List<float[]>[] diffMap = new List[hostBook.size()];
				for(int i = 0; i < diffMap.length; i++)
					diffMap[i] = new ArrayList<float[]>();
				
				System.out.println(String.format("my ID: %d", myID));
				
				int list_size = data_list.size();
				System.out.println(String.format("%d samples loaded", list_size));
				List<CoordinateProfile[]> profiles = ParameterEASGDClient.getProfiles(serverHost, listenPort, myID);
				PartialParameterExchanger exchanger = new PartialParameterExchanger(initWeight, localNet.getVariableProfile(),
						listenPort, 2, myID, th, profiles, param);
				exchanger.startServer();
				ParameterEASGDClient.waitOtherProcess(serverHost, listenPort, myID);

				// iterative training
				for(int i = 0; i < iteration; i++) {
					// sample batch set
					Minibatch d = data_list.get(i % list_size);
					
					// launch training
					localNet.setFloatInputs(Arrays.asList(d.data, d.label, new float[]{0.5f}),0);
					localNet.train();
					
					
					// parameter exchange
					if((i+1) % period == 0) {
						System.out.println("Exchange!!");
						List<Weight>[] weights = localNet.getWeights(profiles, hostBook.size());
						ParameterEASGDClient.exchangePartialWeight(hostBook, myID, listenPort, weights, diffMap, i, param);
						
						localNet.setWeights(weights);
					}
				}
				
				// finalize training
				System.out.println("Job's done");
				ParameterEASGDClient.gather(serverHost, listenPort, exchanger);
			}
		});
        Date endTime = new Date();
		long time = endTime .getTime() - startTime.getTime();
        System.out.println(String.format("Training time: %f secs", (double) time / 1000));
        
        // test
        //net.setWeights(server.getServer_params());
        List<Weight>[] final_weight = manager.getParameters();
        net.setWeights(final_weight);
        
        acc = 0;
        for(int i = 0; i < 100; i++) {
        	net.setFloatInputs(Arrays.asList(test_sample[i].data, test_sample[i].label, new float[]{1.0f}),0);
        	net.test();
        	float[] a =net.getResult(0, 1);
        	acc += a[0];
        }
        acc /= 100;
        System.out.println(acc);
        
        sc.close();
    }
    
    private static Map<Integer, String> buildHostMap(List<String> nodeHosts) {
		Iterator<String> iter = nodeHosts.iterator();
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		int  i =0;
		while(iter.hasNext()) {
			map.put(i++, iter.next());
		}
		return map;
	}
}
