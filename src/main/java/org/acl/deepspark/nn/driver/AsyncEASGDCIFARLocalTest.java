package org.acl.deepspark.nn.driver;

import java.io.File;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.acl.deepspark.data.LMDBWriter;
import org.acl.deepspark.data.Record;
import org.acl.deepspark.data.Weight;
import org.acl.deepspark.nn.CaffeNet;
import org.acl.deepspark.nn.async.easgd.DistAsyncEASGDSolverWithPruning;
import org.acl.deepspark.nn.async.easgd.DistAsyncEASGDSolverWithPruningRandom;
import org.acl.deepspark.utils.CIFARLoader;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam;
import org.acl.deepspark.utils.DeepSparkParamLoader;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

public class AsyncEASGDCIFARLocalTest {
    public static void main(String[] args) throws Exception {
    	SparkConf conf = new SparkConf().setAppName("AsyncCIFARTest")
                .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        
        JavaSparkContext sc = new JavaSparkContext(conf);
        
        DeepSparkParam param = DeepSparkParamLoader.readConf(args[1]);
        System.out.println("Data Loading...");
        Record[] train_sample = CIFARLoader.loadFromHDFSText("/data/CIFAR-10/train_batch.txt", false);
        Record[] test_sample = CIFARLoader.loadFromHDFSText("/data/CIFAR-10/test_batch.txt", false);

        int numExecutors = conf.getInt("spark.executor.instances", -1);
        System.out.println("number of executors = " + numExecutors);
        JavaRDD<Record> train_data,test_data;
        if(numExecutors != -1) {
        	train_data = sc.parallelize(Arrays.asList(train_sample), numExecutors).cache();
        	test_data= sc.parallelize(Arrays.asList(test_sample), numExecutors).cache();
        } else {
        	train_data = sc.parallelize(Arrays.asList(train_sample)).cache();
        	test_data= sc.parallelize(Arrays.asList(test_sample)).cache();
        }
        
        File trainFile = new File("cifar10_train_lmdb");
        trainFile.mkdirs();
        File testFile = new File("cifar10_test_lmdb");
        testFile.mkdirs();
        
        LMDBWriter trainWriter = new LMDBWriter("cifar10_train_lmdb");
        for(int i = 0; i < train_sample.length; i++) {
        	trainWriter.putSample(train_sample[i]);
        }
        trainWriter.closeLMDB();
        
        LMDBWriter testWriter = new LMDBWriter("cifar10_test_lmdb");
        for(int i = 0; i < test_sample.length; i++) {
        	testWriter.putSample(test_sample[i]);
        }
        testWriter.closeLMDB();
        
        CaffeNet net = new CaffeNet(args[0]);
        
        String serverHost = InetAddress.getLocalHost().getHostAddress();
        final int port = 10020;
        System.out.println("ParameterServer host: " + serverHost);
        DistAsyncEASGDSolverWithPruningRandom driver = new DistAsyncEASGDSolverWithPruningRandom(net, serverHost, port, param);
        
        driver.prepareLocal(train_data, "cifar10_train_lmdb", numExecutors);
        driver.prepareLocal(test_data, "cifar10_test_lmdb", numExecutors);
        
        System.out.println("Start Learning...");
        Date startTime = new Date();
                
        Integer[] tempkey = new Integer[numExecutors];
        for(int i = 0; i < tempkey.length; i++)
        	tempkey[i] = i;
        JavaRDD<Integer> nodeNum = sc.parallelize(Arrays.asList(tempkey), numExecutors);
        driver.trainWithLMDB(nodeNum, null, numExecutors);
        
        Date endTime = new Date();
        float[] a = driver.testWithLMDB();
        long time = endTime.getTime() - startTime.getTime();
        System.out.println(String.format("Training time: %f secs", (double) time / 1000));
        System.out.println(String.format("Final Accuracy: %f ", a[0]));
        
        //save all.
        Configuration hdfs_conf = new Configuration();
        FileSystem fs = FileSystem.get(hdfs_conf);
        List<Weight>[] weights = new List[numExecutors];
        
        for(int j = param.getSnapshotPeriod() * param.getPeriod();
        		j <= net.getSolverSetting().getMaxIter(); j += param.getSnapshotPeriod() * param.getPeriod()) {
	        for(int i = 0; i < numExecutors; i++) {
	        	FSDataInputStream fin = fs.open(new Path(String.format("%s_%d_%d.weight", param.getSnapshotPrefix(), i, j)));
		        ObjectInputStream in = new ObjectInputStream(fin);
		        
		        weights[i] = (List<Weight>) in.readObject();
		        in.close();
		        fin.close();
	        }

	        driver.getNet().setWeights(weights);
	        a = driver.testWithLMDB();
	        System.out.println(String.format("Iteration %d :Accuracy: %f ", j, a[0]));
	        
	        driver.saveCaffeModel(weights, String.format("%s_%d.caffemodel", param.getSnapshotPrefix(),j), fs);
        }
        net.close();
        sc.close();
    }
}
