package org.acl.deepspark.nn.async.easgd;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.acl.deepspark.data.Weight;
import org.acl.deepspark.data.WeightPackage;
import org.acl.deepspark.data.WeightStat;
import org.acl.deepspark.utils.DeepSparkConf.CompressionParam.Regularizer;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam.Variables;
import org.acl.deepspark.utils.WeightUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class PartialParameterExchanger implements Runnable {
	private int myID;
	private ServerSocket exchangeSocket;
	private boolean stopSign = false;
	private Thread thread;
	
	private int updateCount = 0;
	private int tick;
	private String dirName;
	private int applyOption;
	private int numWorker;
	
	
	private long startTime;
	
	private List<Weight> server_params;
	
	private float movingRate;
	private float sparseCoeff;
	private float threshold;
	
	private PrintStream logStream = null;
	
	private List<Variables> wInfo;
	private ExecutorService connectionService;
	
	private DeepSparkParam param;
	private Regularizer l1_norm; 
	
	public void setStartTime(long time) {
		startTime = time;
	}
	
	private void printLogTime(String mesg) {
		if(logStream == null)
			System.out.println(String.format("[%f parameter exchanger]: %s", (float) (System.currentTimeMillis() - startTime) / 1000 ,mesg));
		else {
			System.out.println(String.format("[%f parameter exchanger]: %s", (float) (System.currentTimeMillis() - startTime) / 1000 ,mesg));
			logStream.println(String.format("[%f parameter exchanger]: %s", (float) (System.currentTimeMillis() - startTime) / 1000 ,mesg));
		}
	}
	
	public List<Weight> getServerParam() {
		return server_params;
	}
	
	public PartialParameterExchanger(List<Weight> initWeight, List<Variables> wInfo, 
			int listenPort, int numThread, int myID, float th,
			List<CoordinateProfile[]> profiles, DeepSparkParam param) throws IOException {
		this.myID = myID;
		this.applyOption = param.getCompressParam().getApplyOption();
		this.param = param;
		
		server_params = initPartialWeights(initWeight, profiles, myID);
		tick = 0;
						
		// moving rate, alpha
		movingRate = param.getMovingRate();
		sparseCoeff = param.getFixedMovingRate();
		threshold = th;
		
		try {
			this.wInfo = wInfo;
			this.l1_norm = param.getCompressParam().getPretrainMethod(); 
			
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Compression disabled!");
		}
		
		// network setting
		exchangeSocket = new ServerSocket(listenPort);
		connectionService = Executors.newFixedThreadPool(numThread);
		
		// param log init
//		String logfile = String.format("%s/log_%d.txt", dirName, myID);
//		Path path = new Path(logfile);
//		FSDataOutputStream fin = FileSystem.get(new Configuration()).create(path);
//		logStream = new PrintStream(fin);
		
		printLogTime("Exchanger Ready!");
	}
		
	private List<Weight> initPartialWeights(List<Weight> entire, List<CoordinateProfile[]> profiles, int myID) {
		Iterator<Weight> wIter = entire.iterator();
		Iterator<CoordinateProfile[]> pIter = profiles.iterator();
		
		List<Weight> partialWeights = new ArrayList<Weight>();
		
		while(wIter.hasNext()) {
			Weight w = wIter.next();
			CoordinateProfile[] p = pIter.next();
			numWorker = p.length;
			Weight partialWeight = new Weight();
			partialWeight.layerIndex = w.layerIndex;
			partialWeight.offset = p[myID].offset;
			partialWeight.floatData = new float[p[myID].length];
			System.arraycopy(w.floatData, p[myID].offset, partialWeight.floatData, 0, p[myID].length);
			
			partialWeights.add(partialWeight);
		}
		
		return partialWeights;
	}
	
	public void stopServer() {
		stopSign = true;
		try {
			exchangeSocket.close();
			thread.join();
			logStream.close();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void startServer() throws IOException {
		thread = new Thread(this);
		this.startTime = System.currentTimeMillis();
		thread.start();
	}
	
	
	public List<Weight> getParams() {
		return server_params;
	}

	public int getUpdateCount() {
		return updateCount;
	}

	@Override
	public void run() {
		printLogTime("Start Server...");
		
		try {
			while(!stopSign) {
				Socket a = exchangeSocket.accept();
				connectionService.execute(new EASGDThread(a,++updateCount));						
				
				if(tick != 0 && updateCount % tick == 0) {
					String filename = String.format("%s/state_iter_%d_%d.weight", dirName, myID, updateCount);
					Path statePath = new Path(filename);
					
					FSDataOutputStream stateIn = FileSystem.get(new Configuration()).create(statePath);
					ObjectOutputStream out = new ObjectOutputStream(stateIn);
					out.writeObject(server_params);
					out.close();
					printLogTime(String.format("%s saved..",filename));
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		connectionService.shutdown();
		while (!connectionService.isTerminated()) {
        }
	}
	
	private class EASGDThread implements Runnable {
		private Socket s;
		private int iter;
		
		public EASGDThread(Socket s, int updateCount) {
			this.s = s;
			this.iter = updateCount;
		}
		
		@Override
		public void run() {
			try {
				ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
				ObjectInputStream in = new ObjectInputStream(s.getInputStream());
				float eff_rate = movingRate;
				List<Weight> output;
				
				@SuppressWarnings("unchecked")
				WeightPackage clientData = (WeightPackage) in.readObject();
				List<Weight> client_w = clientData.weightList;
				
				WeightPackage serverData = new WeightPackage();
				serverData.isCompressed = clientData.isCompressed;
				serverData.threshold = threshold;
				
				if(clientData.isCompressed) {
					serverData.factor = clientData.factor;
					output = WeightUtil.compressWeightListForServer(server_params, client_w, wInfo);
					serverData.stat = WeightStat.generateWeightStat(server_params);
					Iterator<Weight> it = output.iterator();
					Iterator<Weight> it2 = server_params.iterator();
					int org = 0;
					int comp = 0;
					while(it.hasNext()) {
						Weight w = it.next();
						if(w.floatData == null)
							comp += w.sparseData.data.length;
						else
							comp += w.floatData.length;
						org += it2.next().floatData.length;
					}
					
					printLogTime(String.format("deflated to %.2f%%", 1.25f * comp / org * 100));					
				} else {
					output = server_params;
				}
				
				serverData.weightList = output;
				
				out.writeObject(serverData);
				in.close();
				out.flush();
				out.close();
				s.close();
				
				printLogTime(String.format("parameter exchanged (threshold: %f)", threshold));
				
				threshold = clientData.threshold;
				//threshold *= (1.0f - eff_rate); 
				//threshold += eff_rate * clientData.threshold;
				
				// EASGD update
				List<Weight> server_w = server_params;

				Iterator<Weight> s_iter = server_w.iterator();
				Iterator<Weight> c_iter = client_w.iterator();
				//Iterator<Variables> p_iter = wInfo.iterator();
				while(s_iter.hasNext()) {
					WeightUtil.EASGDUpdateWithDiff(c_iter.next(), s_iter.next(), clientData.stat, serverData.stat, clientData.factor, param);
				}
				
				// save weights
				if(param.hasSnapshotPeriod() &&
        				iter % (param.getSnapshotPeriod() * numWorker) == 0 ) {
        			long start = System.currentTimeMillis();
					String filename = String.format("%s_%d_%d.weight", param.getSnapshotPrefix(), myID, iter / numWorker * param.getPeriod());
					Path path = new Path(filename);
					
					FileSystem fs = FileSystem.get(new Configuration()); 
					FSDataOutputStream fin = fs.create(path);
					ObjectOutputStream fout = new ObjectOutputStream(fin);
					fout.writeObject(getServerParam());
					fout.close();
					long end = System.currentTimeMillis();
					printLogTime(String.format("%s saved... (%d ms)", filename, end-start));
				}
				
			} catch (IOException | RuntimeException e) {
				if(!stopSign) {
					printLogTime("got Error!");
					e.printStackTrace();
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
