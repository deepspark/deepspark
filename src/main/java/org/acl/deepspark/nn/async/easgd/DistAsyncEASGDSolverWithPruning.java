package org.acl.deepspark.nn.async.easgd;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.acl.deepspark.data.ByteRecord;
import org.acl.deepspark.data.LMDBWriter;
import org.acl.deepspark.data.Record;
import org.acl.deepspark.data.Weight;
import org.acl.deepspark.nn.CaffeNet;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam;
import org.acl.deepspark.utils.WeightUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;

import com.jmatio.io.MatFileWriter;
import com.jmatio.types.MLArray;
import com.jmatio.types.MLDouble;

import caffe.Caffe.SolverParameter.SolverMode;

public class DistAsyncEASGDSolverWithPruning implements Serializable {
	private static final long serialVersionUID = -5070368428661536358L;

	private transient CaffeNet net;

    private String host;
    private int port;
    
	private DeepSparkParam param;
	
	public DistAsyncEASGDSolverWithPruning(CaffeNet net, String host, int port, DeepSparkParam param) {
		this.net = net;
        this.host = host;
        this.port = port;
        
        DeepSparkParam.Builder b = DeepSparkParam.newBuilder(param);
        b.addAllVariables(net.getVariableProfile());
        
        this.param = b.build();
        
    }
	
	private long startTime;
    
	private void printLog(String mesg) {
		System.out.println(String.format("[%s solver]: %s", new Timestamp(System.currentTimeMillis()),mesg));
	}
	
	private void printLogTime(String mesg) {
		System.out.println(String.format("[%f solver]: %s",  (float) (System.currentTimeMillis() - startTime ) / 1000, mesg));
	}
	
	public CaffeNet getNet() {
		return net;
	}
    
    public void trainWithLMDB(JavaRDD<Integer> nodeNums, String path, final int numWorker) throws Exception{
        SolverMode mode = net.getSolverSetting().getSolverMode();
        if(mode == SolverMode.GPU) {
        	printLog("running on GPU...");
        	net.setGPU();
        } else {
        	printLog("running on CPU...");
        	net.setCPU();
        }
        
    	net.printLearnableWeightInfo();
    	
    	final byte[] solverParams = net.getSolverSetting().toByteArray();
        final byte[] netParams = net.getNetConf().toByteArray();
        
        final List<Weight> initialWeight = net.getWeights();
    	
        List<String> nodeHosts = nodeNums.map(new Function<Integer, String>() {
			private static final long serialVersionUID = 1568682441468673814L;

			@Override
			public String call(Integer v1) throws Exception {
				System.out.println("I got " + v1.intValue());
				return InetAddress.getLocalHost().getHostAddress();
			}
        	
		}).collect();
        
        final Map<Integer, String> hostBook = buildHostMap(nodeHosts);
        // final float th = WeightUtil.findThreshold(initialWeight, net.getVariableProfile(), param.getCompressParam().getRatio());
		
    	PartialExchangerManager manager = new PartialExchangerManager(net.getWeights(), port, numWorker);
        manager.distributeProfiles();
    	
        startTime = System.currentTimeMillis();
        printLogTime("Initiate Training Process...");
        nodeNums.foreachPartition(new VoidFunction<Iterator<Integer>>() {
            private static final long serialVersionUID = -4641037124928675165L;
            
			public void call(Iterator<Integer> samples) throws Exception {
				System.gc();
								
				// find myID
				int myID = -1;
				for(int i = 0; i < hostBook.size();i++) {
					if(hostBook.get(i).equals(InetAddress.getLocalHost().getHostAddress())) {
						printLogTime(String.format("I'm %d", i));
						myID = i;
						break;
					}
				}
				
				// error!
				if(myID == -1) {
					System.out.println("WARNING: host " + InetAddress.getLocalHost().getHostAddress());

					System.out.println("HOST BOOK>");
					for(int i = 0; i < hostBook.size();i++) {
						System.out.println(hostBook.get(i));
					}
					
					throw new Exception();
				}
				
				// difference map for masking
				List<float[]>[] diffMap = new List[hostBook.size()];
				for(int i = 0; i < diffMap.length; i++) {
					diffMap[i] = new ArrayList<float[]>();
				}
				
				startTime = System.currentTimeMillis();
            	// init net
                CaffeNet net = new CaffeNet(solverParams, netParams);
                SolverMode mode = net.getSolverSetting().getSolverMode();
                if(mode == SolverMode.GPU) {
                	printLog("running on GPU...");
                	net.setGPU();
                } else {
                	printLog("running on CPU...");
                	net.setCPU();
                }
                
               // for worker node snapshot
				Configuration conf=null; FileSystem fs =null;
				conf = new Configuration();
				fs = FileSystem.get(conf);
			    
                // get initial weight
                net.printLearnableWeightInfo();
                net.setWeights(initialWeight);                
                
                int iteration = net.getIteration();
                float th = 0.0f;
                
                List<CoordinateProfile[]> profiles = ParameterEASGDClient.getProfiles(host, port, myID);
				PartialParameterExchanger exchanger = new PartialParameterExchanger(initialWeight, net.getVariableProfile(),
						port, 2, myID, th, profiles, param);
				exchanger.startServer();
				exchanger.setStartTime(startTime);
				ParameterEASGDClient.waitOtherProcess(host, port, myID);
                
				int ex_counter = 0;
				long train_time_per_batch = 0;
				long exchange_time = 0;
				long exchange_normal = 0;
				long exchange_comp = 0;
				long train_time = 0;
				List<Weight>[] weights = net.getWeights(profiles, hostBook.size());
				WeightUtil.initDiffMap(diffMap, weights);
				
				printLogTime("Training Start!");
				
                for (int i = 0; i < iteration; i++) {
                	long start_train = System.currentTimeMillis();
                	net.trainWithLocal();
                	
                	long end_tra = System.currentTimeMillis();
                	train_time += end_tra -start_train;
                	train_time_per_batch += end_tra -start_train;
                	                	
                	if( (i + 1) % param.getPeriod() == 0 ) {
                		// exchange weights
                		printLogTime(String.format("%d batches processed, and its avg. process time: %d ms", param.getPeriod(), train_time / param.getPeriod()));
                		train_time = 0L;
                		long start = System.currentTimeMillis();
                		
                		// threshold decay = exp( - a * iteration )
                		int i_eff = Math.max(0, i+1 - param.getCompressParam().getPretrainIteration());
						float decay = (float) (Math.expm1(-param.getCompressParam().getRatioDecay() * (i_eff)) + 1) * (1.0f - param.getCompressParam().getRatio());
						th = WeightUtil.findThresholdFromDiff(diffMap, net.getVariableProfile(), (1.0f - decay));
                		
						// get weight
                		if(param.getCompressed() & i >= param.getCompressParam().getPretrainIteration())
                    		net.getWeightWithMasking2(weights, diffMap,th);
                    	else
                    		net.getWeights(weights);
						
						ParameterEASGDClient.exchangePartialWeightWithFixedTh(hostBook, myID, port, weights, diffMap, th, i, param);
						net.setWeights(weights);
						ex_counter++;
						
						boolean debug = false;
						if(debug && myID == 0 &&
								ex_counter % param.getSnapshotPeriod() == 0) {
							saveMatFile(diffMap,weights, String.format("%s_%d_debug.mat", param.getSnapshotPrefix(), i+1));
						}
						
						long end = System.currentTimeMillis();
						printLogTime(String.format("client: %d-th weight exchanged (%d ms)", ex_counter, end-start));
						exchange_time += (end-start);
						if(param.getCompressed() & i >= param.getCompressParam().getPretrainIteration())
							exchange_comp += (end-start);
						else
							exchange_normal += (end-start);
                	}
                }
                
                printLogTime(String.format("avg. batch computing time: %d ms", train_time_per_batch / iteration));
                printLogTime(String.format("avg. communication time: %d ms", exchange_time / ex_counter));
                printLogTime(String.format("avg. normal communication time: %d ms", exchange_normal * param.getPeriod() /
                		param.getCompressParam().getPretrainIteration() ));
                printLogTime(String.format("avg. compressed communication time: %d ms", exchange_comp * param.getPeriod() 
                		/ (iteration - param.getCompressParam().getPretrainIteration()) ));
                
                // finalize training
                printLogTime("Job's done");
				ParameterEASGDClient.gather(host, port, exchanger);
            }
        });
        Date endTime = new Date();
		long time = endTime.getTime() - startTime;

        printLogTime("Training complete...");
        printLogTime(String.format("Training time: %f secs", (double) time / 1000));
        List<Weight>[] final_weight = manager.getParameters();
        
        net.setWeights(final_weight);
    }
  
    protected void saveMatFile(List<float[]>[] diffMap, List<Weight>[] weights, String format) throws IOException {
    	FileSystem fs = FileSystem.get(new Configuration()); 
		
		int size = 0;
		for(int i = 0; i < diffMap.length; i++) {
			Iterator<float[]> iter = diffMap[i].iterator();
			
			while(iter.hasNext()) {
				size += iter.next().length;
			}
		}
		
		double[] j_diff = new double[size];
		double[] j_weight = new double[size];
		
		
		int index= 0;
		for(int i = 0; i < diffMap.length; i++) {
			Iterator<float[]> iter_diff = diffMap[i].iterator();
			Iterator<Weight> iter_w = weights[i].iterator();
			
			while(iter_diff.hasNext()) {
				float[] diff = iter_diff.next();
				float[] w = iter_w.next().floatData;
				
				for(int k =0; k < diff.length; k++) {
					j_diff[index+k] = diff[k];
					j_weight[index+k] = w[k];
				}
				
				index += diff.length;
			}
		}
		
		
		MLDouble diff = new MLDouble("diff", j_diff, 1);
		MLDouble weight = new MLDouble("weight", j_weight, 1);
		
		ArrayList<MLArray> a = new ArrayList<MLArray>();
		a.add(diff);
		a.add(weight);
		
		new MatFileWriter(format, a);
		
		fs.copyFromLocalFile(new Path(format), new Path(format));
	}

	public float[] test(List<Record> data) {
    	net.setTestData(net.buildBlobs(data));
        return net.test();
    }
    
    public float[] testWithLMDB() {
    	return net.testWithLMDB();
    }

	public void saveWeight(String pathString) throws FileNotFoundException, IOException {
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		Path path = new Path(pathString);
		
		FSDataOutputStream fin = fs.create(path);
		ObjectOutputStream out = new ObjectOutputStream(fin);
		out.writeObject(net.getWeights());
		out.close();
	}
	
	public void saveCaffeModel(List<Weight>[] weights, String pathString, FileSystem fs) {
		try {
			net.setWeights(weights);
			net.snapshot(pathString);
			
			fs.copyFromLocalFile(new Path(pathString), new Path(pathString));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public void prepareLocal(JavaRDD<Record> data, final String output, int workerSize) {
		JavaRDD<Record> reparted_data = data.repartition(workerSize);
		
		reparted_data.foreach(new VoidFunction<Record>() {
			transient int count;
			transient LMDBWriter dbWriter;
			
			private void readObject(ObjectInputStream in) throws ClassNotFoundException, IOException {
				System.gc();
				in.defaultReadObject();
				File dir = new File(output);
				if(dir.mkdirs())
					printLog("mkdir successfully done...");
				
				dbWriter = new LMDBWriter(output, 1000);
				count = 0;
			}
			
			@Override
			public void call(Record arg0) throws Exception {
				dbWriter.putSample(arg0);
				
				if((++count % 1000) == 0) {
					printLog(String.format("%d images saved...", count));
				}
			}
			
			@Override
			public void finalize() {
				try {
					dbWriter.closeLMDB();
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				printLog(String.format("Total %d images saved...", count));
			}
		});
	}
	
	public void prepareLocalByte(JavaRDD<ByteRecord> data, final String output, int workerSize) {
		JavaRDD<ByteRecord> reparted_data = data.repartition(workerSize);
		
		reparted_data.foreach(new VoidFunction<ByteRecord>() {
			transient int count;
			transient LMDBWriter dbWriter;
			
			private void readObject(ObjectInputStream in) throws ClassNotFoundException, IOException {
				System.gc();
				in.defaultReadObject();
				File dir = new File(output);
				if(dir.mkdirs())
					printLog("mkdir successfully done...");
				
				dbWriter = new LMDBWriter(output, 1000);
				count = 0;
			}
			
			@Override
			public void call(ByteRecord arg0) throws Exception {
				dbWriter.putSample(arg0);
				
				if((++count % 1000) == 0) {
					printLog(String.format("%d images saved...", count));
				}
			}
			
			@Override
			public void finalize() {
				try {
					dbWriter.closeLMDB();
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				printLog(String.format("Total %d images saved...", count));
			}
		});
	}
	
	public void prepareLocalDummy(JavaRDD<Integer> rdd_key, final String output, final int[] dim) {
		rdd_key.foreachPartition(new VoidFunction<Iterator<Integer>>() {
			@Override
			public void call(Iterator<Integer> arg0) throws Exception {
				int batchsize = 1;
				String source = output;
				int[] dummySize = dim;
				File dir = new File(source);
				if(!dir.exists()) {
					dir.mkdirs();
					
					LMDBWriter d = new LMDBWriter(source, batchsize);
					ByteRecord s = new ByteRecord();
					s.data = new byte[dummySize[0] * dummySize[1] * dummySize[2]];
					s.label = 0;
					s.dim = new int[] {dummySize[0], dummySize[1], dummySize[2]};
							
					for(int i = 0; i < batchsize; i++) {
						d.putSample(s);
					}
					d.closeLMDB();
				}
			}
		});
	}
	
	public void prepareLocalByteBySample(JavaRDD<ByteRecord> data, final String output, int workerSize, double fraction) {
		JavaRDD<ByteRecord> reparted_data = data.sample(true, fraction).repartition(workerSize);
		
		reparted_data.foreach(new VoidFunction<ByteRecord>() {
			transient int count;
			transient LMDBWriter dbWriter;
			
			private void readObject(ObjectInputStream in) throws ClassNotFoundException, IOException {
				System.gc();
				in.defaultReadObject();
				File dir = new File(output);
				if(dir.mkdirs())
					printLog("mkdir successfully done...");
				
				dbWriter = new LMDBWriter(output, 1000);
				count = 0;
			}
			
			@Override
			public void call(ByteRecord arg0) throws Exception {
				dbWriter.putSample(arg0);
				
				if((++count % 1000) == 0) {
					printLog(String.format("%d images saved...", count));
				}
			}
			
			@Override
			public void finalize() {
				try {
					dbWriter.closeLMDB();
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				printLog(String.format("Total %d images saved...", count));
			}
		});
	}
	
	private static Map<Integer, String> buildHostMap(List<String> nodeHosts) {
		Iterator<String> iter = nodeHosts.iterator();
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		int  i =0;
		while(iter.hasNext()) {
			map.put(i++, iter.next());
		}
		return map;
	}
}
