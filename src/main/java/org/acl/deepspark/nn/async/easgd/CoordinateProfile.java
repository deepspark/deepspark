package org.acl.deepspark.nn.async.easgd;

import java.io.Serializable;

public class CoordinateProfile implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6105939288732346674L;
	
	public int offset;
	public int length;
}
