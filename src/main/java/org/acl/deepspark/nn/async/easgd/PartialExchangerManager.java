package org.acl.deepspark.nn.async.easgd;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.acl.deepspark.data.Weight;

public class PartialExchangerManager implements Runnable {
	private int listenPort;
	private ServerSocket exchangeSocket;
	private int numWorker;
	private List<Weight>[] data;
	
	private List<CoordinateProfile[]> profiles;
	
	private Thread t;
	
	public PartialExchangerManager(List<Weight> list, int listenPort, int numWorker) throws IOException {
		this.listenPort = listenPort;
		this.numWorker = numWorker;
		
		profiles = new ArrayList<CoordinateProfile[]>();
		data = new List[numWorker];
		
		Iterator<Weight> wIter = list.iterator();
		while(wIter.hasNext()) {
			CoordinateProfile[] profile = new CoordinateProfile[numWorker];
			Weight w = wIter.next();
			int partSize = w.floatData.length / numWorker;
			
			for(int i = 0; i < numWorker;i++) {
				profile[i] = new CoordinateProfile();
				profile[i].offset = i * partSize;
				
				if(i == (numWorker-1)) // last part
					profile[i].length = w.floatData.length - profile[i].offset;
				else
					profile[i].length = partSize;
			}
			
			profiles.add(profile);
		}
	}
	
	public void distributeProfiles() throws IOException {
		t = new Thread(this);
		t.start();
	}
	
	public List<Weight>[] getParameters() {
		if(t != null)
			try {
				t.join();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		return data;
	}
	
	@Override
	public void run() {
		try {
			exchangeSocket = new ServerSocket(listenPort);
			Socket[] workers = new Socket[numWorker];
			
			System.out.println("Barrier Start!");
			for(int i = 0; i < numWorker; i++) {
				workers[i] = exchangeSocket.accept();
				System.out.println(String.format("Barrier counter %d", i));
			}
			System.out.println("Barrier End!");
			
			System.out.println("distribute profiles");
			for(int i = 0; i < numWorker;i++) {
				System.out.println(String.format("Sent profile map to %d", i));
				ObjectOutputStream out = new ObjectOutputStream(workers[i].getOutputStream());
				out.writeObject(profiles);
				out.close();
				workers[i].close();
			}
			
			for(int i = 0; i < numWorker; i++) {
				workers[i] = exchangeSocket.accept();
				workers[i].getOutputStream().write(10);
				workers[i].close();
				System.out.println(String.format("ready node %d", i));
			}
			
			
			System.out.println("final result barrier start!");
			for(int i = 0; i < numWorker; i++) {
				workers[i] = exchangeSocket.accept();
			}
			System.out.println("final result barrier End!");
			
			System.out.println("gathering start!");
			for(int i = 0; i < numWorker; i++) {
				Socket s = workers[i];
				s.getOutputStream().write(0);
				ObjectInputStream in = new ObjectInputStream(s.getInputStream());
				data[i] = (List<Weight>) in.readObject();
				s.close();
			}
			System.out.println("gathering end!");
			
			exchangeSocket.close();
			
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
