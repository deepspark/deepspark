package org.acl.deepspark.nn.async.easgd;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.acl.deepspark.data.Weight;
import org.acl.deepspark.data.WeightPackage;
import org.acl.deepspark.data.WeightStat;
import org.acl.deepspark.utils.DeepSparkConf.CompressionParam.Regularizer;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam.Variables;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import com.jmatio.io.MatFileWriter;
import com.jmatio.types.MLArray;
import com.jmatio.types.MLDouble;

import org.acl.deepspark.utils.WeightUtil;

import caffe.Caffe.SolverParameter;

public class ParameterEASGDClient {
	private static float updateFactor = 1.0f;
	
	public static List<Weight> exchangeWeight(String host, int port, List<Weight> client_w, float coeff1, float coeff2, 
			boolean compress, List<Variables> weightProfile, float ratio, int option) throws Exception {
		long start = System.currentTimeMillis();
		
		WeightPackage clientData = new WeightPackage();
		List<Weight> output;
		if(compress) {
			
			Date startTime = new Date();
			float th = WeightUtil.findThreshold(client_w, weightProfile, ratio);
			
			clientData.isCompressed = true;
			clientData.threshold = th;
			
			output = new ArrayList<Weight>();
			
			Iterator<Weight> iter = client_w.iterator();
			Iterator<Variables> pIter = weightProfile.iterator();
			
			long orig_size = 0;
			long comp_size = 0;
			while(iter.hasNext()) {
				Weight w = iter.next();
				if(pIter.next().getSkipCompress())
					output.add(w);
				else {
					Weight comp = WeightUtil.compress(w, th);
					comp_size = comp.sparseData.data.length * 5;
					orig_size = w.floatData.length * 4;
					output.add(comp);
				}
			}
			
			Date endTime = new Date();
			System.out.println(String.format("compressed size: %.2f%% for %d ms", 
					( (float) comp_size / orig_size * 100), (endTime.getTime() - startTime.getTime())));
			
		} else {
			clientData.isCompressed = false;
			output = client_w;
		}
		clientData.weightList = output;
		
		Socket s = new Socket(host, port);
		
		ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
		ObjectInputStream in = new ObjectInputStream(s.getInputStream());
		
		@SuppressWarnings("unchecked")
		WeightPackage serverData = (WeightPackage) in.readObject();
		List<Weight> server_w = serverData.weightList;
		System.out.println("client: weight recieved");
		
		out.writeObject(clientData);
		System.out.println("client: weight sent");
		s.close();
		
		if(server_w.size() != client_w.size()) {
			throw new Exception("weight size mismatched!");
		}
		
		Iterator<Weight> c_iter = client_w.iterator();
		Iterator<Weight> s_iter = server_w.iterator();
		
		while(c_iter.hasNext()) {
			Weight c_weight = c_iter.next();
			Weight s_weight = s_iter.next();
			
			WeightUtil.EASGDUpdate(s_weight, c_weight, coeff1, coeff2, serverData.threshold, option, Regularizer.L2);	
		}
		
		long end = System.currentTimeMillis();
		System.out.println(String.format("client: weight exchanged (%d ms)", end-start));
		return client_w;
	}
	
	public static List<Weight> exchangeWeight(String host, int port, WeightPackage clientData, float coeff1, float coeff2, int option) throws Exception {
		long start = System.currentTimeMillis();
		Socket s = new Socket(host, port);
		
		ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
		ObjectInputStream in = new ObjectInputStream(s.getInputStream());
		
		WeightPackage serverData = (WeightPackage) in.readObject();
		List<Weight> server_w = serverData.weightList;
		System.out.println("client: weight recieved");
		
		out.writeObject(clientData);
		System.out.println("client: weight sent");
		in.close();
		out.flush();
		out.close();
		s.close();
		
		List<Weight> client_w = clientData.weightList;
		if(server_w.size() != client_w.size()) {
			throw new Exception("weight size mismatched!");
		}
		
		Iterator<Weight> c_iter = client_w.iterator();
		Iterator<Weight> s_iter = server_w.iterator();
		
		while(c_iter.hasNext()) {
			Weight c_weight = c_iter.next();
			Weight s_weight = s_iter.next();
			
			WeightUtil.EASGDUpdate(s_weight, c_weight, coeff1,coeff2, serverData.threshold, option, Regularizer.L2);	
		}
		
		long end = System.currentTimeMillis();
		System.out.println(String.format("client: weight exchanged (%d ms)", end-start));
		return client_w;
	}
	
	public static List<Weight> exchangeWeight(String host, int port, WeightPackage clientData, float coeff1, int option) throws Exception {
		long start = System.currentTimeMillis();
		Socket s = new Socket(host, port);
		
		ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
		ObjectInputStream in = new ObjectInputStream(s.getInputStream());
		
		WeightPackage serverData = (WeightPackage) in.readObject();
		List<Weight> server_w = serverData.weightList;
		System.out.println("client: weight recieved");
		
		out.writeObject(clientData);
		System.out.println("client: weight sent");
		s.close();
		
		List<Weight> client_w = clientData.weightList;
		if(server_w.size() != client_w.size()) {
			throw new Exception("weight size mismatched!");
		}
		
		Iterator<Weight> c_iter = client_w.iterator();
		Iterator<Weight> s_iter = server_w.iterator();
		
		while(c_iter.hasNext()) {
			Weight c_weight = c_iter.next();
			Weight s_weight = s_iter.next();
			
			WeightUtil.EASGDUpdate(s_weight, c_weight, coeff1, 0.0f, serverData.threshold, option, Regularizer.L2);	
		}
		
		long end = System.currentTimeMillis();
		System.out.println(String.format("client: weight exchanged (%d ms)", end-start));
		return client_w;
	}

	public static List<CoordinateProfile[]> getProfiles(String serverHost, int listenPort, int myID) throws UnknownHostException, IOException, ClassNotFoundException {
		System.out.println("create coordinate profiles");
		Socket s = new Socket(serverHost, listenPort);
		ObjectInputStream in = new ObjectInputStream(s.getInputStream());
		List<CoordinateProfile[]> res = (List<CoordinateProfile[]>) in.readObject();
		s.close();
		System.out.println("coordinate profiles ready");
		return res;
	}
	
	public static void waitOtherProcess(String serverHost, int listenPort, int myID) throws UnknownHostException, IOException {
		Socket s = new Socket(serverHost, listenPort);
		s.getInputStream().read();
		s.close();
	}

	
	public static List<Weight>[] exchangePartialWeight(Map<Integer, String> hostBook,
			int myID, int listenPort, List<Weight>[] client_w, int iteration, DeepSparkParam param) throws Exception {
		long start = System.currentTimeMillis();
		
		float coeff1 = param.getMovingRate(); // moving rate
		float coeff2 = param.getFixedMovingRate(); // l1-norm coeff
		List<Variables> weightProfile = param.getVariablesList();
		
		// compression options
		boolean compress = (param.getCompressed() & iteration > param.getCompressParam().getPretrainIteration());
		float th = WeightUtil.findThreshold(client_w, weightProfile, param.getCompressParam().getRatio());
		int option = param.getCompressParam().getApplyOption();
		Regularizer l1_norm = param.getCompressParam().getPretrainMethod();
		
		// random ordering for avoiding contention
		List<Integer> order = new ArrayList<Integer>(hostBook.keySet());
		Collections.shuffle(order);
		
		Iterator<Integer> iter_order = order.iterator();
		while(iter_order.hasNext()) {
			int i = iter_order.next();
			
			WeightPackage clientData = new WeightPackage();
			clientData.threshold = th;
			
			List<Weight> output = client_w[i];
			
			if(compress) {
				clientData.isCompressed = true;
				
				List<Weight> compressed = WeightUtil.compressWeightList(output, th, weightProfile);
				Iterator<Weight> it = compressed .iterator();
				Iterator<Weight> it2 = output.iterator();
				Iterator<Variables> it3 = weightProfile.iterator();
				
				int org = 0;
				int comp = 0;
				while(it.hasNext()) {
					boolean skipCompress = it3.next().getSkipCompress();
					if(skipCompress)
						comp += it.next().floatData.length;
					else
						comp += it.next().sparseData.data.length;
					
					org += it2.next().floatData.length;
				}
				System.out.println(String.format("deflated to %.2f%%", 1.25f * comp / org * 100));
				
				output = compressed; 
			}
			
			clientData.weightList = output;
			
			Socket s = new Socket(hostBook.get(i), listenPort);
			
			ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
			ObjectInputStream in = new ObjectInputStream(s.getInputStream());
			
			out.writeObject(clientData);
			System.out.println(String.format("client: weight sent to %s (%d)", hostBook.get(i), i));
			
			
			WeightPackage serverData = (WeightPackage) in.readObject();
			List<Weight> server_w = serverData.weightList;
			System.out.println(String.format("client: received weight from %s (%d)", hostBook.get(i), i));
			
			s.close();
			
			if(server_w.size() != client_w[i].size()) {
				throw new Exception("weight size mismatched!");
			}
			
			Iterator<Weight> c_iter = client_w[i].iterator();
			Iterator<Weight> s_iter = server_w.iterator();
			Iterator<Variables> p_iter = weightProfile.iterator();
			
			while(c_iter.hasNext()) {
				Weight c_weight = c_iter.next();
				Weight s_weight = s_iter.next();
				Variables profile = p_iter.next();
				
				if(profile.getSkipCompress())
					WeightUtil.EASGDUpdate(s_weight, c_weight, coeff1);
				else 
					WeightUtil.EASGDUpdate(s_weight, c_weight, coeff1, coeff2, serverData.threshold, option, l1_norm);
			
			}
		}
		
		long end = System.currentTimeMillis();
		System.out.println(String.format("client: weight exchanged (%d ms)", end-start));
		
		return client_w;
	}

	public static List<Weight>[] exchangePartialWeight(Map<Integer, String> hostBook,
			int myID, int listenPort, List<Weight>[] client_w, List<float[]>[] diffMap, int iteration, DeepSparkParam param) throws Exception {
		float coeff1 = param.getMovingRate(); // moving rate
		float coeff2 = param.getFixedMovingRate(); // l1-norm coeff
		List<Variables> weightProfile = param.getVariablesList();
		
		// compression options
		boolean compress = (param.getCompressed() & iteration > param.getCompressParam().getPretrainIteration());
		float th = WeightUtil.findThresholdFromDiff(diffMap, param.getVariablesList(), param.getCompressParam().getRatio());
		Regularizer l1_norm = param.getCompressParam().getPretrainMethod();
		
		// random ordering for avoiding contention
		List<Integer> order = new ArrayList<Integer>(hostBook.keySet());
		Collections.shuffle(order);
		
		Iterator<Integer> iter_order = order.iterator();
		while(iter_order.hasNext()) {
			int i = iter_order.next();
			
			WeightPackage clientData = new WeightPackage();
			clientData.threshold = th;
			
			List<Weight> output = client_w[i];
			
			if(compress) {
				clientData.isCompressed = true;
				
				List<Weight> compressed = WeightUtil.compressWeightListWithDiffMap(output, th, weightProfile, diffMap[i]);
				Iterator<Weight> it = compressed .iterator();
				Iterator<Weight> it2 = output.iterator();
				Iterator<Variables> it3 = weightProfile.iterator();
				
				int org = 0;
				int comp = 0;
				while(it.hasNext()) {
					boolean skipCompress = it3.next().getSkipCompress();
					if(skipCompress)
						comp += it.next().floatData.length;
					else
						comp += it.next().sparseData.data.length;
					
					org += it2.next().floatData.length;
				}
				System.out.println(String.format("deflated to %.2f%%", 1.25f * comp / org * 100));
				
				output = compressed; 
			}
			
			clientData.weightList = output;
			
			Socket s = new Socket(hostBook.get(i), listenPort);
			
			ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
			ObjectInputStream in = new ObjectInputStream(s.getInputStream());
			
			out.writeObject(clientData);
			System.out.println(String.format("client: weight sent to %s (%d)", hostBook.get(i), i));
			
			
			WeightPackage serverData = (WeightPackage) in.readObject();
			List<Weight> server_w = serverData.weightList;
			System.out.println(String.format("client: received weight from %s (%d)", hostBook.get(i), i));
			
			s.close();
			
			if(server_w.size() != client_w[i].size()) {
				throw new Exception("weight size mismatched!");
			}
			
			WeightUtil.applyDiffWithMomentum(client_w[i], server_w, diffMap[i], param.getCompressParam().getPretrainMomentum());
			
			Iterator<Weight> c_iter = client_w[i].iterator();
			Iterator<Weight> s_iter = server_w.iterator();
			Iterator<Variables> p_iter = weightProfile.iterator();
			
			while(c_iter.hasNext()) {
				Weight c_weight = c_iter.next();
				Weight s_weight = s_iter.next();
				Variables profile = p_iter.next();
				
				if(profile.getSkipCompress())
					WeightUtil.EASGDUpdate(s_weight, c_weight, coeff1);
				else 
					WeightUtil.EASGDUpdateWithDiff(s_weight, c_weight, param);
			
			}
		}
	
		return client_w;
	}	
	
	public static List<Weight>[] exchangePartialWeightWithFixedTh(Map<Integer, String> hostBook,
			int myID, int listenPort, List<Weight>[] client_w, List<float[]>[] diffMap, float th, int iteration, DeepSparkParam param) throws Exception {
		float coeff1 = param.getMovingRate(); // moving rate
		float coeff2 = param.getFixedMovingRate(); // l1-norm coeff
		List<Variables> weightProfile = param.getVariablesList();
		
		// compression options
		boolean compress = (param.getCompressed() & iteration >= param.getCompressParam().getPretrainIteration());
		Regularizer l1_norm = param.getCompressParam().getPretrainMethod();
		
		// random ordering for avoiding contention
		List<Integer> order = new ArrayList<Integer>(hostBook.keySet());
		Collections.shuffle(order);
		
		Iterator<Integer> iter_order = order.iterator();
		while(iter_order.hasNext()) {
			int i = iter_order.next();
			
			WeightPackage clientData = new WeightPackage();
			clientData.threshold = th;
			
			List<Weight> output = client_w[i];
			
			if(compress) {
				clientData.isCompressed = true;
				
				List<Weight> compressed = WeightUtil.compressWeightListWithDiffMap(output, th, weightProfile, diffMap[i]);
				Iterator<Weight> it = compressed .iterator();
				Iterator<Weight> it2 = output.iterator();
				Iterator<Variables> it3 = weightProfile.iterator();
				
				int org = 0;
				int comp = 0;
				while(it.hasNext()) {
					boolean skipCompress = it3.next().getSkipCompress();
					if(skipCompress)
						comp += it.next().floatData.length;
					else
						comp += it.next().sparseData.data.length;
					
					org += it2.next().floatData.length;
				}
				System.out.println(String.format("deflated to %.2f%%", 1.25f * comp / org * 100));
				
				output = compressed; 
			}
			
			clientData.weightList = output;
			
			Socket s = new Socket(hostBook.get(i), listenPort);
			
			ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
			ObjectInputStream in = new ObjectInputStream(s.getInputStream());
			
			out.writeObject(clientData);
			System.out.println(String.format("client: weight sent to %s (%d)", hostBook.get(i), i));
			
			
			WeightPackage serverData = (WeightPackage) in.readObject();
			List<Weight> server_w = serverData.weightList;
			System.out.println(String.format("client: received weight from %s (%d)", hostBook.get(i), i));
			
			s.close();
			
			if(server_w.size() != client_w[i].size()) {
				throw new Exception("weight size mismatched!");
			}
			
			WeightUtil.applyDiffWithMomentum(client_w[i], server_w, diffMap[i], param.getCompressParam().getPretrainMomentum());
			
			Iterator<Weight> c_iter = client_w[i].iterator();
			Iterator<Weight> s_iter = server_w.iterator();
			Iterator<Variables> p_iter = weightProfile.iterator();
			
			while(c_iter.hasNext()) {
				Weight c_weight = c_iter.next();
				Weight s_weight = s_iter.next();
				Variables profile = p_iter.next();
				
				if(profile.getSkipCompress() | !param.getCompressed())
					WeightUtil.EASGDUpdate(s_weight, c_weight, coeff1);
				else 
					WeightUtil.EASGDUpdateWithDiff(s_weight, c_weight, param);
			
			}
		}
	
		return client_w;
	}	
	
	public static List<Weight>[] exchangePartialWeightRandomly(Map<Integer, String> hostBook,
			int myID, int listenPort, List<Weight>[] client_w, int iteration, DeepSparkParam param) throws Exception {
		float coeff1 = param.getMovingRate(); // moving rate
		float coeff2 = param.getFixedMovingRate(); // l1-norm coeff
		List<Variables> weightProfile = param.getVariablesList();
		
		// compression options
		boolean compress = (param.getCompressed() & iteration > param.getCompressParam().getPretrainIteration());
		Regularizer l1_norm = param.getCompressParam().getPretrainMethod();
		
		// random ordering for avoiding contention
		List<Integer> order = new ArrayList<Integer>(hostBook.keySet());
		Collections.shuffle(order);
		
		Iterator<Integer> iter_order = order.iterator();
		while(iter_order.hasNext()) {
			int i = iter_order.next();
			
			WeightPackage clientData = new WeightPackage();
			
			List<Weight> output = client_w[i];
			
			if(compress) {
				clientData.isCompressed = true;
				clientData.threshold = param.getCompressParam().getRatio();
				clientData.stat = WeightStat.generateWeightStat(output);
				List<Weight> compressed = WeightUtil.compressWeightListRandomly2(output, weightProfile, clientData.threshold);
				Iterator<Weight> it = compressed .iterator();
				Iterator<Weight> it2 = output.iterator();
				
				int org = 0;
				int comp = 0;
				while(it.hasNext()) {
					Weight w = it.next();
					if(w.floatData != null)
						comp += w.floatData.length;
					else
						comp += w.sparseData.data.length;
					
					org += it2.next().floatData.length;
				}
				System.out.println(String.format("deflated to %.2f%%", 1.25f * comp / org * 100));
				
				output = compressed; 
			}
			
			clientData.weightList = output;
			
			Socket s = new Socket(hostBook.get(i), listenPort);
			
			ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
			ObjectInputStream in = new ObjectInputStream(s.getInputStream());
			
			out.writeObject(clientData);
			System.out.println(String.format("client: weight sent to %s (%d)", hostBook.get(i), i));
			
			
			WeightPackage serverData = (WeightPackage) in.readObject();
			List<Weight> server_w = serverData.weightList;
			System.out.println(String.format("client: received weight from %s (%d)", hostBook.get(i), i));
			
			s.close();
			
			if(server_w.size() != client_w[i].size()) {
				throw new Exception("weight size mismatched!");
			}
			
			Iterator<Weight> c_iter = client_w[i].iterator();
			Iterator<Weight> s_iter = server_w.iterator();
			
			while(c_iter.hasNext()) {
				Weight c_weight = c_iter.next();
				Weight s_weight = s_iter.next();
				WeightUtil.EASGDUpdateClient(s_weight, c_weight, serverData.stat, clientData.stat, 1.0f, param);
			}
		}
	
		return client_w;
	}
	
	public static void gather(String serverHost, int listenPort, PartialParameterExchanger exchanger) throws UnknownHostException, IOException {
		Socket s = new Socket(serverHost, listenPort);
		s.getInputStream().read(); // barrier
		
		System.out.print("sending final result...  ");
		List<Weight> params = exchanger.getParams();
		ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
		out.writeObject(params);
		s.close();
	}

	public static List<Weight>[] exchangePartialWeightRandomly(Map<Integer, String> hostBook, int myID, int listenPort,
			List<Weight>[] client_w, SolverParameter solverSetting, int iteration, DeepSparkParam param) throws Exception {
		List<Variables> weightProfile = param.getVariablesList();
		// debug mode?
		boolean debug = false;
		if(debug && myID == 0 &&
				iteration % (param.getSnapshotPeriod() * param.getPeriod()) == 0) {
			saveMatFile(client_w, String.format("%s_%d_worker_debug.mat", param.getSnapshotPrefix(), iteration));
		}
		
		// compression options
		boolean compress = (param.getCompressed() & iteration > param.getCompressParam().getPretrainIteration());
		
		// random ordering for avoiding contention
		List<Integer> order = new ArrayList<Integer>(hostBook.keySet());
		Collections.shuffle(order);
		List<Weight>[] server_list = new List[client_w.length];
		
		Iterator<Integer> iter_order = order.iterator();
		while(iter_order.hasNext()) {
			int i = iter_order.next();
			
			WeightPackage clientData = new WeightPackage();
			
			List<Weight> output = client_w[i];
			
			if(compress) {
				clientData.isCompressed = true;
				clientData.threshold = param.getCompressParam().getRatio();
				List<Weight> compressed = WeightUtil.compressWeightListRandomly2(output, weightProfile, clientData.threshold);
				Iterator<Weight> it = compressed .iterator();
				Iterator<Weight> it2 = output.iterator();
				
				int org = 0;
				int comp = 0;
				while(it.hasNext()) {
					Weight w = it.next();
					if(w.floatData != null)
						comp += w.floatData.length;
					else
						comp += w.sparseData.data.length;
					
					org += it2.next().floatData.length;
				}
				System.out.println(String.format("deflated to %.2f%%", 1.25f * comp / org * 100));
				
				clientData.stat = WeightStat.generateWeightStat(output);
				output = compressed;
			}
			
			clientData.weightList = output;
			clientData.factor = updateFactor;
			Socket s = new Socket(hostBook.get(i), listenPort);
			
			ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
			ObjectInputStream in = new ObjectInputStream(s.getInputStream());
			
			out.writeObject(clientData);
			System.out.println(String.format("client: weight sent to %s (%d)", hostBook.get(i), i));
			
			
			WeightPackage serverData = (WeightPackage) in.readObject();
			List<Weight> server_w = serverData.weightList;
			server_list[i] = server_w;
			System.out.println(String.format("client: received weight from %s (%d)", hostBook.get(i), i));
			
			s.close();
			
			if(server_w.size() != client_w[i].size()) {
				throw new Exception("weight size mismatched!");
			}
					
			
			Iterator<Weight> c_iter = client_w[i].iterator();
			Iterator<Weight> s_iter = server_w.iterator();
			
			while(c_iter.hasNext()) {
				Weight c_weight = c_iter.next();
				Weight s_weight = s_iter.next();
				WeightUtil.EASGDUpdateClient(s_weight, c_weight, serverData.stat, clientData.stat, updateFactor, param);
			}
		}
		
		if(debug && myID == 0 &&
				iteration % (param.getSnapshotPeriod() * param.getPeriod()) == 0) {
			saveMatFile(server_list, String.format("%s_%d_server_debug.mat", param.getSnapshotPrefix(), iteration));
		}	
	
		return client_w;
	}
	
    protected static void saveMatFile(List<Weight>[] weights, String format) throws IOException {
    	FileSystem fs = FileSystem.get(new Configuration()); 
		
		int size = 0;
		for(int i = 0; i < weights.length; i++) {
			Iterator<Weight> iter = weights[i].iterator();
			
			while(iter.hasNext()) {
				size += iter.next().floatData.length;
			}
		}
		
		double[] j_weight = new double[size];
		
		
		int index= 0;
		for(int i = 0; i < weights.length; i++) {
			Iterator<Weight> iter_w = weights[i].iterator();
			
			while(iter_w.hasNext()) {
				float[] w = iter_w.next().floatData;
				
				for(int k =0; k < w.length; k++) {
					j_weight[index+k] = w[k];
				}
				
				index += w.length;
			}
		}
		
		
		MLDouble weight = new MLDouble("weight", j_weight, 1);
		
		ArrayList<MLArray> a = new ArrayList<MLArray>();
		a.add(weight);
		
		new MatFileWriter(format, a);
		
		fs.copyFromLocalFile(new Path(format), new Path(format));
	}
}
