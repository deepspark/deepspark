package org.acl.deepspark.nn.async.easgd;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.acl.deepspark.data.Weight;
import org.acl.deepspark.data.WeightPackage;
import org.acl.deepspark.nn.CaffeNet;
import org.acl.deepspark.tf.TFNet;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam;
import org.acl.deepspark.utils.DeepSparkConf.CompressionParam.Regularizer;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam.Variables;
import org.acl.deepspark.utils.WeightUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class ParameterEASGDServer {
	private int listenPort;
	private ServerSocket exchangeSocket;
	
	private boolean stopSign = false;
		
	private Thread thread;
	
	private int updateCount = 0;
	
	private int tick;
	private String dirName;
	
	private long startTime;
	
	private List<Weight> server_params;
	
	private float movingRate;
	private float decayRate;
	private float sparseCoeff;
	private int decayLimit;
	private int decayStep;
	
	private float serverFactor;
	private float factorDecayRate;
	private int factorDecayLimit;
	private int factorDecayStep;
	private PrintStream logStream = null;
	
	private boolean compress = false;
	private float compressRatio;
	private List<Variables> weightProfiles;
	private float threshold = 0.0f;
	private int applyOption;
	
	
	public void setStartTime(long time) {
		startTime = time;
	}
	
	private void printLogTime(String mesg) {
		if(logStream == null)
			System.out.println(String.format("[%f parameterserver]: %s", (float) (System.currentTimeMillis() - startTime) / 1000 ,mesg));
		else
			logStream.println(String.format("[%f parameterserver]: %s", (float) (System.currentTimeMillis() - startTime) / 1000 ,mesg));
	}
	
	private class EASGDThread implements Runnable {
		private Socket s;
		
		public EASGDThread(Socket s) {
			this.s = s;
		}
		
		@Override
		public void run() {
			try {
				ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
				ObjectInputStream in = new ObjectInputStream(s.getInputStream());
				
				WeightPackage serverData = new WeightPackage();
				serverData.isCompressed = compress;
				serverData.threshold = threshold;
				
				List<Weight> output;
				
				if(compress) {
					Date startTime = new Date();
					output = new ArrayList<Weight>();
					long orig_size = 0;
					long comp_size = 0;
					Iterator<Weight> iter = server_params.iterator();
					Iterator<Variables> pIter = weightProfiles.iterator();
					while(iter.hasNext()) {
						Weight w = iter.next();
						if(pIter.next().getSkipCompress())
							output.add(w);
						else {
							Weight comp = WeightUtil.compress(w, threshold); 
							comp_size = comp.sparseData.data.length * 5;
							orig_size = w.floatData.length * 4;
							output.add(comp);
						}
					}
					Date endTime = new Date();
					System.out.println(String.format("compressed size: %.2f%% for %d ms", 
							( (float) comp_size / orig_size * 100), (endTime.getTime() - startTime.getTime())));
					
				} else {
					output = server_params;
				}
				
				serverData.weightList = output;
				
				out.writeObject(serverData);
				
				WeightPackage clientData = (WeightPackage) in.readObject();
				List<Weight> client_w = clientData.weightList;
				s.close();

				float eff_rate = movingRate * serverFactor;
				printLogTime(String.format("parameter exchanged with coeff %f", eff_rate));
				
				//threshold = threshold * (1-eff_rate) + (eff_rate) * client_w.get(0).threshold;
				
				// EASGD update
				List<Weight> server_w = server_params;

				Iterator<Weight> s_iter = server_w.iterator();
				Iterator<Weight> c_iter = client_w.iterator();
				while(s_iter.hasNext())
					WeightUtil.EASGDUpdate(c_iter.next(), s_iter.next(), eff_rate, sparseCoeff,clientData.threshold, applyOption, Regularizer.L2);
				
			} catch (IOException | RuntimeException e) {
				if(stopSign) {
					printLogTime("closed.");
				} else {
					printLogTime("got Error!");
					e.printStackTrace();
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public ParameterEASGDServer(CaffeNet net, int listenPort, int numWorker, DeepSparkParam param) {
		//p = net;
			
		this.listenPort = listenPort;
		this.startTime = System.currentTimeMillis();
		this.applyOption = param.getCompressParam().getApplyOption();
		
		// snapshot
		if(net.getSolverSetting().hasSnapshot()) {
			tick = net.getSolverSetting().getSnapshot() * numWorker / param.getPeriod();
			dirName = net.getSolverSetting().getSnapshotPrefix();
		} else {
			tick = 0;
		}
		server_params = net.getWeights();
		
		// intermediate test
//		if(p.getSolverSetting().hasTestInterval())
//			testStep = p.getSolverSetting().getTestInterval();
//		else
//			testStep = 0;
						
		// moving rate, alpha
		movingRate = param.getMovingRate();
		sparseCoeff = param.getFixedMovingRate();
		decayLimit = param.getDecayLimit();
		decayRate = param.getDecayRate();
		decayStep = param.getDecayStep() * numWorker / param.getPeriod();
		
		// server factor
		serverFactor = param.getServerFactor();
		factorDecayLimit = param.getFactorDecayLimit();
		factorDecayRate = param.getFactorDecayRate();
		factorDecayStep = param.getFactorDecayStep() * numWorker / param.getPeriod();		
	}
	
	public ParameterEASGDServer(TFNet net, int listenPort, int numWorker, DeepSparkParam param) {
		this.listenPort = listenPort;
		this.startTime = System.currentTimeMillis();
		server_params = net.getWeights();
		tick = 0;
						
		// moving rate, alpha
		movingRate = param.getMovingRate();
		decayLimit = param.getDecayLimit();
		decayRate = param.getDecayRate();
		decayStep = param.getDecayStep() * numWorker / param.getPeriod();
		
		// server factor
		serverFactor = param.getServerFactor();
		factorDecayLimit = param.getFactorDecayLimit();
		factorDecayRate = param.getFactorDecayRate();
		factorDecayStep = param.getFactorDecayStep() * numWorker / param.getPeriod();
		
		try {
			compress = param.getCompressed();
			compressRatio = param.getCompressParam().getRatio();
			weightProfiles = net.getVariableProfile();
			threshold = WeightUtil.findThreshold(server_params, weightProfiles, compressRatio);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("Compression disabled!");
		}
	}
		
	public void stopServer() {
		stopSign = true;
		try {
			exchangeSocket.close();
			thread.join();
			logStream.close();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	public void startServer() throws IOException {
		exchangeSocket = new ServerSocket(listenPort);
		
		// exchange Thread
		thread = new Thread(new Runnable() {
			ExecutorService connectionService;
			
			@Override
			public void run() {
				System.out.println("Start Server...");
				connectionService = Executors.newFixedThreadPool(4);
				try {
					Configuration conf = new Configuration();
					FileSystem fs;
					fs = FileSystem.get(conf);
					
					// param log init
					if(dirName != null) {
						String logfile = String.format("%s/log.txt", dirName);
						Path path = new Path(logfile);
						FSDataOutputStream fin = fs.create(path);
						logStream = new PrintStream(fin);
					}
					
					while(!stopSign) {
						Socket a = exchangeSocket.accept();
						connectionService.execute(new EASGDThread(a));						
						
						updateCount++;
						
						
						// decay moving rate
						if(decayStep > 0 && decayLimit != 0 && updateCount % decayStep == 0 ) {
							decayLimit--;
							movingRate *= decayRate;
							printLogTime(String.format("moving rate: %f", movingRate));
						}
						
						// decay server factor						
						if(factorDecayStep > 0 && factorDecayLimit != 0 && updateCount % factorDecayStep == 0 ) {
							factorDecayLimit--;
							serverFactor *= factorDecayRate;
							printLogTime(String.format("server factor : %f", serverFactor));
						}
						
						if(tick != 0 && updateCount % tick == 0) {
							String filename = String.format("%s/state_iter_%d.weight", dirName, updateCount);
							Path path = new Path(filename);
							
							FSDataOutputStream fin = fs.create(path);
							ObjectOutputStream out = new ObjectOutputStream(fin);
							out.writeObject(server_params);
							out.close();
							printLogTime(String.format("%s saved..",filename));
						}
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				connectionService.shutdown();
				while (!connectionService.isTerminated()) {
		        }
			}
		});
		thread.start();
	}
	
	
	public List<Weight> getServer_params() {
		return server_params;
	}

	public int getUpdateCount() {
		return updateCount;
	}
}
