package org.acl.deepspark.nn;

import java.io.File;
import java.io.FileReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.acl.deepspark.caffe.CaffeWrapper;
import org.acl.deepspark.data.ByteRecord;
import org.acl.deepspark.data.LMDBWriter;
import org.acl.deepspark.data.Record;
import org.acl.deepspark.data.RecordBatch;
import org.acl.deepspark.data.Weight;
import org.acl.deepspark.learning.Optimizer;
import org.acl.deepspark.nn.async.easgd.CoordinateProfile;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam.Variables;
import org.acl.deepspark.utils.DeepSparkConf.DeepSparkParam.Variables.Type;

import com.google.protobuf.TextFormat;

import caffe.Caffe.LayerParameter;
import caffe.Caffe.NetParameter;
import caffe.Caffe.NetStateRule;
import caffe.Caffe.Phase;
import caffe.Caffe.SolverParameter;

public class CaffeNet implements Serializable, Optimizer {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4406616544404720649L;
	private CaffeWrapper netLearner;
    private String trainingDataName;
    private String testDataName;
    private int[] inputDim;
    private List<Variables> variable_specs = null;
    
    public CaffeNet(String solverSpec, String netSpec, int[] dummySize) {
    	try {
    		FileReader reader;
    		reader = new FileReader(solverSpec);
			SolverParameter.Builder solBuilder = SolverParameter.newBuilder();
			TextFormat.merge(reader, solBuilder);
			String netFile = solBuilder.getNet();
			reader.close();
			
			reader = new FileReader(netFile);
			NetParameter.Builder netBuilder = NetParameter.newBuilder();
			TextFormat.merge(reader, netBuilder);
			
			makeDummy(netBuilder.build(), dummySize);
			reader.close();
			
			this.netLearner = new CaffeWrapper(solverSpec);
	        initialize();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
    public CaffeNet(String solverSpec) {
    	this.netLearner = new CaffeWrapper(solverSpec);
        initialize();
    }
    
    public CaffeNet(String solverSpec, int[] dummySize) {
    	try {
    		FileReader reader;
    		reader = new FileReader(solverSpec);
			SolverParameter.Builder solBuilder = SolverParameter.newBuilder();
			TextFormat.merge(reader, solBuilder);
			String netFile = solBuilder.getNet();
			reader.close();
			
			reader = new FileReader(netFile);
			NetParameter.Builder netBuilder = NetParameter.newBuilder();
			TextFormat.merge(reader, netBuilder);
			
			makeDummy(netBuilder.build(), dummySize);
			reader.close();
			
			this.netLearner = new CaffeWrapper(solverSpec);
	        initialize();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }


    private void makeDummy(NetParameter netparam, int[] dummySize) {
    	// make missed dirs
		try {
			List<LayerParameter> list = netparam.getLayerList();
			
			// for training layer
			int idx = findDataLayer(list, Phase.TRAIN);			
			LayerParameter l = list.get(idx);
			if(l.getType().equals("Data")) {
				int batchsize = l.getDataParam().getBatchSize();
				String source = l.getDataParam().getSource();
				File dir = new File(source);
				if(!dir.exists()) {
					dir.mkdirs();
					
					LMDBWriter d = new LMDBWriter(source, batchsize);
					ByteRecord s = new ByteRecord();
					s.data = new byte[dummySize[0] * dummySize[1] * dummySize[2]];
					s.label = 0;
					s.dim = new int[] {dummySize[0], dummySize[1], dummySize[2]};
							
					for(int i = 0; i < batchsize; i++) {
						d.putSample(s);
					}
					d.closeLMDB();
				}
			}
			
			// for testing layer
			idx = findDataLayer(list, Phase.TEST);			
			l = list.get(idx);
			if(l.getType().equals("Data")) {
				int batchsize = l.getDataParam().getBatchSize();
				String source = l.getDataParam().getSource();
				File dir = new File(source);
				if(!dir.exists()) {
					dir.mkdirs();
				
					LMDBWriter d = new LMDBWriter(source, batchsize);
					Record s = new Record();
					s.data = new float[dummySize[0] * dummySize[1] * dummySize[2]];
					s.label = 0;
					s.dim = new int[] {dummySize[0], dummySize[1], dummySize[2]};
					
					for(int i = 0; i < batchsize; i++) {
						d.putSample(s);
					}
					
					d.closeLMDB();
				}
			}
			
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
    }
    
    public CaffeNet(byte[] solverSpec, byte[] netSpec) {
    	this.netLearner = new CaffeWrapper(solverSpec);
    	initialize();
    }
    
    public int getTrainingBatchSize() {
    	List<LayerParameter> layers = netLearner.getNetParameter().getLayerList();
        int trainIdx = findDataLayer(layers, Phase.TRAIN);
        LayerParameter trainingDataLayer = layers.get(trainIdx);
        return trainingDataLayer.getMemoryDataParam().getBatchSize();
    }
    
    private void initialize() {
    	List<LayerParameter> layers = netLearner.getNetParameter().getLayerList();
        int trainIdx = findDataLayer(layers, Phase.TRAIN);
        
        if(trainIdx == -1) {
        	// error handling
        	System.out.println("cannot find training data layer...");
        } else {
	        LayerParameter trainingDataLayer = layers.get(trainIdx);
	        trainingDataName = trainingDataLayer.getName();
	        
	        String layerType = trainingDataLayer.getType();
	        if(layerType.equals("MemoryData")) {
		        inputDim = new int[4]; // batchSize, channels, height, width;
		        inputDim[0] = trainingDataLayer.getMemoryDataParam().getBatchSize();// batchSize;
		        inputDim[1] = trainingDataLayer.getMemoryDataParam().getChannels();// channels;
		        inputDim[2] = trainingDataLayer.getMemoryDataParam().getHeight();// height;
		        inputDim[3] = trainingDataLayer.getMemoryDataParam().getWidth();// width;
		        
		        System.out.println("Iteration : " + netLearner.getSolverParameter().getMaxIter());
		        System.out.println("Learning Rate : " + netLearner.getSolverParameter().getBaseLr());
		        System.out.println(String.format("batchSize: %d", inputDim[0]));
		        System.out.println(String.format("channel: %d", inputDim[1]));
		        System.out.println(String.format("height: %d", inputDim[2]));
		        System.out.println(String.format("width: %d", inputDim[3]));
		        System.out.println(String.format("momentum: %4f", netLearner.getSolverParameter().getMomentum()));
		        System.out.println(String.format("decayLambda: %4f", netLearner.getSolverParameter().getWeightDecay()));
	        } else if(layerType.equals("Data")) {
	        	inputDim = new int[1];
	        	inputDim[0] = trainingDataLayer.getDataParam().getBatchSize();
	        	
	        	System.out.println("Iteration : " + netLearner.getSolverParameter().getMaxIter());
		        System.out.println("Learning Rate : " + netLearner.getSolverParameter().getBaseLr());
		        System.out.println(String.format("batchSize: %d", inputDim[0]));
		        System.out.println(String.format("momentum: %4f", netLearner.getSolverParameter().getMomentum()));
		        System.out.println(String.format("decayLambda: %4f", netLearner.getSolverParameter().getWeightDecay()));
		        System.out.println(String.format("Training Source: %s", trainingDataLayer.getDataParam().getSource()));
	        }
        }
        
        int testIdx = findDataLayer(layers, Phase.TEST);	
        if(testIdx == -1 ) {
        	// error handling
        } else {
	        
	        LayerParameter testingDataLayer = layers.get(testIdx);
	        testDataName = testingDataLayer.getName();
        }
    }

    private int findDataLayer(List<LayerParameter> list, Phase phase) {
    	int res = -1;
    	for(int i = 0; i < list.size();i++) {
    		LayerParameter l = list.get(i);
    		List<NetStateRule> rule = l.getIncludeList();
    		for(int j =0; j < rule.size(); j++)
	    		if( l.getType().contains("Data") &&
	    				rule.get(j).hasPhase() &&
	    				rule.get(j).getPhase() == phase) {
	    			res = i;
	    			return res;
	    		}
    	}
    	
    	return res;
    }
    
    public int findDataLayer(Phase phase) {
    	return findDataLayer(netLearner.getNetParameter().getLayerList(), phase);
    }
    
    public int getIteration() {
    	return netLearner.getSolverParameter().getMaxIter();
    }
    
    public void setTrainData(RecordBatch dataset) {
        netLearner.setTrainBuffer(trainingDataName, dataset.data, dataset.label, dataset.size);
    }
    
    public void setTestData(RecordBatch dataset) {
        netLearner.setTestBuffer(testDataName, dataset.data, dataset.label, dataset.size);
    }
    
    public void train() {
    	netLearner.train();
    }
    
    public void trainWithLocal() {
    	netLearner.trainWithLocal();
    }
    
    public float[] test() {
    	return netLearner.test();
    }
    
    public float[] testWithLMDB() {
    	return netLearner.testWithLMDB();
    }
    
    public void snapshot(String filename) {
    	netLearner.snapshot(filename);
    }
    
    public void restore(String filename) {
    	netLearner.restore(filename);
    }
    
//    public List<Weight> getGradients() {
//    	List<float[]> list = netLearner.getGradients();
//    	List<Weight> ret = new ArrayList<Weight>();
//    	
//    	for(int i = 0; i < list.size();i++) {
//    		Weight w = new Weight();
//    		w.layerIndex = i;
//    		w.offset = 0;
//    		w.floatData = list.get(i);
//    		ret.add(w);
//    	}
//    	
//    	return ret;
//    }
    
//    public void setGradients(List<Weight> grads) {
//    	for(int i =0; i < grads.size(); i++) {
//    		Weight w = grads.get(i);
//    		netLearner.setGradient(w.floatData, w.offset, i);
//    	}
//    }
    
    public List<Weight> getWeights() {
    	List<float[]> list = netLearner.getWeights();
    	List<Weight> ret = new ArrayList<Weight>();
    	
    	for(int i = 0; i < list.size();i++) {
    		Weight w = new Weight();
    		w.layerIndex = i;
    		w.offset = 0;
    		w.floatData = list.get(i);
    		ret.add(w);
    	}
    	
    	return ret;
    }
    
    public void getWeights(List<Weight>[] oldWeight) {
    	for(int i =0; i < oldWeight.length; i++) {
    		Iterator<Weight> iter = oldWeight[i].iterator();
    		int l = 0;
    		while(iter.hasNext()) {
    			Weight w = iter.next();
    			netLearner.getWeight(w.floatData, w.offset, w.floatData.length, l);
    			l++;
    		}
    	}
    }
    
    public List<Weight>[] getWeights(List<CoordinateProfile[]> profiles, int numWorker) {
    	List<Weight>[] list = new List[numWorker];
		for (int i = 0; i < numWorker; i++)
			list[i] = new ArrayList<Weight>(); 
		
		Iterator<Variables> vIter = variable_specs.iterator();
		Iterator<CoordinateProfile[]> pIter = profiles.iterator();
    	Iterator<Weight> wIter = getWeights().iterator();
		
		int index = 0;
		while(vIter.hasNext()) {
			Variables v = vIter.next();
			CoordinateProfile[] p = pIter.next();
			Weight weight = wIter.next();
			
			for(int i =0; i < p.length; i++) {
				Weight w = new Weight();
				w.layerIndex = index;
				w.offset = p[i].offset;
				w.type = v.getType().getNumber();
				w.floatData = new float[p[i].length];
				System.arraycopy(weight.floatData, p[i].offset, w.floatData, 0, p[i].length);
				list[i].add(w);
			}
			index++;
		}
		
		return list;
    }
    
    public Map<Integer, Weight> getWeightsMap() {
    	List<float[]> list = netLearner.getWeights();
    	Map<Integer,Weight> ret = new HashMap<Integer,Weight>();
    	
    	for(int i = 0; i < list.size();i++) {
    		Weight w = new Weight();
    		w.offset = 0;
    		w.floatData = list.get(i);
    		ret.put(i, w);
    	}
    	
    	return ret;
    }
    
    public void setWeights(List<Weight> weights) {
    	for(int i =0; i < weights.size(); i++) {
    		Weight w = weights.get(i);
    		netLearner.setWeight(w.floatData, w.offset, i);
    	}
    }
    
    public RecordBatch buildBlobs(List<Record> list) {
    	if(inputDim.length== 4) {
	    	RecordBatch ret = new RecordBatch(list.size(), inputDim[1], inputDim[2], inputDim[3]);
	    	
	    	for(int i = 0; i < list.size(); i++) {
	    		Record s = list.get(i);
	    		for(int j = 0; j < s.data.length; j++) {
	    			ret.data.putFloat((i* s.data.length + j) * Float.SIZE / Byte.SIZE, s.data[j]);
	    		}
	    		ret.label.putFloat(i*Float.SIZE / Byte.SIZE, s.label);
	    	}
	    	return ret;
    	} else return null;
    }
    
    public RecordBatch buildBlobs(Iterator<Record> iter) {
    	List<Record> array = new ArrayList<Record>();
    	while(iter.hasNext())
    		array.add(iter.next());
    	return buildBlobs(array);
    }
    
    public SolverParameter getSolverSetting() {
    	return netLearner.getSolverParameter();
    }
    
    public NetParameter getNetConf() {
    	return netLearner.getNetParameter();
    }

    public void setCPU() {
    	netLearner.set_mode(CaffeWrapper.CAFFE_CPU);
    }
    
    public void setGPU() {
    	netLearner.set_mode(CaffeWrapper.CAFFE_GPU);
    }
    
    public int getBatchSize() {
    	return inputDim[0];
    }
    
    public void printLearnableWeightInfo() {
    	List<Weight> list = getWeights();
    	System.out.println("<Weight Info>");
    	for(int i =0; i < list.size(); i++) {
    		Weight w = list.get(i);
    		System.out.println(String.format("Layer%d, # of parameter: %d", i, w.floatData.length));
    	}
    }
    
    public void close() {
    	netLearner.clearSolver();
    }

	public List<Variables> getVariableProfile() {
		if(variable_specs == null) {
			List<Variables> list= new ArrayList<Variables>();
			Iterator<Weight> w_iter = getWeights().iterator();
			
			int index = 0;
			while(w_iter.hasNext()) {
				Weight w = w_iter.next();
				
				Variables.Builder b = Variables.newBuilder();
				b.addDim(w.floatData.length);
				b.setName(String.format("layer%d", index++));
				b.setSkipCompress(false);
				b.setType(Type.FLOAT);
				
				list.add(b.build());
			}
			variable_specs = list;
		}
		
		return variable_specs;
	}

	public void setWeights(List<Weight>[] weights) {
		// TODO Auto-generated method stub
		Iterator<Weight>[] iter = new Iterator[weights.length];
		for(int i = 0; i< weights.length; i++) {
			iter[i] = weights[i].iterator();
		}
		
		int index = 0;
		while(iter[0].hasNext()) {
			for(int i =0; i < iter.length; i++) {
				Weight w = iter[i].next();
				netLearner.setWeight(w.floatData, w.offset, index);
			}
			index++;
		}
	}

	public void getWeightWithMasking(List<Weight>[] oldWeight, List<float[]>[] diffMap, float th) {
		long start = System.currentTimeMillis();
		Iterator<Weight> currentIter = getWeights().iterator();
		long end = System.currentTimeMillis();
		System.out.println(String.format("get weight : %d ms", end - start));
		Iterator<Weight>[] wIter = new Iterator[oldWeight.length];
		Iterator<float[]>[] diffIter = new Iterator[diffMap.length];
		for(int i = 0; i < diffIter.length; i++) {
			diffIter[i] = diffMap[i].iterator();
			wIter[i] = oldWeight[i].iterator();
		}
		
		start = System.currentTimeMillis();
		while(wIter[0].hasNext()) {
			Weight c = currentIter.next();
			int offset = 0;
			for(int i = 0; i < diffIter.length;i++) {
				Weight w = wIter[i].next();
				float[] diff = diffIter[i].next();
				for(int k = 0; k < diff.length; k++) {
					if(diff[k] > th) {
						w.floatData[k] = c.floatData[offset+k];
					}
				}
				
				offset += diff.length;
			}
		}			
		end = System.currentTimeMillis();
		System.out.println(String.format("mod weight : %d ms", end - start));
		
		start = System.currentTimeMillis();
		setWeights(oldWeight);
		end = System.currentTimeMillis();

		System.out.println(String.format("set weight : %d ms", end - start));
	}
	
	public void getWeightWithMaskingTiming(List<Weight>[] oldWeight, List<float[]>[] diffMap, float th) {
		long start = System.currentTimeMillis();
		Iterator<Weight> currentIter = getWeights().iterator();
		long end = System.currentTimeMillis();
		System.out.println(String.format("get weight : %d ms", end - start));
		Iterator<Weight>[] wIter = new Iterator[oldWeight.length];
		Iterator<float[]>[] diffIter = new Iterator[diffMap.length];
		for(int i = 0; i < diffIter.length; i++) {
			diffIter[i] = diffMap[i].iterator();
			wIter[i] = oldWeight[i].iterator();
		}
		
		start = System.currentTimeMillis();
		while(wIter[0].hasNext()) {
			Weight c = currentIter.next();
			int offset = 0;
			for(int i = 0; i < diffIter.length;i++) {
				Weight w = wIter[i].next();
				float[] diff = diffIter[i].next();
				for(int k = 0; k < diff.length; k++) {
					if(diff[k] > th) {
						w.floatData[k] = c.floatData[offset+k];
					}
				}
				
				offset += diff.length;
			}
		}			
		end = System.currentTimeMillis();
		System.out.println(String.format("mod weight : %d ms", end - start));
		
		start = System.currentTimeMillis();
		setWeights(oldWeight);
		end = System.currentTimeMillis();

		System.out.println(String.format("set weight : %d ms", end - start));
	}
	
	public void getWeightWithMasking2(List<Weight>[] oldWeight, List<float[]>[] diffMap, float th) {
		Iterator<Weight> currentIter = getWeights().iterator();
		Iterator<Weight>[] wIter = new Iterator[oldWeight.length];
		Iterator<float[]>[] diffIter = new Iterator[diffMap.length];
		for(int i = 0; i < diffIter.length; i++) {
			diffIter[i] = diffMap[i].iterator();
			wIter[i] = oldWeight[i].iterator();
		}
		
		while(wIter[0].hasNext()) {
			Weight c = currentIter.next();
			int offset = 0;
			for(int i = 0; i < diffIter.length;i++) {
				Weight w = wIter[i].next();
				float[] diff = diffIter[i].next();
				for(int k = 0; k < diff.length; k++) {
					if(diff[k] > th) {
						w.floatData[k] = c.floatData[offset+k];
					}
				}
				
				offset += diff.length;
			}
		}
	}

	public void addWeights(List<Weight>[] weights) {
		Iterator<Weight>[] iter = new Iterator[weights.length];
		for(int i = 0; i< weights.length; i++) {
			iter[i] = weights[i].iterator();
		}
		
		int index = 0;
		while(iter[0].hasNext()) {
			for(int i =0; i < iter.length; i++) {
				Weight w = iter[i].next();
				netLearner.addWeight(w.floatData, w.offset, index);
			}
			index++;
		}
	}
}
