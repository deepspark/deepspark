package org.acl.deepspark.data;

public class SeqBatch {
	public int bucketId;
	public int batchSize;
	public int[][] encodeBatch;
	public int[][] decodeBatch;
	public float[][] targetWeight;
}
