package org.acl.deepspark.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.math.stat.descriptive.rank.Median;

public class WeightStat implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8958905253342093967L;
	private float mean = 1.0f;
	public float getMean() {
		return mean;
	}

	public float getStdev() {
		return stdev;
	}

	private float stdev = 1.0f;
	
	public static float makeBias(List<Weight> list, List<Weight> sparseList, float a) {
		float b=0f;
		
		
		return b;
	}
	
	public static WeightStat generateWeightStat(List<Weight> list) {
		WeightStat ret = new WeightStat();
		
		ret.mean = 0f;
		ret.stdev = 0f;
		int count = 0;
		
		Iterator<Weight> iter = list.iterator();
		while(iter.hasNext()) {
			Weight w = iter.next();
			if(w.floatData == null) {
				float local_mean = 0f;
				float local_var = 0f;
				for(int k = 0; k < w.sparseData.data.length; k++) {
					float val = w.sparseData.data[k];
					local_mean += val;
					local_var += (val * val);
				}
				ret.mean += local_mean * ((float) w.originalLength / w.sparseData.data.length);
				ret.stdev += local_var * ((float) w.originalLength / w.sparseData.data.length);
				count += w.originalLength;
			} else {
				for(int k = 0; k <w.floatData.length; k++) {
					float val = w.floatData[k];
					ret.mean += val;
					ret.stdev += (val * val);
					
				}
				count += w.floatData.length;
			}
		}
		
		if(count != 0) {
			ret.stdev /= count;
			ret.mean /= count;
			
			ret.stdev -= (ret.mean * ret.mean); // var = E(x^2) - E(x)^2
			ret.stdev = (float) Math.sqrt(ret.stdev); // stdev = sqrt(var);
		}
		
		return ret;
	}
	
	public static WeightStat generateWeightStat(List<Weight> list, List<Weight> sparseList) {
		WeightStat ret = new WeightStat();
		
		ret.mean = 0f;
		ret.stdev = 0f;
		int count = 0;
		
		Iterator<Weight> iter = list.iterator();
		Iterator<Weight> s_iter = sparseList.iterator();
		while(iter.hasNext()) {
			Weight w = iter.next();
			Weight sparse = s_iter.next();
			
			if(sparse.floatData == null) { // spatial mean and stdev
				for(int k = 0; k <w.floatData.length; k++) {
					float val = w.floatData[k];
					ret.mean += val;
					ret.stdev += (val * val);
					
				}
				count += w.floatData.length;
				
				// remove real data	from the stats
				for(int k = 0; k <sparse.sparseData.data.length; k++) {
					float val = sparse.sparseData.data[k];
					if(!Float.isNaN(val)) {
						ret.mean -= val;
						ret.stdev -= (val * val);
						count--;
					}	
				}
			} else {
				for(int k = 0; k <w.floatData.length; k++) {
					float val = w.floatData[k];
					ret.mean += val;
					ret.stdev += (val * val);
					
				}
				count += w.floatData.length;
			}
		}
		
		if(count != 0) {
			ret.stdev /= count;
			ret.mean /= count;
			
			ret.stdev -= (ret.mean * ret.mean); // var = E(x^2) - E(x)^2
			ret.stdev = (float) Math.sqrt(ret.stdev); // stdev = sqrt(var);
		}
		
		System.out.println(ret.mean);
		System.out.println(ret.stdev);
		
		return ret;
	}
}
