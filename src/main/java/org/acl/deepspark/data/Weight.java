package org.acl.deepspark.data;

import java.io.Serializable;

import org.acl.deepspark.tf.TFWrapper.Tensor;

public class Weight implements Serializable {
	private static final long serialVersionUID = 4428776235887313512L;
	public float[] floatData = null;
	public int offset;
	public int layerIndex;
	public int type = Tensor.FLOAT;
	
	public SparseEntries sparseData = null;
	public int originalLength = 0;
}
