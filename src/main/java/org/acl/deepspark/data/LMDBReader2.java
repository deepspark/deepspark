package org.acl.deepspark.data;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import org.fusesource.lmdbjni.BufferCursor;
import org.fusesource.lmdbjni.Constants;
import org.fusesource.lmdbjni.Database;
import org.fusesource.lmdbjni.Env;
import org.fusesource.lmdbjni.Transaction;

public class LMDBReader2 implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3966228856774805437L;
	private Env env;
	private Database dbInstance;
	private BufferCursor iter;
	private Transaction tx;
	
	
	public LMDBReader2(String path) {
		boolean isOpen = false;
		try {
			env = new Env();
			env.setMapSize(1099511627776L);
			env.open(path, Constants.NOLOCK);
			
			dbInstance = env.openDatabase();
			isOpen = true;
		} catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			if (isOpen) {
				System.out.println("lmdb opened successfully");
			}
		}
		tx = env.createReadTransaction();
		iter = dbInstance.bufferCursor(tx);
		iter.first();
	}
	
	public boolean isLast() {
		boolean test = iter.next();
		if(test)
			iter.prev();
		return test;
	}
	
	public SeqRecord getSample() throws IOException, ClassNotFoundException {
		ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(iter.valBytes()));
		SeqRecord data = (SeqRecord) in.readObject();
		if(!iter.next()) {
			iter.first();
		}
		
		return data;
	}
	
	public void closeLMDB() {
		System.out.println("Reader successfully closed.");
	}
	
	@Override
	public void finalize() {
		closeLMDB();
	}
}
