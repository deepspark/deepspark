package org.acl.deepspark.data;

import java.io.Serializable;
import java.util.List;

public class WeightPackage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3449161899833433591L;
	
	public List<Weight> weightList = null;
	public boolean isCompressed = false;
	public float threshold = 0.0f;
	public float factor = 1.0f;
	public WeightStat stat = null;
}
