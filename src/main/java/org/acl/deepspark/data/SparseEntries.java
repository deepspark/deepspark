package org.acl.deepspark.data;

import java.io.Serializable;

public class SparseEntries implements Serializable {
	private static final long serialVersionUID = -2983185100284651689L;

	public byte[] index;
	public float[] data;
}
