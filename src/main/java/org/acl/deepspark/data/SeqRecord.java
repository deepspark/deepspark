package org.acl.deepspark.data;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SeqRecord implements Serializable {
	 
	/**
	 * 
	 */ 
	private static final long serialVersionUID = -5952537408488324812L;
	
	public List<Integer> encode_data = new ArrayList<Integer>();
	public List<Integer> decode_data = new ArrayList<Integer>();
	
	public byte[] toByteArray() throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ObjectOutputStream obj = new ObjectOutputStream(out);
		obj.writeObject(this);
		return out.toByteArray();
	}
}
